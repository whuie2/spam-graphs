(** Simple computer algebra system on the field of real numbers. *)

(** Represents an unevaluated expression on the field of real numbers.

    Expressions can be numeric, symbolic, or a combination of the two through a
    series of operations. Use the constructors to create arbitrary, verbatim
    expressions or the module's provided functions to make simplifications where
    possible.
    *)
type s =
  | Literal of float
  | Symbol of string
  | Neg of s
  | Add of s * s
  | Sub of s * s
  | Mul of s * s
  | Div of s * s
  | Sqrt of s
  | Sin of s
  | Cos of s
  | Tan of s
  | ASin of s
  | ACos of s
  | ATan of s
  | Sinh of s
  | Cosh of s
  | Tanh of s
  | ASinh of s
  | ACosh of s
  | ATanh of s
  | Pow of s * s
  | Exp of s
  | Log of s * s
  | Log2 of s
  | Log10 of s
  | Ln of s

(** {!s.Literal} constant values. *)
module Consts : sig
  (** Euler's number (e) *)
  val e : s

  (** 1/π *)
  val frac_1_pi : s

  (** 1/sqrt(2) *)
  val frac_1_sqrt_2 : s

  (** 2/π *)
  val frac_2_pi : s

  (** 2/sqrt(π) *)
  val frac_2_sqrt_pi : s

  (** π/2 *)
  val frac_pi_2 : s

  (** π/3 *)
  val frac_pi_3 : s

  (** π/4 *)
  val frac_pi_4 : s

  (** π/6 *)
  val frac_pi_6 : s

  (** π/8 *)
  val frac_pi_8 : s

  (** ln(2) *)
  val ln_2 : s

  (** ln(10) *)
  val ln_10 : s

  (** log{_ 2}(10) *)
  val log2_10 : s

  (** log{_ 2}(e) *)
  val log2_e : s

  (** log{_ 10}(2) *)
  val log10_2 : s

  (** log{_ 10}(e) *)
  val log10_e : s

  (** Archimedes' constant (π) *)
  val pi : s

  (** sqrt(2) *)
  val sqrt_2 : s

  (** The full circle constant (τ) *)
  val tau : s
end

(** [eq l r] tests for equality between two expressions.

    Two expressions are equal if:
    - they are both {!s.Literal}s holding equal values; or
    - they are both identical {!s.Symbol}s; or
    - they are physically equal.
    *)
val eq : s -> s -> bool

(** Logical inverse of {!eq}. *)
val ne : s -> s -> bool

(** [compare l r] is [Some n] if [l] and [r] are both {!s.Literal}s, where [n]
    is the output of {!Float.compare} for the contained [float]s, or [Some 0] if
    [l] and [r] are {!eq}.
    *)
val compare : s -> s -> int option

(** [lt l r] is [true] if [l] and [r] {!compare} to [Some n] with [n < 0]. *)
val lt : s -> s -> bool

(** [le l r] is [true] if [l] and [r] {!compare} to [Some n] with [n <= 0]. *)
val le : s -> s -> bool

(** [gt l r] is [true] if [l] and [r] {!compare} to [Some n] with [n > 0]. *)
val gt : s -> s -> bool

(** [ge l r] is [true] if [l] and [r] {!compare} to [Some n] with [n >= 0]. *)
val ge : s -> s -> bool

(** [neg x] is the additive inverse of [x]. *)
val neg : s -> s

(** [negf x] is the additive inverse of [x]. *)
val negf : float -> s

(** [add l r] is the sum of [l] and [r]. *)
val add : s -> s -> s

(** [fadd l r] is the sum of [l] and [r]. *)
val fadd : float -> s -> s

(** [addf l r] is the sum of [l] and [r]. *)
val addf : s -> float -> s

(** [faddf l r] is the sum of [l] and [r]. *)
val faddf : float -> float -> s

(** [sub l r] is the difference resulting from subtracting of [r] from [l]. *)
val sub : s -> s -> s

(** [fsub l r] is the difference resulting from subtracting of [r] from [l]. *)
val fsub : float -> s -> s

(** [subf l r] is the difference resulting from subtracting of [r] from [l]. *)
val subf : s -> float -> s

(** [fsubf l r] is the difference resulting from subtracting of [r] from [l]. *)
val fsubf : float -> float -> s

(** [mul l r] is the product of [l] and [r]. *)
val mul : s -> s -> s

(** [fmul l r] is the product of [l] and [r]. *)
val fmul : float -> s -> s

(** [mulf l r] is the product of [l] and [r]. *)
val mulf : s -> float -> s

(** [fmulf l r] is the product of [l] and [r]. *)
val fmulf : float -> float -> s

(** [div l r] is the quotient resulting from dividing [l] by [r]. *)
val div : s -> s -> s

(** [fdiv l r] is the quotient resulting from dividing [l] by [r]. *)
val fdiv : float -> s -> s

(** [divf l r] is the quotient resulting from dividing [l] by [r]. *)
val divf : s -> float -> s

(** [fdivf l r] is the quotient resulting from dividing [l] by [r]. *)
val fdivf : float -> float -> s

(** [sqrt x] is the square root of [x]. *)
val sqrt : s -> s

(** [sqrtf x] is the square root of [x]. *)
val sqrtf : float -> s

(** [sin x] is the sine of [x]. *)
val sin : s -> s

(** [sinf x] is the sine of [x]. *)
val sinf : float -> s

(** [cos x] is the cosine of [x]. *)
val cos : s -> s

(** [cosf x] is the cosine of [x]. *)
val cosf : float -> s

(** [tan x] is the tangent of [x]. *)
val tan : s -> s

(** [tanf x] is the tangent of [x]. *)
val tanf : float -> s

(** [asin x] is the arcsine of [x]. *)
val asin : s -> s

(** [asinf x] is the arcsine of [x]. *)
val asinf : float -> s

(** [acos x] is the arccosine of [x]. *)
val acos : s -> s

(** [acosf x] is the arccosine of [x]. *)
val acosf : float -> s

(** [atan x] is the arctangent of [x]. *)
val atan : s -> s

(** [atanf x] is the arctangent of [x]. *)
val atanf : float -> s

(** [sinh x] is the hyperbolic sine of [x]. *)
val sinh : s -> s

(** [sinhf x] is the hyperbolic sine of [x]. *)
val sinhf : float -> s

(** [cosh x] is the hyperbolic cosine of [x]. *)
val cosh : s -> s

(** [coshf x] is the hyperbolic cosine of [x]. *)
val coshf : float -> s

(** [tanh x] is the hyperbolic tangent of [x]. *)
val tanh : s -> s

(** [tanhf x] is the hyperbolic tangent of [x]. *)
val tanhf : float -> s

(** [asinh x] is the inverse hyperbolic sine of [x]. *)
val asinh : s -> s

(** [asinhf x] is the inverse hyperbolic sine of [x]. *)
val asinhf : float -> s

(** [acosh x] is the inverse hyperbolic cosine of [x]. *)
val acosh : s -> s

(** [acoshf x] is the inverse hyperbolic cosine of [x]. *)
val acoshf : float -> s

(** [atanh x] is the inverse hyperbolic tangent of [x]. *)
val atanh : s -> s

(** [atanhf x] is the inverse hyperbolic tangent of [x]. *)
val atanhf : float -> s

(** [pow b e] is the base [b] raised to the power [e]. *)
val pow : s -> s -> s

(** [fpow b e] is the base [b] raised to the power [e]. *)
val fpow : float -> s -> s

(** [powf b e] is the base [b] raised to the power [e]. *)
val powf : s -> float -> s

(** [fpowf b e] is the base [b] raised to the power [e]. *)
val fpowf : float -> float -> s

(** [exp x] is Euler's number e raised to the power [x]. *)
val exp : s -> s

(** [expf x] is Euler's number e raised to the power [x]. *)
val expf : float -> s

(** [log b x] is the logarithm of [x] in base [b]. *)
val log : s -> s -> s

(** [flog b x] is the logarithm of [x] in base [b]. *)
val flog : float -> s -> s

(** [logf b x] is the logarithm of [x] in base [b]. *)
val logf : s -> float -> s

(** [flogf b x] is the logarithm of [x] in base [b]. *)
val flogf : float -> float -> s

(** [log2 x] is the logarithm of [x] in base 2. *)
val log2 : s -> s

(** [log2f x] is the logarithm of [x] in base 2. *)
val log2f : float -> s

(** [log10 x] is the logarithm of [x] in base [10]. *)
val log10 : s -> s

(** [log10f x] is the logarithm of [x] in base [10]. *)
val log10f : float -> s

(** [ln x] is the natural logarithm if [x]. *)
val ln : s -> s

(** [lnf x] is the natural logarithm if [x]. *)
val lnf : float -> s

(** [sum terms] is the sum of all expressions contained in [terms], collected
    from the left.
    *)
val sum : s list -> s

(** [prod terms] is the product of all expressions contained in [terms],
    collected from the left.
    *)
val prod : s list -> s

(** [is_literal expr] is [true] if [expr] is a {!s.Literal}. *)
val is_literal : s -> bool

(** [is_symbol expr] is [true] if [expr] is a {!s.Symbol}. *)
val is_symbol : s -> bool

(** [is_neg expr] is [true] if [expr] is a {!s.Neg}. *)
val is_neg : s -> bool

(** [is_add expr] is [true] if [expr] is an {!s.Add}. *)
val is_add : s -> bool

(** [is_sub expr] is [true] if [expr] is a {!s.Sub}. *)
val is_sub : s -> bool

(** [is_mul expr] is [true] if [expr] is a {!s.Mul}. *)
val is_mul : s -> bool

(** [is_div expr] is [true] if [expr] is a {!s.Div}. *)
val is_div : s -> bool

(** [is_sqrt expr] is [true] if [expr] is a {!s.Sqrt}. *)
val is_sqrt : s -> bool

(** [is_sin expr] is [true] if [expr] is a {!s.Sin}. *)
val is_sin : s -> bool

(** [is_cos expr] is [true] if [expr] is a {!s.Cos}. *)
val is_cos : s -> bool

(** [is_tan expr] is [true] if [expr] is a {!s.Tan}. *)
val is_tan : s -> bool

(** [is_asin expr] is [true] if [expr] is an {!s.ASin}. *)
val is_asin : s -> bool

(** [is_acos expr] is [true] if [expr] is an {!s.ACos}. *)
val is_acos : s -> bool

(** [is_atan expr] is [true] if [expr] is an {!s.ATan}. *)
val is_atan : s -> bool

(** [is_sinh expr] is [true] if [expr] is a {!s.Sinh}. *)
val is_sinh : s -> bool

(** [is_cosh expr] is [true] if [expr] is a {!s.Cosh}. *)
val is_cosh : s -> bool

(** [is_tanh expr] is [true] if [expr] is a {!s.Tanh}. *)
val is_tanh : s -> bool

(** [is_asinh expr] is [true] if [expr] is an {!s.ASinh}. *)
val is_asinh : s -> bool

(** [is_acosh expr] is [true] if [expr] is an {!s.ACosh}. *)
val is_acosh : s -> bool

(** [is_atanh expr] is [true] if [expr] is an {!s.ATanh}. *)
val is_atanh : s -> bool

(** [is_pow expr] is [true] if [expr] is a {!s.Pow}. *)
val is_pow : s -> bool

(** [is_exp expr] is [true] if [expr] is an {!s.Exp}. *)
val is_exp : s -> bool

(** [is_log expr] is [true] if [expr] is a {!s.Log}. *)
val is_log : s -> bool

(** [is_log2 expr] is [true] if [expr] is a {!s.Log2}. *)
val is_log2 : s -> bool

(** [is_log10 expr] is [true] if [expr] is a {!s.Log10}. *)
val is_log10 : s -> bool

(** [is_ln expr] is [true] if [expr] is a {!s.Ln}. *)
val is_ln : s -> bool

(** [is_atom expr] is [true] if [expr] is a {!s.Literal} or a {!s.Symbol}. *)
val is_atom : s -> bool

(** [contains expr subexpr] is [true] if [expr] contains at least one instance
    of [subexpr], verbatim.
    *)
val contains : s -> s -> bool

(** [get_symbols expr] is a list of all {!s.Symbol}s within [expr]. *)
val get_symbols : s -> s list

(** [get_atoms expr] is a list of all {!s.Literal}s and {!s.Symbol}s within
    [expr].
    *)
val get_atoms : s -> s list

(** [diff expr var] is the partial derivative of [expr] with respect to [var].
    *)
val diff : s -> s -> s

(** Re-evaluate all sub-expressions within [expr]. *)
val reval : s -> s

(** Replace all instances of [target] within [expr] with [replace] and
    re-evaluate all sub-expressions.

    [target] must be matched verbatim in order for the substitution to be made.
    *)
val subs_single : s -> s -> s -> s

(** Make a series of substitutions and re-evaluate all sub-expressions. *)
val subs : (s * s) list -> s -> s

(** Replace all instances of [target] within [expr] with [replace] and
    re-evaluate all sub-expressions.

    [target] must be matched verbatim in order for the substitition to be made.
    *)
val subsf_single : s -> float -> s -> s

(** Make a series of substitutions and re-evaluate all sub-expressions. *)
val subsf : (s * float) list -> s -> s

(** Replace all instances of [target] within [expr] with [replace] and attempt
    to re-evaluate to a single float.

    [Error syms] is returned if there are any symbols remaining, where [syms] is
    a list of all such unique symbols.
    *)
val evalf_single : s -> float -> s -> (float, s list) result

(** Make a series of substitutions and attempt to re-evaluate to a single float.

    [Error syms] is returned if there are any symbols remaining, where [syms] is
    a list of all such unique symbols.
    *)
val evalf : (s * float) list -> s -> (float, s list) result

(** [to_string expr] is a string representation of [expr]. *)
val to_string : s -> string

(** [to_string_rust expr] is [expr] rendered as a Rust expression, taking all
    symbols to be of type [f64].
    *)
val to_string_rust : s -> string

(** [to_string_python expr] is [expr] rendered as a Python expression, taking
    all symbols to be of type [float].

    Appropriate functions from the [math] module are assumed to be in scope.
    *)
val to_string_python : s -> string

(** [to_string_numpy expr] is [expr] rendered as a Python expression using NumPy
    function, taking all symbols to be of type [numpy.float64].

    The [numpy] module is assumed to be in scope as [np].
    *)
val to_string_numpy : s -> string

