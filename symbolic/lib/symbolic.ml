type s =
  | Literal of float
  | Symbol of string
  | Neg of s
  | Add of s * s
  | Sub of s * s
  | Mul of s * s
  | Div of s * s
  | Sqrt of s
  | Sin of s
  | Cos of s
  | Tan of s
  | ASin of s
  | ACos of s
  | ATan of s
  | Sinh of s
  | Cosh of s
  | Tanh of s
  | ASinh of s
  | ACosh of s
  | ATanh of s
  | Pow of s * s
  | Exp of s
  | Log of s * s
  | Log2 of s
  | Log10 of s
  | Ln of s

module Consts = struct
  let e = Literal 2.718281828459045
  let frac_1_pi = Literal 0.3183098861837907
  let frac_1_sqrt_2 = Literal 0.7071067811865476
  let frac_2_pi = Literal 0.6366197723675814
  let frac_2_sqrt_pi = Literal 1.1283791670955126
  let frac_pi_2 = Literal 1.5707963267948966
  let frac_pi_3 = Literal 1.0471975511965979
  let frac_pi_4 = Literal 0.7853981633974483
  let frac_pi_6 = Literal 0.5235987755982989
  let frac_pi_8 = Literal 0.39269908169872414
  let ln_2 = Literal 0.6931471805599453
  let ln_10 = Literal 2.302585092994046
  let log2_10 = Literal 3.321928094887362
  let log2_e = Literal 1.4426950408889634
  let log10_2 = Literal 0.3010299956639812
  let log10_e = Literal 0.4342944819032518
  let pi = Literal 3.141592653589793
  let sqrt_2 = Literal 1.4142135623730951
  let tau = Literal 6.283185307179586
end

let rec eq (l: s) (r: s): bool =
  match (l, r) with
  | (Literal l, Literal r) -> l = r
  | (Symbol l, Symbol r) -> l = r
  | (Neg l, Neg r) -> eq l r
  | (Add (ll, lr), Add (rl, rr)) -> (eq ll rl) && (eq lr rr)
  | (Sub (ll, lr), Sub (rl, rr)) -> (eq ll rl) && (eq lr rr)
  | (Mul (ll, lr), Mul (rl, rr)) -> (eq ll rl) && (eq lr rr)
  | (Div (ll, lr), Div (rl, rr)) -> (eq ll rl) && (eq lr rr)
  | (Sqrt l, Sqrt r) -> eq l r
  | (Sin l, Sin r) -> eq l r
  | (Cos l, Cos r) -> eq l r
  | (Tan l, Tan r) -> eq l r
  | (ASin l, ASin r) -> eq l r
  | (ACos l, ACos r) -> eq l r
  | (ATan l, ATan r) -> eq l r
  | (Sinh l, Sinh r) -> eq l r
  | (Cosh l, Cosh r) -> eq l r
  | (Tanh l, Tanh r) -> eq l r
  | (ASinh l, ASinh r) -> eq l r
  | (ACosh l, ACosh r) -> eq l r
  | (ATanh l, ATanh r) -> eq l r
  | (Pow (bl, el), Pow (br, er)) -> (eq bl br) && (eq el er)
  | (Exp l, Exp r) -> eq l r
  | (Log (bl, xl), Log (br, xr)) -> (eq bl br) && (eq xl xr)
  | (Log2 l, Log2 r) -> eq l r
  | (Log10 l, Log10 r) -> eq l r
  | (Ln l, Ln r) -> eq l r
  | _ -> false

let ne (l: s) (r: s): bool = not (eq l r)

let compare (l: s) (r: s): int option =
  match (l, r) with
  | (Literal lf, Literal rf) -> Some (compare lf rf)
  | (l, r) -> if eq l r then Some 0 else None

let lt (l: s) (r: s): bool =
  match compare l r with
  | Some c -> c < 0
  | None -> false

let le (l: s) (r: s): bool =
  match compare l r with
  | Some c -> c <= 0
  | None -> false

let gt (l: s) (r: s): bool =
  match compare l r with
  | Some c -> c > 0
  | None -> false

let ge (l: s) (r: s): bool =
  match compare l r with
  | Some c -> c >= 0
  | None -> false

let rec neg (x: s): s =
  match x with
  | Literal f -> Literal (-.f)
  | Neg x -> x
  | Mul (Neg l, r) -> mul l r
  | Mul (l, Neg r) -> mul l r
  | Div (Neg l, r) -> div l r
  | Div (l, Neg r) -> div l r
  | x -> Neg x
and negf (x: float): s = neg (Literal x)
and add (l: s) (r: s): s =
  match (l, r) with
  | (Literal l, Literal r) -> Literal (l +. r)
  | (l, Literal r) when r = 0.0 -> l
  | (Literal l, r) when l = 0.0 -> r
  | (Mul (ll, lr), Mul (rl, rr)) when eq ll rl -> Mul (add lr rr, ll)
  | (Mul (ll, lr), Mul (rl, rr)) when eq ll rr -> Mul (add lr rl, ll)
  | (Mul (ll, lr), Mul (rl, rr)) when eq lr rl -> Mul (add ll rr, lr)
  | (Mul (ll, lr), Mul (rl, rr)) when eq lr rr -> Mul (add ll rl, lr)
  | (Mul (ll, lr), Mul (rl, rr)) -> Add (mul ll lr, mul rl rr)
  | (l, Mul (rl, rr)) when eq l rl -> Mul (add rr (Literal 1.0), l)
  | (l, Mul (rl, rr)) when eq l rr -> Mul (add rl (Literal 1.0), l)
  | (l, Mul (rl, rr)) -> Add (l, mul rl rr)
  | (Mul (ll, lr), r) when eq ll r -> Mul (add lr (Literal 1.0), r)
  | (Mul (ll, lr), r) when eq lr r -> Mul (add ll (Literal 1.0), r)
  | (Mul (ll, lr), r) -> Mul (mul ll lr, r)
  | (Neg l, r) -> sub r l
  | (l, Neg r) -> sub l r
  | (l, r) when eq l r -> Mul (Literal 2.0, l)
  | (l, r) -> Add (l, r)
and fadd (l: float) (r: s): s = add (Literal l) r
and addf (l: s) (r: float): s = add l (Literal r)
and faddf (l: float) (r: float): s = add (Literal l) (Literal r)
and sub (l: s) (r: s): s =
  match (l, r) with
  | (Literal l, Literal r) -> Literal (l -. r)
  | (l, Literal r) when r = 0.0 -> l
  | (Literal l, r) when l = 0.0 -> neg r
  | (Mul (ll, lr), Mul (rl, rr)) when eq ll rl -> Mul (sub lr rr, ll)
  | (Mul (ll, lr), Mul (rl, rr)) when eq ll rr -> Mul (sub lr rl, ll)
  | (Mul (ll, lr), Mul (rl, rr)) when eq lr rl -> Mul (sub ll rr, lr)
  | (Mul (ll, lr), Mul (rl, rr)) when eq lr rr -> Mul (sub ll rl, lr)
  | (Mul (ll, lr), Mul (rl, rr)) -> Sub (mul ll lr, mul rl rr)
  | (l, Mul (rl, rr)) when eq l rl -> Mul (sub (Literal 1.0) rr, l)
  | (l, Mul (rl, rr)) when eq l rr -> Mul (sub (Literal 1.0) rl, l)
  | (l, Mul (rl, rr)) -> Sub (l, mul rl rr)
  | (Mul (ll, lr), r) when eq ll r -> Mul (sub lr (Literal 1.0), r)
  | (Mul (ll, lr), r) when eq lr r -> Mul (sub ll (Literal 1.0), r)
  | (Mul (ll, lr), r) -> Sub (mul ll lr, r)
  | (l, Neg r) -> add l r
  | (l, r) when eq l r -> Literal 0.0
  | (l, r) -> Sub (l, r)
and fsub (l: float) (r: s): s = sub (Literal l) r
and subf (l: s) (r: float): s = sub l (Literal r)
and fsubf (l: float) (r: float): s = sub (Literal l) (Literal r)
and mul (l: s) (r: s): s =
  match (l, r) with
  | (Literal l, Literal r) -> Literal (l *. r)
  | (_, Literal r) when r = 0.0 -> Literal 0.0
  | (l, Literal r) when r = 1.0 -> l
  | (l, Literal r) when r = -1.0 -> neg l
  | (l, Literal r) -> Mul (Literal r, l)
  | (Literal l, _) when l = 0.0 -> Literal 0.0
  | (Literal l, r) when l = 1.0 -> r
  | (Literal l, r) when l = -1.0 -> neg r
  | (Literal l, r) -> Mul (Literal l, r)
  | (Neg l, Neg r) -> mul l r
  | (l, Neg r) -> neg (mul l r)
  | (Neg l, r) -> neg (mul l r)
  | (Exp el, Exp er) -> exp (add el er)
  | (Exp el, Pow (br, er)) when eq br Consts.e -> exp (add el er)
  | (Exp el, Pow (br, er)) -> Mul (exp el, pow br er)
  | (Pow (bl, el), Exp er) when eq bl Consts.e -> exp (add el er)
  | (Pow (bl, el), Exp er) -> Mul (pow bl el, exp er)
  | (Pow (bl, el), Pow (br, er)) when eq bl br -> pow bl (add el er)
  | (Pow (bl, el), Pow (br, er)) -> Mul (pow bl el, pow br er)
  | (l, Pow (br, er)) when eq l br -> pow br (add (Literal 1.0) er)
  | (l, Pow (br, er)) -> Mul (l, pow br er)
  | (Pow (bl, el), r) when eq bl r -> pow bl (add el (Literal 1.0))
  | (Pow (bl, el), r) -> Mul (pow bl el, r)
  | (l, Div (rl, rr)) when eq l rr -> rl
  | (l, Div (rl, rr)) -> Mul (l, div rl rr)
  | (Div (ll, lr), r) when eq lr r -> ll
  | (Div (ll, lr), r) -> Mul (div ll lr, r)
  | (l, r) when eq l r -> pow l (Literal 2.0)
  | (l, r) -> Mul (l, r)
and fmul (l: float) (r: s): s = mul (Literal l) r
and mulf (l: s) (r: float): s = mul l (Literal r)
and fmulf (l: float) (r: float): s = mul (Literal l) (Literal r)
and div (l: s) (r: s): s =
  match (l, r) with
  | (Literal l, Literal r) -> Literal (l /. r)
  | (l, Literal r) when r = 1.0 -> l
  | (l, Literal r) when r = -1.0 -> neg l
  | (l, Literal r) -> Div (l, Literal r)
  | (Literal l, _) when l = 0.0 -> Literal 0.0
  | (Literal l, r) -> Div (Literal l, r)
  | (Neg l, Neg r) -> div l r
  | (l, Neg r) -> neg (div l r)
  | (Neg l, r) -> neg (div l r)
  | (Exp el, Exp er) -> exp (sub el er)
  | (Exp el, Pow (br, er)) when eq Consts.e br -> exp (sub el er)
  | (Exp el, Pow (br, er)) -> Div (exp el, pow br er)
  | (Pow (bl, el), Exp er) when eq bl Consts.e -> exp (sub el er)
  | (Pow (bl, _ ), Exp er) -> Div (pow bl er, exp er)
  | (Pow (bl, el), Pow (br, er)) when eq bl br -> pow bl (sub el er)
  | (Pow (bl, el), Pow (br, er)) -> Div (pow bl el, pow br er)
  | (l, Pow (br, er)) when eq l br -> pow br (sub (Literal 1.0) er)
  | (l, Pow (br, er)) -> Div (l, pow br er)
  | (Pow (bl, el), r) when eq bl r -> pow bl (sub el (Literal 1.0))
  | (Pow (bl, el), r) -> Div (pow bl el, r)
  | (l, Mul (rl, rr)) when eq l rl -> div (Literal 1.0) rr
  | (l, Mul (rl, rr)) when eq l rr -> div (Literal 1.0) rl
  | (l, Mul (rl, rr)) -> Div (l, mul rl rr)
  | (l, Div (rl, rr)) -> div (mul l rr) rl
  | (Mul (ll, lr), r) when eq ll r -> lr
  | (Mul (ll, lr), r) when eq lr r -> ll
  | (Mul (ll, lr), r) -> Div (mul ll lr, r)
  | (Div (ll, lr), r) when eq ll r -> div (Literal 1.0) lr
  | (Div (ll, lr), r) -> Div (ll, mul lr r)
  | (l, r) when eq l r -> Literal 1.0
  | (l, r) -> Div (l, r)
and fdiv (l: float) (r: s): s = div (Literal l) r
and divf (l: s) (r: float): s = div l (Literal r)
and fdivf (l: float) (r: float): s = div (Literal l) (Literal r)
and sqrt (x: s): s =
  match x with
  | Literal x -> Literal (Float.sqrt x)
  | x -> Sqrt x
and sqrtf (x: float): s = sqrt (Literal x)
and sin (x: s): s =
  match x with
  | Literal x -> Literal (Float.sin x)
  | x -> Sin x
and sinf (x: float): s = sin (Literal x)
and cos (x: s): s =
  match x with
  | Literal x -> Literal (Float.cos x)
  | x -> Cos x
and cosf (x: float): s = cos (Literal x)
and tan (x: s): s =
  match x with
  | Literal x -> Literal (Float.tan x)
  | x -> Tan x
and tanf (x: float): s = tan (Literal x)
and asin (x: s): s =
  match x with
  | Literal x -> Literal (Float.asin x)
  | x -> ASin x
and asinf (x: float): s = asin (Literal x)
and acos (x: s): s =
  match x with
  | Literal x -> Literal (Float.acos x)
  | x -> ACos x
and acosf (x: float): s = acos (Literal x)
and atan (x: s): s =
  match x with
  | Literal x -> Literal (Float.atan x)
  | x -> ATan x
and atanf (x: float): s = atan (Literal x)
and sinh (x: s): s =
  match x with
  | Literal x -> Literal (Float.sinh x)
  | x -> Sinh x
and sinhf (x: float): s = sinh (Literal x)
and cosh (x: s): s =
  match x with
  | Literal x -> Literal (Float.cosh x)
  | x -> Cosh x
and coshf (x: float): s = cosh (Literal x)
and tanh (x: s): s =
  match x with
  | Literal x -> Literal (Float.tanh x)
  | x -> Tanh x
and tanhf (x: float): s = tanh (Literal x)
and asinh (x: s): s =
  match x with
  | Literal x -> Literal (Float.asinh x)
  | x -> ASinh x
and asinhf (x: float): s = asinh (Literal x)
and acosh (x: s): s =
  match x with
  | Literal x -> Literal (Float.acosh x)
  | x -> ACosh x
and acoshf (x: float): s = acosh (Literal x)
and atanh (x: s): s =
  match x with
  | Literal x -> Literal (Float.atanh x)
  | x -> ATanh x
and atanhf (x: float): s = atanh (Literal x)
and pow (b: s) (e: s): s =
  match (b, e) with
  | (Literal b, Literal e) -> Literal (b ** e)
  | (Literal b, e) when eq (Literal b) Consts.e -> exp e
  | (Literal b, e) -> Pow (Literal b, e)
  | (Sqrt b, e) -> pow b (div e (Literal 2.0))
  | (b, Log (eb, ex)) when eq b eb -> ex
  | (b, Log (eb, ex)) -> Pow (b, log eb ex)
  | (b, Log2 ex) when eq b (Literal 2.0) -> ex
  | (b, Log2 ex) -> Pow (b, log2 ex)
  | (b, Ln ex) when eq b Consts.e -> ex
  | (b, Ln ex) -> Pow (b, ln ex)
  | (b, Log10 ex) when eq b (Literal 10.0) -> ex
  | (b, Log10 ex) -> Pow (b, log10 ex)
  | (_, Literal e) when e = 0.0 -> Literal 1.0
  | (b, Literal e) when e = 0.5 -> sqrt b
  | (b, Literal e) when e = 1.0 -> b
  | (b, Literal e) when e = -1.0 -> div (Literal 1.0) b
  | (b, Literal e) when e = -0.5 -> div (Literal 1.0) (sqrt b)
  | (b, Literal e) -> Pow (b, Literal e)
  | (b, e) -> Pow (b, e)
and fpow (b: float) (e: s): s = pow (Literal b) e
and powf (b: s) (e: float): s = pow b (Literal e)
and fpowf (b: float) (e: float): s = pow (Literal b) (Literal e)
and exp (x: s): s =
  match x with
  | Literal x -> Literal (Float.exp x)
  | Ln e -> e
  | x -> Exp x
and expf (x: float): s = exp (Literal x)
and log (b: s) (x: s): s =
  match (b, x) with
  | (Literal b, Literal x) -> Literal ((Float.log x) /. (Float.log b))
  | (Literal b, x) when b = 2.0 -> log2 x
  | (Literal b, x) when Literal b = Consts.e -> ln x
  | (Literal b, x) when b = 10.0 -> log10 x
  | (Literal b, x) -> Log (Literal b, x)
  | (b, Sqrt x) -> mul (Literal 0.5) (log b x)
  | (b, Pow (bb, be)) when eq bb b -> be
  | (b, Pow (bb, be)) -> Log (b, pow bb be)
  | (b, Exp be) when eq Consts.e b -> be
  | (b, Exp be) -> Log (b, exp be)
  | (b, e) -> Log (b, e)
and flog (b: float) (x: s): s = log (Literal b) x
and logf (b: s) (x: float): s = log b (Literal x)
and flogf (b: float) (x: float): s = log (Literal b) (Literal x)
and log2 (x: s): s =
  match x with
  | Literal x -> Literal (Float.log2 x)
  | Sqrt x -> mul (Literal 0.5) (log2 x)
  | Pow (b, e) when eq b (Literal 2.0) -> e
  | Pow (b, e) -> Log2 (pow b e)
  | x -> Log2 x
and log2f (x: float): s = log2 (Literal x)
and log10 (x: s): s =
  match x with
  | Literal x -> Literal (Float.log10 x)
  | Sqrt x -> mul (Literal 0.5) (log10 x)
  | Pow (b, e) when eq b (Literal 10.0) -> e
  | Pow (b, e) -> Log10 (pow b e)
  | x -> Log10 x
and log10f (x: float): s = log10 (Literal x)
and ln (x: s): s =
  match x with
  | Literal x -> Literal (Float.log x)
  | Sqrt x -> mul (Literal 0.5) (ln x)
  | Pow (b, e) when eq b Consts.e -> e
  | Pow (b, e) -> Ln (pow b e)
  | Exp e -> e
  | x -> Ln x
and lnf (x: float): s = ln (Literal x)

let sum (terms: s list): s =
  let rec sum_inner (acc: s) (terms: s list): s =
    match terms with
      | [] -> acc
      | first :: rest ->
          if eq acc (Literal 0.0) then
            sum_inner first rest
          else
            sum_inner (add acc first) rest
  in
  sum_inner (Literal 0.0) terms

let prod (factors: s list): s =
  let rec prod_inner (acc: s) (factors: s list): s =
    match factors with
    | [] -> acc
    | first ::rest ->
        if eq acc (Literal 1.0) then
          prod_inner first rest
        else
          prod_inner (mul acc first) rest
  in
  prod_inner (Literal 1.0) factors

let is_literal (expr: s): bool =
  match expr with
  | Literal _ -> true
  | _ -> false

let is_symbol (expr: s): bool =
  match expr with
  | Symbol _ -> true
  | _ -> false

let is_neg (expr: s): bool =
  match expr with
  | Neg _ -> true
  | _ -> false

let is_add (expr: s): bool =
  match expr with
  | Add _ -> true
  | _ -> false

let is_sub (expr: s): bool =
  match expr with
  | Sub _ -> true
  | _ -> false

let is_mul (expr: s): bool =
  match expr with
  | Mul _ -> true
  | _ -> false

let is_div (expr: s): bool =
  match expr with
  | Div _ -> true
  | _ -> false

let is_sqrt (expr: s): bool =
  match expr with
  | Sqrt _ -> true
  | _ -> false

let is_sin (expr: s): bool =
  match expr with
  | Sin _ -> true
  | _ -> false

let is_cos (expr: s): bool =
  match expr with
  | Cos _ -> true
  | _ -> false

let is_tan (expr: s): bool =
  match expr with
  | Tan _ -> true
  | _ -> false

let is_asin (expr: s): bool =
  match expr with
  | ASin _ -> true
  | _ -> false

let is_acos (expr: s): bool =
  match expr with
  | ACos _ -> true
  | _ -> false

let is_atan (expr: s): bool =
  match expr with
  | ATan _ -> true
  | _ -> false

let is_sinh (expr: s): bool =
  match expr with
  | Sinh _ -> true
  | _ -> false

let is_cosh (expr: s): bool =
  match expr with
  | Cosh _ -> true
  | _ -> false

let is_tanh (expr: s): bool =
  match expr with
  | Tanh _ -> true
  | _ -> false

let is_asinh (expr: s): bool =
  match expr with
  | ASinh _ -> true
  | _ -> false

let is_acosh (expr: s): bool =
  match expr with
  | ACosh _ -> true
  | _ -> false

let is_atanh (expr: s): bool =
  match expr with
  | ATanh _ -> true
  | _ -> false

let is_pow (expr: s): bool =
  match expr with
  | Pow (_, _) -> true
  | _ -> false

let is_exp (expr: s): bool =
  match expr with
  | Exp _ -> true
  | _ -> false

let is_log (expr: s): bool =
  match expr with
  | Log (_, _) -> true
  | _ -> false

let is_log2 (expr: s): bool =
  match expr with
  | Log2 _ -> true
  | _ -> false

let is_log10 (expr: s): bool =
  match expr with
  | Log10 _ -> true
  | _ -> false

let is_ln (expr: s): bool =
  match expr with
  | Ln _ -> true
  | _ -> false

let is_atom (expr: s): bool = is_literal expr || is_symbol expr

let rec contains (expr: s) (subexpr: s): bool =
  if eq expr subexpr then
    true
  else
    match expr with
    | Literal _ -> false
    | Symbol _ -> false
    | Neg x -> contains x subexpr
    | Add (l, r) -> contains l subexpr || contains r subexpr
    | Sub (l, r) -> contains l subexpr || contains r subexpr
    | Mul (l, r) -> contains l subexpr || contains r subexpr
    | Div (l, r) -> contains l subexpr || contains r subexpr
    | Sqrt x -> contains x subexpr
    | Sin x -> contains x subexpr
    | Cos x -> contains x subexpr
    | Tan x -> contains x subexpr
    | ASin x -> contains x subexpr
    | ACos x -> contains x subexpr
    | ATan x -> contains x subexpr
    | Sinh x -> contains x subexpr
    | Cosh x -> contains x subexpr
    | Tanh x -> contains x subexpr
    | ASinh x -> contains x subexpr
    | ACosh x -> contains x subexpr
    | ATanh x -> contains x subexpr
    | Pow (b, e) -> contains b subexpr || contains e subexpr
    | Exp x -> contains x subexpr
    | Log (b, x) -> contains b subexpr || contains x subexpr
    | Log2 x -> contains x subexpr
    | Log10 x -> contains x subexpr
    | Ln x -> contains x subexpr

let get_symbols (expr: s): s list =
  let rec get_symbols_inner (acc: s list) (expr: s): s list =
    match expr with
    | Literal _ -> acc
    | Symbol _ -> if List.mem expr acc then acc else acc @ [expr]
    | Neg x -> get_symbols_inner acc x
    | Add (l, r) -> get_symbols_inner (get_symbols_inner acc l) r
    | Sub (l, r) -> get_symbols_inner (get_symbols_inner acc l) r
    | Mul (l, r) -> get_symbols_inner (get_symbols_inner acc l) r
    | Div (l, r) -> get_symbols_inner (get_symbols_inner acc l) r
    | Sqrt x -> get_symbols_inner acc x
    | Sin x -> get_symbols_inner acc x
    | Cos x -> get_symbols_inner acc x
    | Tan x -> get_symbols_inner acc x
    | ASin x -> get_symbols_inner acc x
    | ACos x -> get_symbols_inner acc x
    | ATan x -> get_symbols_inner acc x
    | Sinh x -> get_symbols_inner acc x
    | Cosh x -> get_symbols_inner acc x
    | Tanh x -> get_symbols_inner acc x
    | ASinh x -> get_symbols_inner acc x
    | ACosh x -> get_symbols_inner acc x
    | ATanh x -> get_symbols_inner acc x
    | Pow (b, e) -> get_symbols_inner (get_symbols_inner acc b) e
    | Exp x -> get_symbols_inner acc x
    | Log (b, x) -> get_symbols_inner (get_symbols_inner acc b) x
    | Log2 x -> get_symbols_inner acc x
    | Log10 x -> get_symbols_inner acc x
    | Ln x -> get_symbols_inner acc x
  in
  get_symbols_inner [] expr

let get_atoms (expr: s): s list =
  let rec get_atoms_inner (acc: s list) (expr: s): s list =
    match expr with
    | Literal _ -> if List.mem expr acc then acc else acc @ [expr]
    | Symbol _ -> if List.mem expr acc then acc else acc @ [expr]
    | Neg x -> get_atoms_inner acc x
    | Add (l, r) -> get_atoms_inner (get_atoms_inner acc l) r
    | Sub (l, r) -> get_atoms_inner (get_atoms_inner acc l) r
    | Mul (l, r) -> get_atoms_inner (get_atoms_inner acc l) r
    | Div (l, r) -> get_atoms_inner (get_atoms_inner acc l) r
    | Sqrt x -> get_atoms_inner acc x
    | Sin x -> get_atoms_inner acc x
    | Cos x -> get_atoms_inner acc x
    | Tan x -> get_atoms_inner acc x
    | ASin x -> get_atoms_inner acc x
    | ACos x -> get_atoms_inner acc x
    | ATan x -> get_atoms_inner acc x
    | Sinh x -> get_atoms_inner acc x
    | Cosh x -> get_atoms_inner acc x
    | Tanh x -> get_atoms_inner acc x
    | ASinh x -> get_atoms_inner acc x
    | ACosh x -> get_atoms_inner acc x
    | ATanh x -> get_atoms_inner acc x
    | Pow (b, e) -> get_atoms_inner (get_atoms_inner acc b) e
    | Exp x -> get_atoms_inner acc x
    | Log (b, x) -> get_atoms_inner (get_atoms_inner acc b) x
    | Log2 x -> get_atoms_inner acc x
    | Log10 x -> get_atoms_inner acc x
    | Ln x -> get_atoms_inner acc x
  in
  get_atoms_inner [] expr

let rec diff (expr: s) (var: s): s =
  let dv x = diff x var in
  if eq expr var then
    Literal 1.0
  else if not (contains expr var) then
    Literal 0.0
  else
    match expr with
    | Literal _ -> Literal 0.0
    | Symbol x ->
        if eq (Symbol x) var then
          Literal 1.0
        else
          Literal 0.0
    | Neg x -> neg (dv x)
    | Add (l, r) -> add (dv l) (dv r)
    | Sub (l, r) -> sub (dv l) (dv r)
    | Mul (l, r) -> add (mul (dv l) r) (mul (dv r) l)
    | Div (l, r) ->
        sub
          (div (dv l) r)
          (mul (dv r) (div l (pow r (Literal 2.0))))
    | Sqrt x -> div (dv x) (mul (Literal 2.0) (sqrt x))
    | Sin x -> mul (dv x) (cos x)
    | Cos x -> neg (mul (dv x) (sin x))
    | Tan x -> div (dv x) (pow (cos x) (Literal 2.0))
    | ASin x -> div (dv x) (sqrt (sub (Literal 1.0) (pow x (Literal 2.0))))
    | ACos x ->
        neg (div (dv x) (sqrt (sub (Literal 1.0) (pow x (Literal 2.0)))))
    | ATan x -> div (dv x) (add (Literal 1.0) (pow x (Literal 2.0)))
    | Sinh x -> mul (dv x) (cosh x)
    | Cosh x -> mul (dv x) (sinh x)
    | Tanh x -> mul (dv x) (sub (Literal 1.0) (pow (tanh x) (Literal 2.0)))
    | ASinh x -> div (dv x) (sqrt (add (pow x (Literal 2.0)) (Literal 1.0)))
    | ACosh x -> div (dv x) (sqrt (sub (pow x (Literal 2.0)) (Literal 1.0)))
    | ATanh x -> neg (div (dv x) (sub (pow x (Literal 2.0)) (Literal 1.0)))
    | Pow (b, e) ->
        mul
          (add (mul (dv e) (ln b)) (mul (dv b) (div e b)))
          (pow b e)
    | Exp x -> mul (dv x) (exp x)
    | Log (b, x) ->
        add
          (neg (div (dv b) (mul b (pow (ln b) (Literal 2.0)))))
          (div (dv x) (mul x (ln b)))
    | Log2 x -> div (dv x) (mul Consts.ln_2 x)
    | Log10 x -> div (dv x) (mul Consts.ln_10 x)
    | Ln x -> div (dv x) x

let rec reval (expr: s): s =
  match expr with
  | Literal x -> Literal x
  | Symbol x -> Symbol x
  | Neg x -> neg (reval x)
  | Add (l, r) -> add (reval l) (reval r)
  | Sub (l, r) -> sub (reval l) (reval r)
  | Mul (l, r) -> mul (reval l) (reval r)
  | Div (l, r) -> div (reval l) (reval r)
  | Sqrt x -> sqrt (reval x)
  | Sin x -> sin (reval x)
  | Cos x -> cos (reval x)
  | Tan x -> tan (reval x)
  | ASin x -> asin (reval x)
  | ACos x -> acos (reval x)
  | ATan x -> atan (reval x)
  | Sinh x -> sinh (reval x)
  | Cosh x -> cosh (reval x)
  | Tanh x -> tanh (reval x)
  | ASinh x -> asinh (reval x)
  | ACosh x -> acosh (reval x)
  | ATanh x -> atanh (reval x)
  | Pow (b, e) -> pow (reval b) (reval e)
  | Exp x -> exp (reval x)
  | Log (b, x) -> log (reval b) (reval x)
  | Log2 x -> log2 (reval x)
  | Log10 x -> log10 (reval x)
  | Ln x -> ln (reval x)

let rec subs_single (target: s) (replace: s) (expr: s): s =
  let do_subs = subs_single target replace in
  if eq expr target then
    replace
  else
    match expr with
    | Literal x -> Literal x
    | Symbol x -> Symbol x
    | Neg x -> neg (do_subs x)
    | Add (l, r) -> add (do_subs l) (do_subs r)
    | Sub (l, r) -> sub (do_subs l) (do_subs r)
    | Mul (l, r) -> mul (do_subs l) (do_subs r)
    | Div (l, r) -> div (do_subs l) (do_subs r)
    | Sqrt x -> sqrt (do_subs x)
    | Sin x -> sin (do_subs x)
    | Cos x -> cos (do_subs x)
    | Tan x -> tan (do_subs x)
    | ASin x -> asin (do_subs x)
    | ACos x -> acos (do_subs x)
    | ATan x -> atan (do_subs x)
    | Sinh x -> sinh (do_subs x)
    | Cosh x -> cosh (do_subs x)
    | Tanh x -> tanh (do_subs x)
    | ASinh x -> asinh (do_subs x)
    | ACosh x -> acosh (do_subs x)
    | ATanh x -> atanh (do_subs x)
    | Pow (b, e) -> pow (do_subs b) (do_subs e)
    | Exp x -> exp (do_subs x)
    | Log (b, x) -> log (do_subs b) (do_subs x)
    | Log2 x -> log2 (do_subs x)
    | Log10 x -> log10 (do_subs x)
    | Ln x -> ln (do_subs x)

let rec subs (ss: (s * s) list) (expr: s): s =
  match ss with
  | [] -> expr
  | first :: rest -> subs rest (subs_single (fst first) (snd first) expr)

let rec subsf_single (target: s) (replace: float) (expr: s): s =
  let do_subs = subsf_single target replace in
  if eq expr target then
    Literal replace
  else
    match expr with
    | Literal x -> Literal x
    | Symbol x -> Symbol x
    | Neg x -> neg (do_subs x)
    | Add (l, r) -> add (do_subs l) (do_subs r)
    | Sub (l, r) -> sub (do_subs l) (do_subs r)
    | Mul (l, r) -> mul (do_subs l) (do_subs r)
    | Div (l, r) -> div (do_subs l) (do_subs r)
    | Sqrt x -> sqrt (do_subs x)
    | Sin x -> sin (do_subs x)
    | Cos x -> cos (do_subs x)
    | Tan x -> tan (do_subs x)
    | ASin x -> asin (do_subs x)
    | ACos x -> acos (do_subs x)
    | ATan x -> atan (do_subs x)
    | Sinh x -> sinh (do_subs x)
    | Cosh x -> cosh (do_subs x)
    | Tanh x -> tanh (do_subs x)
    | ASinh x -> asinh (do_subs x)
    | ACosh x -> acosh (do_subs x)
    | ATanh x -> atanh (do_subs x)
    | Pow (b, e) -> pow (do_subs b) (do_subs e)
    | Exp x -> exp (do_subs x)
    | Log (b, x) -> log (do_subs b) (do_subs x)
    | Log2 x -> log2 (do_subs x)
    | Log10 x -> log10 (do_subs x)
    | Ln x -> ln (do_subs x)

let rec subsf (ss: (s * float) list) (expr: s): s =
  match ss with
  | [] -> expr
  | first :: rest -> subsf rest (subsf_single (fst first) (snd first) expr)

let evalf_single
  (target: s)
  (replace: float)
  (expr: s)
  : (float, s list) result
=
  match subsf_single target replace expr with
  | Literal x -> Ok x
  | x -> Error (get_symbols x)

let evalf (ss: (s * float) list) (expr: s): (float, s list) result =
  match subsf ss expr with
  | Literal x -> Ok x
  | x -> Error (get_symbols x)

let to_string (expr: s): string =
  let rec with_parens_if (x: s) (tests: (s -> bool) list): string =
    if List.fold_left (fun acc f -> acc || (f x)) false tests then
      "(" ^ (to_string_inner "" x) ^ ")"
    else
      to_string_inner "" x
  and display_function (fn_name: string) (x: s): string =
    fn_name ^ "(" ^ (to_string_inner "" x) ^ ")"
  and to_string_inner (acc: string) (x: s): string =
    let x_str =
      match x with
      | Literal x -> Float.to_string x
      | Symbol x -> x
      | Neg x -> "-" ^ (with_parens_if x [is_add; is_sub; is_neg])
      | Add (l, Neg r) ->
          (to_string_inner "" l)
          ^ " - "
          ^ (with_parens_if r [is_add; is_sub; is_neg])
      | Add (l, r) ->
          (to_string_inner "" l)
          ^ " + "
          ^ (with_parens_if r [is_neg])
      | Sub (l, Neg r) ->
          (to_string_inner "" l)
          ^ " + "
          ^ (with_parens_if r [is_neg])
      | Sub (l, r) ->
          (to_string_inner "" l)
          ^ " - "
          ^ (with_parens_if r [is_add; is_sub; is_neg])
      | Mul (l, r) ->
          (with_parens_if l [is_add; is_sub])
          ^ " * "
          ^ (with_parens_if r [is_add; is_sub])
      | Div (l, r) ->
          (with_parens_if l [is_add; is_sub])
          ^ " / "
          ^ (with_parens_if r [is_add; is_sub; is_mul; is_div])
      | Sqrt x -> display_function "√" x
      | Sin x -> display_function "sin" x
      | Cos x -> display_function "cos" x
      | Tan x -> display_function "tan" x
      | ASin x -> display_function "arcsin" x
      | ACos x -> display_function "arccos" x
      | ATan x -> display_function "arctan" x
      | Sinh x -> display_function "sinh" x
      | Cosh x -> display_function "cosh" x
      | Tanh x -> display_function "tanh" x
      | ASinh x -> display_function "arsinh" x
      | ACosh x -> display_function "arcosh" x
      | ATanh x -> display_function "artanh" x
      | Pow (b, e) ->
          (with_parens_if b [is_add; is_sub; is_mul; is_div; is_neg])
          ^ "^"
          ^ (with_parens_if e [is_add; is_sub; is_mul; is_div; is_neg])
      | Exp x -> display_function "exp" x
      | Log (b, x) ->
          "log_"
          ^ (with_parens_if b [is_add; is_sub; is_mul; is_div; is_neg])
          ^ (with_parens_if x [fun _ -> true])
      | Log2 x -> display_function "log2" x
      | Log10 x -> display_function "log10" x
      | Ln x -> display_function "ln" x
    in
    acc ^ x_str
  in
  to_string_inner "" expr

let to_string_rust (expr: s): string =
  let rec with_parens_if (x: s) (tests: (s -> bool) list): string =
    if List.fold_left (fun acc f -> acc || (f x)) false tests then
      "(" ^ (to_string_inner "" x) ^ ")"
    else
      to_string_inner "" x
  and display_function (fn_name: string) (x: s): string =
    (with_parens_if x [is_add; is_sub; is_mul; is_div; is_neg])
    ^ "." ^ fn_name ^ "()"
  and to_string_inner (acc: string) (x: s): string =
    let x_str =
      match x with
      | Literal x -> Float.to_string x
      | Symbol x -> x
      | Neg x -> "-" ^ (with_parens_if x [is_add; is_sub; is_neg])
      | Add (l, Neg r) ->
          (to_string_inner "" l)
          ^ " - "
          ^ (with_parens_if r [is_add; is_sub; is_neg])
      | Add (l, r) ->
          (to_string_inner "" l)
          ^ " + "
          ^ (with_parens_if r [is_neg])
      | Sub (l, Neg r) ->
          (to_string_inner "" l)
          ^ " + "
          ^ (with_parens_if r [is_neg])
      | Sub (l, r) ->
          (to_string_inner "" l)
          ^ " - "
          ^ (with_parens_if r [is_add; is_sub; is_neg])
      | Mul (l, r) ->
          (with_parens_if l [is_add; is_sub])
          ^ " * "
          ^ (with_parens_if r [is_add; is_sub])
      | Div (l, r) ->
          (with_parens_if l [is_add; is_sub])
          ^ " / "
          ^ (with_parens_if r [is_add; is_sub; is_mul; is_div])
      | Sqrt x -> display_function "sqrt" x
      | Sin x -> display_function "sin" x
      | Cos x -> display_function "cos" x
      | Tan x -> display_function "tan" x
      | ASin x -> display_function "asin" x
      | ACos x -> display_function "acos" x
      | ATan x -> display_function "atan" x
      | Sinh x -> display_function "sinh" x
      | Cosh x -> display_function "cosh" x
      | Tanh x -> display_function "tanh" x
      | ASinh x -> display_function "asinh" x
      | ACosh x -> display_function "acosh" x
      | ATanh x -> display_function "atanh" x
      | Pow (b, e) ->
          (with_parens_if b [is_add; is_sub; is_mul; is_div; is_neg])
          ^ ".powf(" ^ (to_string_inner "" e) ^ ")"
      | Exp x -> display_function "exp" x
      | Log (b, x) ->
          (with_parens_if x [is_add; is_sub; is_mul; is_div; is_neg])
          ^ ".log(" ^ (to_string_inner "" b) ^ ")"
      | Log2 x -> display_function "log2" x
      | Log10 x -> display_function "log10" x
      | Ln x -> display_function "ln" x
    in
    acc ^ x_str
  in
  to_string_inner "" expr

let to_string_python (expr: s): string =
  let rec with_parens_if (x: s) (tests: (s -> bool) list): string =
    if List.fold_left (fun acc f -> acc || (f x)) false tests then
      "(" ^ (to_string_inner "" x) ^ ")"
    else
      to_string_inner "" x
  and display_function (fn_name: string) (x: s): string =
    fn_name ^ "(" ^ (to_string_inner "" x) ^ ")"
  and to_string_inner (acc: string) (x: s): string =
    let x_str =
      match x with
      | Literal x -> Float.to_string x
      | Symbol x -> x
      | Neg x -> "-" ^ (with_parens_if x [is_add; is_sub; is_neg])
      | Add (l, Neg r) ->
          (to_string_inner "" l)
          ^ " - "
          ^ (with_parens_if r [is_add; is_sub; is_neg])
      | Add (l, r) ->
          (to_string_inner "" l)
          ^ " + "
          ^ (with_parens_if r [is_neg])
      | Sub (l, Neg r) ->
          (to_string_inner "" l)
          ^ " + "
          ^ (with_parens_if r [is_neg])
      | Sub (l, r) ->
          (to_string_inner "" l)
          ^ " - "
          ^ (with_parens_if r [is_add; is_sub; is_neg])
      | Mul (l, r) ->
          (with_parens_if l [is_add; is_sub])
          ^ " * "
          ^ (with_parens_if r [is_add; is_sub])
      | Div (l, r) ->
          (with_parens_if l [is_add; is_sub])
          ^ " / "
          ^ (with_parens_if r [is_add; is_sub; is_mul; is_div])
      | Sqrt x -> display_function "sqrt" x
      | Sin x -> display_function "sin" x
      | Cos x -> display_function "cos" x
      | Tan x -> display_function "tan" x
      | ASin x -> display_function "asin" x
      | ACos x -> display_function "acos" x
      | ATan x -> display_function "atan" x
      | Sinh x -> display_function "sinh" x
      | Cosh x -> display_function "cosh" x
      | Tanh x -> display_function "tanh" x
      | ASinh x -> display_function "asinh" x
      | ACosh x -> display_function "acosh" x
      | ATanh x -> display_function "atanh" x
      | Pow (b, e) ->
          (with_parens_if b [is_add; is_sub; is_mul; is_div; is_neg])
          ^ "**"
          ^ (with_parens_if e [is_add; is_sub; is_mul; is_div; is_neg])
      | Exp x -> display_function "exp" x
      | Log (b, x) ->
          "log("
          ^ (to_string_inner "" x)
          ^ ", "
          ^ (to_string_inner "" b)
          ^ ")"
      | Log2 x -> display_function "log2" x
      | Log10 x -> display_function "log10" x
      | Ln x -> display_function "log" x
    in
    acc ^ x_str
  in
  to_string_inner "" expr

let to_string_numpy (expr: s): string =
  let rec with_parens_if (x: s) (tests: (s -> bool) list): string =
    if List.fold_left (fun acc f -> acc || (f x)) false tests then
      "(" ^ (to_string_inner "" x) ^ ")"
    else
      to_string_inner "" x
  and display_function (fn_name: string) (x: s): string =
    "np." ^ fn_name ^ "(" ^ (to_string_inner "" x) ^ ")"
  and to_string_inner (acc: string) (x: s): string =
    let x_str =
      match x with
      | Literal x -> Float.to_string x
      | Symbol x -> x
      | Neg x -> "-" ^ (with_parens_if x [is_add; is_sub; is_neg])
      | Add (l, Neg r) ->
          (to_string_inner "" l)
          ^ " - "
          ^ (with_parens_if r [is_add; is_sub; is_neg])
      | Add (l, r) ->
          (to_string_inner "" l)
          ^ " + "
          ^ (with_parens_if r [is_neg])
      | Sub (l, Neg r) ->
          (to_string_inner "" l)
          ^ " + "
          ^ (with_parens_if r [is_neg])
      | Sub (l, r) ->
          (to_string_inner "" l)
          ^ " - "
          ^ (with_parens_if r [is_add; is_sub; is_neg])
      | Mul (l, r) ->
          (with_parens_if l [is_add; is_sub])
          ^ " * "
          ^ (with_parens_if r [is_add; is_sub])
      | Div (l, r) ->
          (with_parens_if l [is_add; is_sub])
          ^ " / "
          ^ (with_parens_if r [is_add; is_sub; is_mul; is_div; is_log])
      | Sqrt x -> display_function "sqrt" x
      | Sin x -> display_function "sin" x
      | Cos x -> display_function "cos" x
      | Tan x -> display_function "tan" x
      | ASin x -> display_function "arcsin" x
      | ACos x -> display_function "arccos" x
      | ATan x -> display_function "arctan" x
      | Sinh x -> display_function "sinh" x
      | Cosh x -> display_function "cosh" x
      | Tanh x -> display_function "tanh" x
      | ASinh x -> display_function "arcsinh" x
      | ACosh x -> display_function "arccosh" x
      | ATanh x -> display_function "arctanh" x
      | Pow (b, e) ->
          (with_parens_if b [is_add; is_sub; is_mul; is_div; is_neg; is_log])
          ^ "**"
          ^ (with_parens_if e [is_add; is_sub; is_mul; is_div; is_neg; is_log])
      | Exp x -> display_function "exp" x
      | Log (b, x) ->
          "np.log("
          ^ (to_string_inner "" x)
          ^ ") / np.log( "
          ^ (to_string_inner "" b)
          ^ ")"
      | Log2 x -> display_function "log2" x
      | Log10 x -> display_function "log10" x
      | Ln x -> display_function "log" x
    in
    acc ^ x_str
  in
  to_string_inner "" expr

