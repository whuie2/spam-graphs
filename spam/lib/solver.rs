//! Implements a numerical SPAM correction solver for the relevant variables
//! using multidimensional Newton-Raphson (N-R).

use ndarray as nd;
use nalgebra as na;
use ndarray_linalg::{ Determinant, Inverse, Norm };
use thiserror::Error;

#[derive(Debug, Error)]
pub enum SolverError {
    #[error("linalg error: '{0}'")]
    Linalg(#[from] ndarray_linalg::error::LinalgError),

    #[error("encountered non-invertible Jacobian")]
    NonInvertibleJacobian,

    #[error("failed to converge")]
    NoConverge,
}
pub type SolverResult<T> = Result<T, SolverError>;

/// Default N-R iteration limit.
pub const MAXITERS: usize = 1000;

/// Default N-R step size termination threshold.
pub const EPSILON: f64 = 1e-6;

/// Marker trait for data types holding variable data (i.e. numbers the N-R
/// solver can vary).
pub trait Variables
where Self: Into<nd::Array1<f64>> + From<nd::Array1<f64>> + Copy + Clone
{ }

impl<T> Variables for T
where T: Into<nd::Array1<f64>> + From<nd::Array1<f64>> + Copy + Clone
{ }

/// Like [`Variables`] but for conversion to a statically sized
/// [`SVector`][na::SMatrix].
pub trait VariablesStatic<const N: usize>
where Self: Into<na::SVector<f64, N>> + From<na::SVector<f64, N>> + Copy + Clone
{ }

impl<const N: usize, T> VariablesStatic<N> for T
where T: Into<na::SVector<f64, N>> + From<na::SVector<f64, N>> + Copy + Clone
{ }

/// Marker trait for data types holding invariant data (i.e. numbers the N-R
/// solver holds constant).
pub trait Constants
where Self: Copy + Clone
{ }

impl<T> Constants for T
where T: Copy + Clone
{ }

/// Implements a multidimensional Newton-Raphson solver.
///
/// `lhs` computes the left-hand side expressions implicitly set equal to zero.
/// `jacobian` computes the Jacobian matrix of all `lhs` expressions with
/// respect to all variables. `jacobian` must return a square matrix whose side
/// length must be equal to the length of the array returned by `lhs`.
///
/// Iteration is terminated if the computed stepsizes sum in quadrature to less
/// than `epsilon`, which defaults to [`EPSILON`]. See also [`MAXITERS`].
pub fn nr<V, C, F, J>(
    vars_init: V,
    consts: C,
    lhs: F,
    jacobian: J,
    maxiters: Option<usize>,
    epsilon: Option<f64>,
) -> SolverResult<V>
where
    V: Variables,
    C: Constants,
    F: Fn(V, C) -> nd::Array1<f64>,
    J: Fn(V, C) -> nd::Array2<f64>,
{
    let eps = epsilon.unwrap_or(EPSILON);
    let mut x: nd::Array1<f64> = vars_init.into();
    let mut dx: nd::Array1<f64>;
    let mut y: nd::Array1<f64>;
    let mut J: nd::Array2<f64>;
    for _ in 0..maxiters.unwrap_or(MAXITERS) {
        y = lhs(x.clone().into(), consts);
        J = jacobian(x.clone().into(), consts);
        if J.det()?.abs() < 1e-15 {
            return Err(SolverError::NonInvertibleJacobian);
        }
        dx = -J.inv()?.dot(&y);
        x += &dx;
        if dx.norm() < eps { return Ok(x.into()); }
    }
    Err(SolverError::NoConverge)
}

/// Like [`nr`], but using statically sized [`SVector`][na::SVector]s and
/// [`SMatrix`][na::SMatrix]s.
pub fn nr_static<const N: usize, V, C, F, J>(
    vars_init: V,
    consts: C,
    lhs: F,
    jacobian: J,
    maxiters: Option<usize>,
    epsilon: Option<f64>,
) -> SolverResult<V>
where
    V: VariablesStatic<N>,
    C: Constants,
    F: Fn(V, C) -> na::SVector<f64, N>,
    J: Fn(V, C) -> na::SMatrix<f64, N, N>,
{
    let eps = epsilon.unwrap_or(EPSILON);
    let mut x: na::SVector<f64, N> = vars_init.into();
    let mut dx: na::SVector<f64, N> = na::SVector::from_element(0.0);
    let mut y: na::SVector<f64, N>;
    let mut J: na::SMatrix<f64, N, N>;
    for _ in 0..maxiters.unwrap_or(MAXITERS) {
        y = lhs(x.into(), consts);
        J = jacobian(x.into(), consts);
        if !J.is_invertible() {
            return Err(SolverError::NonInvertibleJacobian);
        }
        J.try_inverse_mut();
        J.mul_to(&y, &mut dx);
        x -= &dx;
        if dx.norm() < eps { return Ok(x.into()); }
    }
    Err(SolverError::NoConverge)
}

