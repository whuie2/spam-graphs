//! Simple computer algebra system on the field of real numbers.
//!
//! [`S`] is the main symbolic expression type.

use std::{
    fmt,
    ops::{ Neg, Add, Sub, Mul, Div },
    rc::Rc,
};
use indexmap::IndexSet;
use ordered_float::OrderedFloat;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum SError {
    #[error("remaining unevaluated symbols: {0:?}")]
    Evalf(IndexSet<String>),
}
pub type SResult<T> = Result<T, SError>;

/// Represents an unevaluated expression on the field of real numbers.
///
/// Expressions can be numeric, symbolic, or a combination of the two through
/// a series of operations. This type implements [`Neg`], [`Add`], [`Sub`],
/// [`Mul`], and [`Div`] for owned and referenced values so that arithmetic
/// operations can be written as naturally as possible.
///
/// Ordering is implemented only between `S::Literal` values.
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum S {
    /// A literal real number. This variant is backed by an [`OrderedFloat`] so
    /// that [`Eq`] and [`Hash`] can be implemented.
    Literal(OrderedFloat<f64>),

    /// A symbol representing a real number.
    Symbol(Rc<String>),

    /// Take the additive inverse of an expression.
    Neg(Rc<S>),

    /// Addition of two expressions `l + r`.
    Add(Rc<S>, Rc<S>),

    /// Subtraction of two expressions `l - r`.
    Sub(Rc<S>, Rc<S>),

    /// Multiplication of two expressions `l * r`.
    Mul(Rc<S>, Rc<S>),

    /// Division of two expressions `l / r`.
    Div(Rc<S>, Rc<S>),

    /// Square root of an expression `√(x)`.
    Sqrt(Rc<S>),

    /// Sine of an expression `sin(x)`.
    Sin(Rc<S>),

    /// Cosine of an expression `cos(x)`.
    Cos(Rc<S>),

    /// Tangent of an expression `tan(x)`.
    Tan(Rc<S>),

    /// Arcsine of an expression `arcsin(x)`.
    ASin(Rc<S>),

    /// Arccosine of an expression `arccos(x)`.
    ACos(Rc<S>),

    /// Arctangent of an expression `arctan(x)`.
    ATan(Rc<S>),

    /// Hyperbolic sine of an expression `sinh(x)`.
    Sinh(Rc<S>),

    /// Hyperbolic cosine of an expression `cosh(x)`.
    Cosh(Rc<S>),

    /// Hyperbolic tangent of an expression `tanh(x)`.
    Tanh(Rc<S>),

    /// Inverse hyperbolic sine of an expression `arsinh(x)`.
    ASinh(Rc<S>),

    /// Inverse hyperbolic cosine of an expression `arcosh(x)`.
    ACosh(Rc<S>),

    /// Inverse hyperbolic tangent of an expression `artanh(x)`.
    ATanh(Rc<S>),

    /// Raise one expression to the power of another `l ^ r`.
    Pow(Rc<S>, Rc<S>),

    /// Exponentiation of an expression `exp(l)`.
    Exp(Rc<S>),

    /// Logarithm of an expression `log_b(x)`.
    Log(Rc<S>, Rc<S>),

    /// Base-2 logarithm of an expression `log2(x)`.
    Log2(Rc<S>),

    /// Base-10 logarithm of an expression `log10(x)`.
    Log10(Rc<S>),

    /// Natural logarithm of an expression `ln(x)`.
    Ln(Rc<S>),
}

impl PartialOrd for S {
    fn partial_cmp(&self, rhs: &Self) -> Option<std::cmp::Ordering> {
        match (self, rhs) {
            (S::Literal(l), S::Literal(r)) => Some(l.cmp(r)),
            _ => None,
        }
    }
}

impl PartialOrd<f64> for S {
    fn partial_cmp(&self, rhs: &f64) -> Option<std::cmp::Ordering> {
        match self {
            S::Literal(x) => x.partial_cmp(&OrderedFloat(*rhs)),
            _ => None,
        }
    }
}

impl PartialOrd<S> for f64 {
    fn partial_cmp(&self, rhs: &S) -> Option<std::cmp::Ordering> {
        match rhs {
            S::Literal(x) => OrderedFloat(*self).partial_cmp(x),
            _ => None,
        }
    }
}

impl PartialEq<f64> for S {
    fn eq(&self, rhs: &f64) -> bool {
        match self {
            S::Literal(x) => x == rhs,
            _ => false,
        }
    }
}

impl PartialEq<S> for f64 {
    fn eq(&self, rhs: &S) -> bool {
        match rhs {
            S::Literal(x) => x == self,
            _ => false,
        }
    }
}

impl From<f64> for S {
    fn from(f: f64) -> S { S::Literal(OrderedFloat(f)) }
}

impl<'a> From<&'a str> for S {
    fn from(s: &'a str) -> S { S::Symbol(Rc::new(s.to_string())) }
}

impl From<String> for S {
    fn from(s: String) -> S { S::Symbol(Rc::new(s)) }
}

impl Neg for S {
    type Output = S;
    fn neg(self) -> S {
        match self {
            S::Literal(f) => S::Literal(-f),
            S::Neg(n) => n.as_ref().clone(),
            S::Mul(l, r) if l.is_neg() => l.arg().unwrap() * r.as_ref(),
            S::Mul(l, r) if r.is_neg() => l.as_ref() * r.arg().unwrap(),
            S::Mul(l, r) => S::Neg(Rc::new(S::Mul(l, r))),
            S::Div(l, r) if l.is_neg() => l.arg().unwrap() / r.as_ref(),
            S::Div(l, r) if r.is_neg() => l.as_ref() / r.arg().unwrap(),
            S::Div(l, r) => S::Neg(Rc::new(S::Div(l, r))),
            x => S::Neg(Rc::new(x)),
        }
    }
}

impl Neg for &S {
    type Output = S;
    fn neg(self) -> S { -self.clone() }
}

macro_rules! impl_arith_rest {
    ( $trait:ident, $trait_fn:ident, $op:tt ) => {
        impl $trait<S> for &S {
            type Output = S;
            fn $trait_fn(self, rhs: S) -> S { self.clone() $op rhs }
        }

        impl $trait<&S> for S {
            type Output = S;
            fn $trait_fn(self, rhs: &S) -> S { self $op rhs.clone() }
        }

        impl $trait<&S> for &S {
            type Output = S;
            fn $trait_fn(self, rhs: &S) -> S { self.clone() $op rhs.clone() }
        }

        impl $trait<f64> for S {
            type Output = S;
            fn $trait_fn(self, rhs: f64) -> S { self $op S::from(rhs) }
        }

        impl $trait<f64> for &S {
            type Output = S;
            fn $trait_fn(self, rhs: f64) -> S { self $op S::from(rhs) }
        }

        impl $trait<S> for f64 {
            type Output = S;
            fn $trait_fn(self, rhs: S) -> S { S::from(self) $op rhs }
        }

        impl $trait<&S> for f64 {
            type Output = S;
            fn $trait_fn(self, rhs: &S) -> S { S::from(self) $op rhs }
        }
    }
}

impl Add<S> for S {
    type Output = S;
    fn add(self, rhs: S) -> S {
        match (self, rhs) {
            (S::Literal(l), S::Literal(r)) => S::Literal(l + r),
            (l, S::Literal(OrderedFloat(f))) if f == 0.0 => l,
            (l, S::Literal(OrderedFloat(f)))
                => S::Add(Rc::new(l), Rc::new(f.into())),
            (S::Literal(OrderedFloat(f)), r) if f == 0.0 => r,
            (S::Literal(OrderedFloat(f)), r)
                => S::Add(Rc::new(f.into()), Rc::new(r)),
            (S::Mul(ll, lr), S::Mul(rl, rr)) if ll == rl
                => S::Mul(Rc::new(lr.as_ref() + rr.as_ref()), ll),
            (S::Mul(ll, lr), S::Mul(rl, rr)) if ll == rr
                => S::Mul(Rc::new(lr.as_ref() + rl.as_ref()), ll),
            (S::Mul(ll, lr), S::Mul(rl, rr)) if lr == rl
                => S::Mul(Rc::new(ll.as_ref() + rr.as_ref()), lr),
            (S::Mul(ll, lr), S::Mul(rl, rr)) if lr == rr
                => S::Mul(Rc::new(ll.as_ref() + rl.as_ref()), lr),
            (S::Mul(ll, lr), S::Mul(rl, rr))
                => S::Add(
                    Rc::new(ll.as_ref() * lr.as_ref()),
                    Rc::new(rl.as_ref() * rr.as_ref()),
                ),
            (l, S::Mul(rl, rr)) if &l == rl.as_ref()
                => S::Mul(Rc::new(rr.as_ref() + 1.0), Rc::new(l)),
            (l, S::Mul(rl, rr)) if &l == rr.as_ref()
                => S::Mul(Rc::new(rl.as_ref() + 1.0), Rc::new(l)),
            (l, S::Mul(rl, rr))
                => S::Add(Rc::new(l), Rc::new(rl.as_ref() * rr.as_ref())),
            (S::Mul(ll, lr), r) if ll.as_ref() == &r
                => S::Mul(Rc::new(lr.as_ref() + 1.0), Rc::new(r)),
            (S::Mul(ll, lr), r) if lr.as_ref() == &r
                => S::Mul(Rc::new(ll.as_ref() + 1.0), Rc::new(r)),
            (S::Mul(ll, lr), r)
                => S::Add(Rc::new(ll.as_ref() * lr.as_ref()), Rc::new(r)),
            (S::Neg(l), r) => r - l.as_ref(),
            (l, S::Neg(r)) => l + r.as_ref(),
            (l, r) if l == r => 2.0_f64 * l,
            (l, r) => S::Add(Rc::new(l), Rc::new(r)),
        }
    }
}
impl_arith_rest!(Add, add, +);

impl Sub<S> for S {
    type Output = S;
    fn sub(self, rhs: S) -> S {
        match (self, rhs) {
            (S::Literal(l), S::Literal(r)) => S::Literal(l - r),
            (l, S::Literal(OrderedFloat(f))) if f == 0.0 => l,
            (l, S::Literal(OrderedFloat(f)))
                => S::Sub(Rc::new(l), Rc::new(f.into())),
            (S::Literal(OrderedFloat(f)), r) if f == 0.0 => -r,
            (S::Literal(OrderedFloat(f)), r)
                => S::Sub(Rc::new(f.into()), Rc::new(r)),
            (S::Mul(ll, lr), S::Mul(rl, rr)) if ll == rl
                => S::Mul(Rc::new(lr.as_ref() - rr.as_ref()), ll),
            (S::Mul(ll, lr), S::Mul(rl, rr)) if ll == rr
                => S::Mul(Rc::new(lr.as_ref() - rl.as_ref()), ll),
            (S::Mul(ll, lr), S::Mul(rl, rr)) if lr == rl
                => S::Mul(Rc::new(ll.as_ref() - rr.as_ref()), lr),
            (S::Mul(ll, lr), S::Mul(rl, rr)) if lr == rr
                => S::Mul(Rc::new(ll.as_ref() - rl.as_ref()), lr),
            (S::Mul(ll, lr), S::Mul(rl, rr))
                => S::Sub(
                    Rc::new(ll.as_ref() * lr.as_ref()),
                    Rc::new(rl.as_ref() * rr.as_ref()),
                ),
            (l, S::Mul(rl, rr)) if &l == rl.as_ref()
                => S::Mul(Rc::new(1.0 - rr.as_ref()), Rc::new(l)),
            (l, S::Mul(rl, rr)) if &l == rr.as_ref()
                => S::Mul(Rc::new(1.0 - rl.as_ref()), Rc::new(l)),
            (l, S::Mul(rl, rr))
                => S::Sub(Rc::new(l), Rc::new(rl.as_ref() * rr.as_ref())),
            (S::Mul(ll, lr), r) if ll.as_ref() == &r
                => S::Mul(Rc::new(lr.as_ref() - 1.0), Rc::new(r)),
            (S::Mul(ll, lr), r) if lr.as_ref() == &r
                => S::Mul(Rc::new(ll.as_ref() - 1.0), Rc::new(r)),
            (S::Mul(ll, lr), r)
                => S::Sub(Rc::new(ll.as_ref() * lr.as_ref()), Rc::new(r)),
            (l, S::Neg(r)) => l + r.as_ref(),
            (l, r) if l == r => S::Literal(1.0_f64.into()),
            (l, r) => S::Sub(Rc::new(l), Rc::new(r)),
        }
    }
}
impl_arith_rest!(Sub, sub, -);

impl Mul<S> for S {
    type Output = S;
    fn mul(self, rhs: S) -> S {
        match (self, rhs) {
            (S::Literal(l), S::Literal(r)) => S::Literal(l * r),
            (_, S::Literal(OrderedFloat(f))) if f == 0.0 => 0.0_f64.into(),
            (l, S::Literal(OrderedFloat(f))) if f == 1.0 => l,
            (l, S::Literal(OrderedFloat(f))) if f == -1.0 => -l,
            (l, S::Literal(OrderedFloat(f)))
                => S::Mul(Rc::new(f.into()), Rc::new(l)),
            (S::Literal(OrderedFloat(f)), _) if f == 0.0 => 0.0_f64.into(),
            (S::Literal(OrderedFloat(f)), r) if f == 1.0 => r,
            (S::Literal(OrderedFloat(f)), r) if f == -1.0 => -r,
            (S::Literal(OrderedFloat(f)), r)
                => S::Mul(Rc::new(f.into()), Rc::new(r)),
            (S::Neg(l), S::Neg(r)) => l.as_ref() * r.as_ref(),
            (l, S::Neg(r)) => -(l * r.as_ref()),
            (S::Neg(l), r) => -(l.as_ref() * r),
            (S::Exp(el), S::Exp(er)) => (el.as_ref() + er.as_ref()).exp(),
            (S::Exp(el), S::Pow(br, er)) if br.as_ref() == &consts::E
                => (el.as_ref() + er.as_ref()).exp(),
            (S::Exp(el), S::Pow(br, er))
                => S::Mul(Rc::new(el.exp()), Rc::new(br.pow(&er))),
            (S::Pow(bl, el), S::Exp(er)) if bl.as_ref() == &consts::E
                => (el.as_ref() + er.as_ref()).exp(),
            (S::Pow(bl, el), S::Exp(er))
                => S::Mul(Rc::new(bl.pow(&el)), Rc::new(er.exp())),
            (S::Pow(bl, el), S::Pow(br, er)) if bl == br
                => bl.pow(&(el.as_ref() + er.as_ref())),
            (S::Pow(bl, el), S::Pow(br, er))
                => S::Mul(Rc::new(bl.pow(&el)), Rc::new(br.pow(&er))),
            (l, S::Pow(br, er)) if &l == br.as_ref()
                => br.pow(&(1.0 + er.as_ref())),
            (l, S::Pow(br, er)) => S::Mul(Rc::new(l), Rc::new(br.pow(&er))),
            (S::Pow(bl, el), r) if bl.as_ref() == &r
                => bl.pow(&(el.as_ref() + 1.0)),
            (S::Pow(bl, el), r) => S::Mul(Rc::new(bl.pow(&el)), Rc::new(r)),
            (l, S::Div(rl, rr)) if &l == rr.as_ref() => rl.as_ref().clone(),
            (l, S::Div(rl, rr))
                => S::Mul(Rc::new(l), Rc::new(rl.as_ref() / rr.as_ref())),
            (S::Div(ll, lr), r) if lr.as_ref() == &r => ll.as_ref().clone(),
            (S::Div(ll, lr), r)
                => S::Mul(Rc::new(ll.as_ref() / lr.as_ref()), Rc::new(r)),
            (l, r) if l == r => l.into_pow(2.0_f64.into()),
            (l, r) => S::Mul(Rc::new(l), Rc::new(r)),
        }
    }
}
impl_arith_rest!(Mul, mul, *);

impl Div<S> for S {
    type Output = S;
    fn div(self, rhs: S) -> S {
        match (self, rhs) {
            (S::Literal(l), S::Literal(r)) => S::Literal(l / r),
            (l, S::Literal(OrderedFloat(f))) if f == 1.0 => l,
            (l, S::Literal(OrderedFloat(f))) if f == -1.0 => -l,
            (l, S::Literal(OrderedFloat(f)))
                => S::Div(Rc::new(l), Rc::new(f.into())),
            (S::Literal(OrderedFloat(f)), _) if f == 0.0 => 0.0_f64.into(),
            (S::Literal(OrderedFloat(f)), r)
                => S::Div(Rc::new(f.into()), Rc::new(r)),
            (S::Neg(l), S::Neg(r)) => l.as_ref() / r.as_ref(),
            (l, S::Neg(r)) => -(l / r.as_ref()),
            (S::Neg(l), r) => -(l.as_ref() / r),
            (S::Exp(el), S::Exp(er)) => (el.as_ref() - er.as_ref()).exp(),
            (S::Exp(el), S::Pow(br, er)) if br.as_ref() == &consts::E
                => (el.as_ref() - er.as_ref()).exp(),
            (S::Exp(el), S::Pow(br, er))
                => S::Div(Rc::new(el.exp()), Rc::new(br.pow(&er))),
            (S::Pow(bl, el), S::Exp(er)) if bl.as_ref() == &consts::E
                => (el.as_ref() - er.as_ref()).exp(),
            (S::Pow(bl, el), S::Exp(er))
                => S::Div(Rc::new(bl.pow(&el)), Rc::new(er.exp())),
            (S::Pow(bl, el), S::Pow(br, er)) if bl == br
                => bl.pow(&(el.as_ref() - er.as_ref())),
            (S::Pow(bl, el), S::Pow(br, er))
                => S::Div(Rc::new(bl.pow(&el)), Rc::new(br.pow(&er))),
            (l, S::Pow(br, er)) if &l == br.as_ref()
                => br.pow(&(1.0 - er.as_ref())),
            (l, S::Pow(br, er)) => S::Div(Rc::new(l), Rc::new(br.pow(&er))),
            (S::Pow(bl, el), r) if bl.as_ref() == &r
                => bl.pow(&(el.as_ref() - 1.0)),
            (S::Pow(bl, el), r) => S::Div(Rc::new(bl.pow(&el)), Rc::new(r)),
            (l, S::Mul(rl, rr)) if &l == rl.as_ref() => 1.0 / rr.as_ref(),
            (l, S::Mul(rl, rr)) if &l == rr.as_ref() => 1.0 / rl.as_ref(),
            (l, S::Mul(rl, rr))
                => S::Div(Rc::new(l), Rc::new(rl.as_ref() * rr.as_ref())),
            (l, S::Div(rl, rr)) => l * rr.as_ref() / rl.as_ref(),
            (S::Mul(ll, lr), r) if ll.as_ref() == &r => lr.as_ref().clone(),
            (S::Mul(ll, lr), r) if lr.as_ref() == &r => ll.as_ref().clone(),
            (S::Mul(ll, lr), r)
                => S::Div(Rc::new(ll.as_ref() * lr.as_ref()), Rc::new(r)),
            (S::Div(ll, lr), r) if ll.as_ref() == &r => 1.0 / lr.as_ref(),
            (S::Div(ll, lr), r) => S::Div(ll, Rc::new(lr.as_ref() * r)),
            (l, r) if l == r => 1.0_f64.into(),
            (l, r) => S::Div(Rc::new(l), Rc::new(r)),
        }
    }
}
impl_arith_rest!(Div, div, /);

impl std::iter::Sum<S> for S {
    fn sum<I>(iter: I) -> S
    where I: Iterator<Item = S>
    {
        let mut acc: S = S::new(0.0);
        for term in iter {
            acc = acc + term;
        }
        acc
    }
}

impl std::iter::Product<S> for S {
    fn product<I>(iter: I) -> S
    where I: Iterator<Item = S>
    {
        let mut acc: S = S::new(1.0);
        for term in iter {
            acc = acc * term;
        }
        acc
    }
}

impl S {
    /// Create a new symbol.
    pub fn new<T>(x: T) -> Self
    where T: Into<Self>
    {
        x.into()
    }

    /// Collect several symbols into a single sum.
    ///
    /// Operations are ordered from the left; i.e. in agreement with normal Rust
    /// order.
    pub fn sum<I>(items: I) -> Self
    where I: IntoIterator<Item = S>
    {
        fn sum_inner<I>(acc: S, mut items: I) -> S
        where I: Iterator<Item = S>
        {
            if let Some(next) = items.next() {
                if acc == S::Literal(OrderedFloat(0.0)) {
                    sum_inner(next, items)
                } else {
                    sum_inner(acc + next, items)
                }
            } else {
                acc
            }
        }
        sum_inner(S::new(0.0_f64), items.into_iter())
    }

    /// Collect several symbols into a single product.
    ///
    /// Operations are ordered from the left; i.e. in agreement with normal Rust
    /// order.
    pub fn prod<I>(items: I) -> Self
    where I: IntoIterator<Item = S>
    {
        fn prod_inner<I>(acc: S, mut items: I) -> S
        where I: Iterator<Item = S>
        {
            if let Some(next) = items.next() {
                if acc == S::Literal(OrderedFloat(1.0)) {
                    prod_inner(next, items)
                } else {
                    prod_inner(acc * next, items)
                }
            } else {
                acc
            }
        }
        prod_inner(S::new(1.0_f64), items.into_iter())
    }

    /// Return `true` if `self` is a `Literal`.
    pub fn is_literal(&self) -> bool { matches!(self, S::Literal(_)) }

    /// Return `true` if `self` is a `Symbol`.
    pub fn is_symbol(&self) -> bool { matches!(self, S::Symbol(_)) }

    /// Return `true` if `self` is a `Neg`.
    pub fn is_neg(&self) -> bool { matches!(self, S::Neg(_)) }

    /// Return `true` if `self` is an `Add`.
    pub fn is_add(&self) -> bool { matches!(self, S::Add(_, _)) }

    /// Return `true` if `self` is a `sub`.
    pub fn is_sub(&self) -> bool { matches!(self, S::Sub(_, _)) }

    /// Return `true` if `self` is a `Mul`.
    pub fn is_mul(&self) -> bool { matches!(self, S::Mul(_, _)) }

    /// Return `true` if `self` is a `Div`.
    pub fn is_div(&self) -> bool { matches!(self, S::Div(_, _)) }

    /// Return `true` if `self` is a `Sqrt`.
    pub fn is_sqrt(&self) -> bool { matches!(self, S::Sqrt(_)) }

    /// Return `true` if `self` is a `Sin`.
    pub fn is_sin(&self) -> bool { matches!(self, S::Sin(_)) }

    /// Return `true` if `self` is a `Cos`.
    pub fn is_cos(&self) -> bool { matches!(self, S::Cos(_)) }

    /// Return `true` if `self` is a `Tan`.
    pub fn is_tan(&self) -> bool { matches!(self, S::Tan(_)) }

    /// Return `true` if `self` is an `ASin`.
    pub fn is_asin(&self) -> bool { matches!(self, S::ASin(_)) }

    /// Return `true` if `self` is an `ACos`.
    pub fn is_acos(&self) -> bool { matches!(self, S::ACos(_)) }

    /// Return `true` if `self` is an `ATan`.
    pub fn is_atan(&self) -> bool { matches!(self, S::ATan(_)) }

    /// Return `true` if `self` is a `Sinh`.
    pub fn is_sinh(&self) -> bool { matches!(self, S::Sinh(_)) }

    /// Return `true` if `self` is a `Cosh`.
    pub fn is_cosh(&self) -> bool { matches!(self, S::Cosh(_)) }

    /// Return `true` if `self` is a `Tanh`.
    pub fn is_tanh(&self) -> bool { matches!(self, S::Tanh(_)) }

    /// Return `true` if `self` is an `ASinh`.
    pub fn is_asinh(&self) -> bool { matches!(self, S::ASinh(_)) }

    /// Return `true` if `self` is an `ACosh`.
    pub fn is_acosh(&self) -> bool { matches!(self, S::ACosh(_)) }

    /// Return `true` if `self` is an `ATanh`.
    pub fn is_atanh(&self) -> bool { matches!(self, S::ATanh(_)) }

    /// Return `true` if `self` is a `Pow`.
    pub fn is_pow(&self) -> bool { matches!(self, S::Pow(_, _)) }

    /// Return `true` if `self` is an `Exp`.
    pub fn is_exp(&self) -> bool { matches!(self, S::Exp(_)) }

    /// Return `true` if `self` is a `Log`.
    pub fn is_log(&self) -> bool { matches!(self, S::Log(_, _)) }

    /// Return `true` if `self` is a `Log2`.
    pub fn is_log2(&self) -> bool { matches!(self, S::Log2(_)) }

    /// Return `true` if `self` is a `Log10`.
    pub fn is_log10(&self) -> bool { matches!(self, S::Log10(_)) }

    /// Return `true` if `self` is a `Ln`.
    pub fn is_ln(&self) -> bool { matches!(self, S::Ln(_)) }

    /// Return `true` if `self` is a `Literal` or a `Symbol`.
    pub fn is_atom(&self) -> bool { self.is_literal() || self.is_symbol() }

    /// Return the principal symbolic argument of `self`.
    ///
    /// For `Add`, `Sub`, `Mul`, `Div`, this is the left argument; for `Pow`
    /// this is the base; for `Log` this is the argument (not the base).
    pub fn arg(&self) -> Option<&Self> {
        match self {
            S::Literal(_) => None,
            S::Symbol(_) => None,
            S::Neg(x) => Some(x.as_ref()),
            S::Add(l, _) => Some(l.as_ref()),
            S::Sub(l, _) => Some(l.as_ref()),
            S::Mul(l, _) => Some(l.as_ref()),
            S::Div(l, _) => Some(l.as_ref()),
            S::Sqrt(x) => Some(x.as_ref()),
            S::Sin(x) => Some(x.as_ref()),
            S::Cos(x) => Some(x.as_ref()),
            S::Tan(x) => Some(x.as_ref()),
            S::ASin(x) => Some(x.as_ref()),
            S::ACos(x) => Some(x.as_ref()),
            S::ATan(x) => Some(x.as_ref()),
            S::Sinh(x) => Some(x.as_ref()),
            S::Cosh(x) => Some(x.as_ref()),
            S::Tanh(x) => Some(x.as_ref()),
            S::ASinh(x) => Some(x.as_ref()),
            S::ACosh(x) => Some(x.as_ref()),
            S::ATanh(x) => Some(x.as_ref()),
            S::Pow(b, _) => Some(b.as_ref()),
            S::Exp(x) => Some(x.as_ref()),
            S::Log(_, x) => Some(x.as_ref()),
            S::Log2(x) => Some(x.as_ref()),
            S::Log10(x) => Some(x.as_ref()),
            S::Ln(x) => Some(x.as_ref()),
        }
    }

    /// Return the auxilliary symbolic argument of `self`.
    ///
    /// For `Add`, `Sub`, `Mul`, `Div`, this is the right argument; for `Pow`
    /// this is the exponent; for `Log` this is the base.
    pub fn aux(&self) -> Option<&Self> {
        match self {
            S::Literal(_) => None,
            S::Symbol(_) => None,
            S::Neg(_) => None,
            S::Add(_, r) => Some(r.as_ref()),
            S::Sub(_, r) => Some(r.as_ref()),
            S::Mul(_, r) => Some(r.as_ref()),
            S::Div(_, r) => Some(r.as_ref()),
            S::Sqrt(_) => None,
            S::Sin(_) => None,
            S::Cos(_) => None,
            S::Tan(_) => None,
            S::ASin(_) => None,
            S::ACos(_) => None,
            S::ATan(_) => None,
            S::Sinh(_) => None,
            S::Cosh(_) => None,
            S::Tanh(_) => None,
            S::ASinh(_) => None,
            S::ACosh(_) => None,
            S::ATanh(_) => None,
            S::Pow(_, e) => Some(e.as_ref()),
            S::Exp(_) => None,
            S::Log(b, _) => Some(b.as_ref()),
            S::Log2(_) => None,
            S::Log10(_) => None,
            S::Ln(_) => None,
        }
    }

    /// Return a set of all [symbols][Self::is_symbol] contained in `self`,
    /// listed in the order in which they are encountered.
    pub fn get_symbols(&self) -> IndexSet<S> {
        fn get_symbols_inner(s: &S, acc: &mut IndexSet<S>) {
            match s {
                S::Literal(_) => { },
                S::Symbol(x) => { acc.insert(S::Symbol(x.clone())); },
                S::Neg(x) => { get_symbols_inner(x, acc); },
                S::Add(l, r) => {
                    get_symbols_inner(l, acc);
                    get_symbols_inner(r, acc);
                },
                S::Sub(l, r) => {
                    get_symbols_inner(l, acc);
                    get_symbols_inner(r, acc);
                },
                S::Mul(l, r) => {
                    get_symbols_inner(l, acc);
                    get_symbols_inner(r, acc);
                },
                S::Div(l, r) => {
                    get_symbols_inner(l, acc);
                    get_symbols_inner(r, acc);
                },
                S::Sqrt(x) => { get_symbols_inner(x, acc); },
                S::Sin(x) => { get_symbols_inner(x, acc); },
                S::Cos(x) => { get_symbols_inner(x, acc); },
                S::Tan(x) => { get_symbols_inner(x, acc); },
                S::ASin(x) => { get_symbols_inner(x, acc); },
                S::ACos(x) => { get_symbols_inner(x, acc); },
                S::ATan(x) => { get_symbols_inner(x, acc); },
                S::Sinh(x) => { get_symbols_inner(x, acc); },
                S::Cosh(x) => { get_symbols_inner(x, acc); },
                S::Tanh(x) => { get_symbols_inner(x, acc); },
                S::ASinh(x) => { get_symbols_inner(x, acc); },
                S::ACosh(x) => { get_symbols_inner(x, acc); },
                S::ATanh(x) => { get_symbols_inner(x, acc); },
                S::Pow(b, e) => {
                    get_symbols_inner(b, acc);
                    get_symbols_inner(e, acc);
                },
                S::Exp(x) => { get_symbols_inner(x, acc); },
                S::Log(b, x) => {
                    get_symbols_inner(b, acc);
                    get_symbols_inner(x, acc);
                },
                S::Log2(x) => { get_symbols_inner(x, acc); },
                S::Log10(x) => { get_symbols_inner(x, acc); },
                S::Ln(x) => { get_symbols_inner(x, acc); },
            }
        }
        let mut symbols: IndexSet<S> = IndexSet::new();
        get_symbols_inner(self, &mut symbols);
        symbols
    }

    /// Return a set of all [atoms][Self::is_atom] contained in `self`, listed
    /// in the order in which they are encountered.
    pub fn get_atoms(&self) -> IndexSet<S> {
        fn get_atoms_inner(s: &S, acc: &mut IndexSet<S>) {
            match s {
                S::Literal(x) => { acc.insert(S::Literal(*x)); },
                S::Symbol(x) => { acc.insert(S::Symbol(x.clone())); },
                S::Neg(x) => { get_atoms_inner(x, acc); },
                S::Add(l, r) => {
                    get_atoms_inner(l, acc);
                    get_atoms_inner(r, acc);
                },
                S::Sub(l, r) => {
                    get_atoms_inner(l, acc);
                    get_atoms_inner(r, acc);
                },
                S::Mul(l, r) => {
                    get_atoms_inner(l, acc);
                    get_atoms_inner(r, acc);
                },
                S::Div(l, r) => {
                    get_atoms_inner(l, acc);
                    get_atoms_inner(r, acc);
                },
                S::Sqrt(x) => { get_atoms_inner(x, acc); },
                S::Sin(x) => { get_atoms_inner(x, acc); },
                S::Cos(x) => { get_atoms_inner(x, acc); },
                S::Tan(x) => { get_atoms_inner(x, acc); },
                S::ASin(x) => { get_atoms_inner(x, acc); },
                S::ACos(x) => { get_atoms_inner(x, acc); },
                S::ATan(x) => { get_atoms_inner(x, acc); },
                S::Sinh(x) => { get_atoms_inner(x, acc); },
                S::Cosh(x) => { get_atoms_inner(x, acc); },
                S::Tanh(x) => { get_atoms_inner(x, acc); },
                S::ASinh(x) => { get_atoms_inner(x, acc); },
                S::ACosh(x) => { get_atoms_inner(x, acc); },
                S::ATanh(x) => { get_atoms_inner(x, acc); },
                S::Pow(b, e) => {
                    get_atoms_inner(b, acc);
                    get_atoms_inner(e, acc);
                },
                S::Exp(x) => { get_atoms_inner(x, acc); },
                S::Log(b, x) => {
                    get_atoms_inner(b, acc);
                    get_atoms_inner(x, acc);
                },
                S::Log2(x) => { get_atoms_inner(x, acc); },
                S::Log10(x) => { get_atoms_inner(x, acc); },
                S::Ln(x) => { get_atoms_inner(x, acc); },
            }
        }
        let mut atoms: IndexSet<S> = IndexSet::new();
        get_atoms_inner(self, &mut atoms);
        atoms
    }

    /// Return `sqrt(self)`.
    pub fn sqrt(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.sqrt().into(),
            _ => S::Sqrt(Rc::new(self.clone())),
        }
    }

    /// Return `sqrt(self)`.
    pub fn into_sqrt(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.sqrt().into(),
            x => S::Sqrt(Rc::new(x)),
        }
    }

    /// Return `sin(self)`.
    pub fn sin(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.sin().into(),
            _ => S::Sin(Rc::new(self.clone())),
        }
    }

    /// Return `sin(self)`.
    pub fn into_sin(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.sin().into(),
            x => S::Sin(Rc::new(x)),
        }
    }

    /// Return `cos(self)`.
    pub fn cos(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.cos().into(),
            _ => S::Cos(Rc::new(self.clone())),
        }
    }

    /// Return `cos(self)`.
    pub fn into_cos(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.cos().into(),
            x => S::Cos(Rc::new(x)),
        }
    }

    /// Return `tan(self)`.
    pub fn tan(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.tan().into(),
            _ => S::Tan(Rc::new(self.clone())),
        }
    }

    /// Return `tan(self)`.
    pub fn into_tan(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.tan().into(),
            x => S::Tan(Rc::new(x)),
        }
    }

    /// Return `asin(self)`.
    pub fn asin(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.asin().into(),
            _ => S::ASin(Rc::new(self.clone())),
        }
    }

    /// Return `asin(self)`.
    pub fn into_asin(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.asin().into(),
            x => S::ASin(Rc::new(x)),
        }
    }

    /// Return `acos(self)`.
    pub fn acos(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.acos().into(),
            _ => S::ACos(Rc::new(self.clone())),
        }
    }

    /// Return `acos(self)`.
    pub fn into_acos(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.acos().into(),
            x => S::ACos(Rc::new(x)),
        }
    }

    /// Return `atan(self)`.
    pub fn atan(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.atan().into(),
            _ => S::ATan(Rc::new(self.clone())),
        }
    }

    /// Return `atan(self)`.
    pub fn into_atan(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.atan().into(),
            x => S::ATan(Rc::new(x)),
        }
    }

    /// Return `sinh(self)`.
    pub fn sinh(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.sinh().into(),
            _ => S::Sinh(Rc::new(self.clone())),
        }
    }

    /// Return `sinh(self)`.
    pub fn into_sinh(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.sinh().into(),
            x => S::Sinh(Rc::new(x)),
        }
    }

    /// Return `cosh(self)`.
    pub fn cosh(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.cosh().into(),
            _ => S::Cosh(Rc::new(self.clone())),
        }
    }

    /// Return `cosh(self)`.
    pub fn into_cosh(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.cosh().into(),
            x => S::Cosh(Rc::new(x)),
        }
    }

    /// Return `tanh(self)`.
    pub fn tanh(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.tanh().into(),
            _ => S::Tanh(Rc::new(self.clone())),
        }
    }

    /// Return `tanh(self)`.
    pub fn into_tanh(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.tanh().into(),
            x => S::Tanh(Rc::new(x)),
        }
    }

    /// Return asinh(self)`.
    pub fn asinh(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.asinh().into(),
            _ => S::ASinh(Rc::new(self.clone())),
        }
    }

    /// Return `asinh(self)`.
    pub fn into_asinh(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.asinh().into(),
            x => S::ASinh(Rc::new(x)),
        }
    }

    /// Return `acosh(self)`.
    pub fn acosh(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.acosh().into(),
            _ => S::ACosh(Rc::new(self.clone())),
        }
    }

    /// Return `acosh(self)`.
    pub fn into_acosh(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.acosh().into(),
            x => S::ACosh(Rc::new(x)),
        }
    }

    /// Return `atanh(self)`.
    pub fn atanh(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.atanh().into(),
            _ => S::ATanh(Rc::new(self.clone())),
        }
    }

    /// Return `atanh(self)`.
    pub fn into_atanh(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.atanh().into(),
            x => S::ATanh(Rc::new(x)),
        }
    }

    /// Return `pow(self, exp)`.
    ///
    /// `self` is the base.
    pub fn pow(&self, exp: &Self) -> Self {
        match (self, exp) {
            (S::Literal(OrderedFloat(b)), S::Literal(OrderedFloat(e)))
                => b.powf(*e).into(),
            (S::Literal(OrderedFloat(b)), _) if *b == std::f64::consts::E
                => exp.exp(),
            (S::Literal(OrderedFloat(_)), e)
                => S::Pow(Rc::new(self.clone()), Rc::new(e.clone())),
            (S::Sqrt(b), e) => b.pow(&(e / 2.0)),
            (b, S::Log(eb, ex)) if b == eb.as_ref() => ex.as_ref().clone(),
            (b, S::Log(eb, ex))
                => S::Pow(Rc::new(b.clone()), Rc::new(ex.log(eb))),
            (b, S::Log2(ex)) if b == &S::Literal(2.0_f64.into())
                => ex.as_ref().clone(),
            (b, S::Log2(ex)) => S::Pow(Rc::new(b.clone()), Rc::new(ex.log2())),
            (b, S::Ln(ex)) if b == &consts::E => ex.as_ref().clone(),
            (b, S::Ln(ex)) => S::Pow(Rc::new(b.clone()), Rc::new(ex.ln())),
            (b, S::Log10(ex)) if b == &S::Literal(10.0_f64.into())
                => ex.as_ref().clone(),
            (b, S::Log10(ex))
                => S::Pow(Rc::new(b.clone()), Rc::new(ex.log10())),
            (_, S::Literal(OrderedFloat(e))) if *e == 0.0 => 1.0_f64.into(),
            (b, S::Literal(OrderedFloat(e))) if *e == 0.5 => b.sqrt(),
            (b, S::Literal(OrderedFloat(e))) if *e == 1.0 => b.clone(),
            (b, S::Literal(OrderedFloat(e))) if *e == -1.0 => 1.0 / b,
            (b, S::Literal(OrderedFloat(e))) if *e == -0.5 => 1.0 / b.sqrt(),
            (_, S::Literal(OrderedFloat(_)))
                => S::Pow(Rc::new(self.clone()), Rc::new(exp.clone())),
            _ => S::Pow(Rc::new(self.clone()), Rc::new(exp.clone())),
        }
    }

    /// Return `pow(self, exp)`.
    ///
    /// `self` is the base.
    pub fn into_pow(self, exp: Self) -> Self {
        match (self, exp) {
            (S::Literal(OrderedFloat(b)), S::Literal(OrderedFloat(e)))
                => b.powf(e).into(),
            (S::Literal(OrderedFloat(b)), e) if b == std::f64::consts::E
                => e.into_exp(),
            (S::Literal(OrderedFloat(b)), e)
                => S::Pow(Rc::new(b.into()), Rc::new(e)),
            (S::Sqrt(b), e) => b.pow(&(e / 2.0)),
            (b, S::Log(eb, ex)) if &b == eb.as_ref() => ex.as_ref().clone(),
            (b, S::Log(eb, ex)) => S::Pow(Rc::new(b), Rc::new(ex.log(&eb))),
            (b, S::Log2(ex)) if b == S::Literal(2.0_f64.into())
                => ex.as_ref().clone(),
            (b, S::Log2(ex)) => S::Pow(Rc::new(b), Rc::new(ex.log2())),
            (b, S::Ln(ex)) if b == consts::E => ex.as_ref().clone(),
            (b, S::Ln(ex)) => S::Pow(Rc::new(b), Rc::new(ex.ln())),
            (b, S::Log10(ex)) if b == S::Literal(10.0.into())
                => ex.as_ref().clone(),
            (b, S::Log10(ex)) => S::Pow(Rc::new(b), Rc::new(ex.log10())),
            (_, S::Literal(OrderedFloat(e))) if e == 0.0 => 1.0_f64.into(),
            (b, S::Literal(OrderedFloat(e))) if e == 0.5 => b.into_sqrt(),
            (b, S::Literal(OrderedFloat(e))) if e == 1.0 => b,
            (b, S::Literal(OrderedFloat(e))) if e == -1.0 => 1.0 / b,
            (b, S::Literal(OrderedFloat(e))) if e == -0.5
                => 1.0 / b.into_sqrt(),
            (b, S::Literal(OrderedFloat(e)))
                => S::Pow(Rc::new(b), Rc::new(e.into())),
            (b, e) => S::Pow(Rc::new(b), Rc::new(e)),
        }
    }

    /// Return `exp(self)`.
    pub fn exp(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.exp().into(),
            S::Ln(e) => e.as_ref().clone(),
            _ => S::Exp(Rc::new(self.clone())),
        }
    }

    /// Return `exp(self)`.
    pub fn into_exp(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.exp().into(),
            S::Ln(e) => e.as_ref().clone(),
            x => S::Exp(Rc::new(x)),
        }
    }

    /// Return `log(self, base)`.
    ///
    /// `self` is the number whose logarithm is to be taken.
    pub fn log(&self, base: &Self) -> Self {
        match (self, base) {
            (S::Literal(OrderedFloat(x)), S::Literal(OrderedFloat(b)))
                => x.log(*b).into(),
            (S::Sqrt(x), b) => 0.5 * x.log(b),
            (S::Pow(bb, be), b) if bb.as_ref() == b => be.as_ref().clone(),
            (S::Pow(bb, be), b)
                => S::Log(Rc::new(b.clone()), Rc::new(bb.pow(be))),
            (S::Exp(be), b) if &consts::E == b => be.as_ref().clone(),
            (S::Exp(be), b) => S::Log(Rc::new(b.clone()), Rc::new(be.exp())),
            (x, S::Literal(OrderedFloat(b))) if *b == 2.0 => x.log2(),
            (x, S::Literal(OrderedFloat(b))) if *b == std::f64::consts::E
                => x.ln(),
            (x, S::Literal(OrderedFloat(b))) if *b == 10.0 => x.log10(),
            (x, S::Literal(OrderedFloat(b)))
                => S::Log(Rc::new((*b).into()), Rc::new(x.clone())),
            _ => S::Log(Rc::new(base.clone()), Rc::new(self.clone())),
        }
    }

    /// Return `log(self, base)`.
    ///
    /// `self` is the number whose logarithm is to be taken.
    pub fn into_log(self, base: Self) -> Self {
        match (self, base) {
            (S::Literal(OrderedFloat(x)), S::Literal(OrderedFloat(b)))
                => x.log(b).into(),
            (S::Sqrt(x), b) => 0.5 * x.log(&b),
            (S::Pow(bb, be), b) if bb.as_ref() == &b => be.as_ref().clone(),
            (S::Pow(bb, be), b) => S::Log(Rc::new(b), Rc::new(bb.pow(&be))),
            (S::Exp(ex), b) if consts::E == b => ex.as_ref().clone(),
            (S::Exp(ex), b) => S::Log(Rc::new(b), Rc::new(ex.exp())),
            (x, S::Literal(OrderedFloat(b))) if b == 2.0 => x.into_log2(),
            (x, S::Literal(OrderedFloat(b))) if b == std::f64::consts::E
                => x.into_ln(),
            (x, S::Literal(OrderedFloat(b))) if b == 10.0 => x.into_log10(),
            (x, S::Literal(OrderedFloat(b)))
                => S::Log(Rc::new(b.into()), Rc::new(x)),
            (x, b) => S::Log(Rc::new(b), Rc::new(x)),
        }
    }

    /// Return `log2(self)`.
    pub fn log2(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.log2().into(),
            S::Sqrt(x) => 0.5 * x.log2(),
            S::Pow(b, e) if b.as_ref() == &S::Literal(2.0.into())
                => e.as_ref().clone(),
            S::Pow(b, e) => S::Log2(Rc::new(b.pow(e))),
            _ => S::Log2(Rc::new(self.clone())),
        }
    }

    /// Return `log2(self)`.
    pub fn into_log2(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.log2().into(),
            S::Sqrt(x) => 0.5 * x.log2(),
            S::Pow(b, e) if b.as_ref() == &S::Literal(2.0.into())
                => e.as_ref().clone(),
            S::Pow(b, e) => S::Log2(Rc::new(b.pow(&e))),
            x => S::Log2(Rc::new(x)),
        }
    }

    /// Return `log10(self)`.
    pub fn log10(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.log10().into(),
            S::Sqrt(x) => 0.5 * x.log10(),
            S::Pow(b, e) if b.as_ref() == &S::Literal(10.0.into())
                => e.as_ref().clone(),
            S::Pow(b, e) => S::Log10(Rc::new(b.pow(e))),
            _ => S::Log10(Rc::new(self.clone())),
        }
    }

    /// Return `log10(self)`.
    pub fn into_log10(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.log10().into(),
            S::Sqrt(x) => 0.5 * x.log10(),
            S::Pow(b, e) if b.as_ref() == &S::Literal(10.0.into())
                => e.as_ref().clone(),
            S::Pow(b, e) => S::Log10(Rc::new(b.pow(&e))),
            x => S::Log10(Rc::new(x)),
        }
    }

    /// Return `ln(self)`.
    pub fn ln(&self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.ln().into(),
            S::Sqrt(x) => 0.5 * x.ln(),
            S::Pow(b, e) if b.as_ref() == &consts::E => e.as_ref().clone(),
            S::Pow(b, e) => S::Ln(Rc::new(b.pow(e))),
            S::Exp(e) => e.as_ref().clone(),
            _ => S::Ln(Rc::new(self.clone())),
        }
    }

    /// Return `ln(self)`.
    pub fn into_ln(self) -> Self {
        match self {
            S::Literal(OrderedFloat(x)) => x.ln().into(),
            S::Sqrt(x) => 0.5 * x.ln(),
            S::Pow(b, e) if b.as_ref() == &consts::E => e.as_ref().clone(),
            S::Pow(b, e) => S::Ln(Rc::new(b.pow(&e))),
            S::Exp(e) => e.as_ref().clone(),
            x => S::Ln(Rc::new(x)),
        }
    }

    /// Return `true` if `v` exists, verbatim, within `self`.
    pub fn contains(&self, v: &Self) -> bool {
        if self == v { return true; }
        match self {
            S::Literal(_) => false,
            S::Symbol(_) => false,
            S::Neg(x) => x.contains(v),
            S::Add(l, r) => l.contains(v) || r.contains(v),
            S::Sub(l, r) => l.contains(v) || r.contains(v),
            S::Mul(l, r) => l.contains(v) || r.contains(v),
            S::Div(l, r) => l.contains(v) || r.contains(v),
            S::Sqrt(x) => x.contains(v),
            S::Sin(x) => x.contains(v),
            S::Cos(x) => x.contains(v),
            S::Tan(x) => x.contains(v),
            S::ASin(x) => x.contains(v),
            S::ACos(x) => x.contains(v),
            S::ATan(x) => x.contains(v),
            S::Sinh(x) => x.contains(v),
            S::Cosh(x) => x.contains(v),
            S::Tanh(x) => x.contains(v),
            S::ASinh(x) => x.contains(v),
            S::ACosh(x) => x.contains(v),
            S::ATanh(x) => x.contains(v),
            S::Pow(b, e) => b.contains(v) || e.contains(v),
            S::Exp(x) => x.contains(v),
            S::Log(b, x) => b.contains(v) || x.contains(v),
            S::Log2(x) => x.contains(v),
            S::Log10(x) => x.contains(v),
            S::Ln(x) => x.contains(v),
        }
    }

    /// Compute the partial derivative of `self` with respect to `v`.
    pub fn diff(&self, v: &Self) -> Self {
        if self == v { return 1.0_f64.into(); }
        if !self.contains(v) { return 0.0_f64.into(); }
        match self {
            S::Literal(_) => 0.0_f64.into(),
            S::Symbol(x) => {
                if let S::Symbol(u) = v {
                    if x == u { 1.0_f64.into() } else { 0.0_f64.into() }
                } else {
                    0.0_f64.into()
                }
            },
            S::Neg(x) => -x.diff(v),
            S::Add(l, r) => l.diff(v) + r.diff(v),
            S::Sub(l, r) => l.diff(v) - r.diff(v),
            S::Mul(l, r)
                => l.diff(v) * r.as_ref() + r.diff(v) * l.as_ref(),
            S::Div(l, r)
                => l.diff(v) / r.as_ref()
                - r.diff(v) * (l.as_ref() / r.pow(&2.0_f64.into())),
            S::Sqrt(x) => x.diff(v) / (2.0_f64 * x.sqrt()),
            S::Sin(x) => x.diff(v) * x.cos(),
            S::Cos(x) => -(x.diff(v) * x.sin()),
            S::Tan(x) => x.diff(v) / x.cos().pow(&2.0_f64.into()),
            S::ASin(x)
                => x.diff(v)
                / (1.0_f64 - x.pow(&2.0_f64.into())).sqrt(),
            S::ACos(x)
                => -x.diff(v)
                / (1.0_f64 - x.pow(&2.0_f64.into())).sqrt(),
            S::ATan(x)
                => x.diff(v) / (1.0_f64 + x.pow(&2.0_f64.into())),
            S::Sinh(x) => x.diff(v) * x.cosh(),
            S::Cosh(x) => x.diff(v) * x.sinh(),
            S::Tanh(x)
                => x.diff(v) * (1.0_f64 - x.tanh().pow(&2.0_f64.into())),
            S::ASinh(x)
                => x.diff(v)
                / (x.pow(&2.0_f64.into()) + 1.0_f64).sqrt(),
            S::ACosh(x)
                => x.diff(v)
                / (x.pow(&2.0_f64.into()) - 1.0_f64).sqrt(),
            S::ATanh(x)
                => -x.diff(v) / (x.pow(&2.0_f64.into()) - 1.0_f64),
            S::Pow(b, e)
                => (
                    e.diff(v) * b.ln()
                    + b.diff(v) * (e.as_ref() / b.as_ref())
                ) * b.pow(e.as_ref()),
            S::Exp(x) => x.diff(v) * x.exp(),
            S::Log(b, x)
                => -b.diff(v) / (b.as_ref() * b.ln().pow(&2.0_f64.into()))
                + x.diff(v) / (x.as_ref() * b.ln()),
            S::Log2(x) => x.diff(v) / (&consts::LN_2 * x.as_ref()),
            S::Log10(x) => x.diff(v) / (&consts::LN_10 * x.as_ref()),
            S::Ln(x) => x.diff(v) / x.as_ref(),
        }
    }

    // /// Expand `self` out into simple sums and products.
    // ///
    // /// - Distribute `Neg`s/`Mul`s/`Div`s across `Add`s and `Sub`s
    // /// - Convert `Sub` to `Add`/`Neg` and `Div` to `Mul`/`Pow`
    // pub fn expand(&self) -> Self {
    //     todo!()
    // }

    fn get_add_terms(&self) -> Vec<&Self> {
        fn get_add_terms_inner<'a>(s: &'a S, acc: &mut Vec<&'a S>) {
            if let S::Add(l, r) = s {
                if l.is_add() {
                    get_add_terms_inner(l, acc);
                } else {
                    acc.push(l);
                }
                if r.is_add() {
                    get_add_terms_inner(r, acc);
                } else {
                    acc.push(r);
                }
            }
        }
        let mut terms: Vec<&S> = Vec::new();
        get_add_terms_inner(self, &mut terms);
        terms
    }

    fn get_mul_terms(&self) -> Vec<&Self> {
        fn get_mul_terms_inner<'a>(s: &'a S, acc: &mut Vec<&'a S>) {
            if let S::Mul(l, r) = s {
                if l.is_mul() {
                    get_mul_terms_inner(l, acc);
                } else {
                    acc.push(l);
                }
                if r.is_mul() {
                    get_mul_terms_inner(r, acc);
                } else {
                    acc.push(r);
                }
            }
        }
        let mut terms: Vec<&S> = Vec::new();
        get_mul_terms_inner(self, &mut terms);
        terms
    }

    /// Re-evaluate all sub-expressions.
    pub fn reval(&self) -> Self {
        match self {
            S::Literal(x) => S::Literal(*x),
            S::Symbol(x) => S::Symbol(x.clone()),
            S::Neg(x) => -x.reval(),
            S::Add(l, r) => l.reval() + r.reval(),
            S::Sub(l, r) => l.reval() - r.reval(),
            S::Mul(l, r) => l.reval() * r.reval(),
            S::Div(l, r) => l.reval() / r.reval(),
            S::Sqrt(x) => x.reval().into_sqrt(),
            S::Sin(x) => x.reval().into_sin(),
            S::Cos(x) => x.reval().into_cos(),
            S::Tan(x) => x.reval().into_tan(),
            S::ASin(x) => x.reval().into_asin(),
            S::ACos(x) => x.reval().into_acos(),
            S::ATan(x) => x.reval().into_atan(),
            S::Sinh(x) => x.reval().into_sinh(),
            S::Cosh(x) => x.reval().into_cosh(),
            S::Tanh(x) => x.reval().into_tanh(),
            S::ASinh(x) => x.reval().into_asinh(),
            S::ACosh(x) => x.reval().into_acosh(),
            S::ATanh(x) => x.reval().into_atanh(),
            S::Pow(b, e) => b.reval().into_pow(e.reval()),
            S::Exp(x) => x.reval().into_exp(),
            S::Log(b, x) => x.reval().into_log(b.reval()),
            S::Log2(x) => x.reval().into_log2(),
            S::Log10(x) => x.reval().into_log10(),
            S::Ln(x) => x.reval().into_ln(),
        }
    }

    /// Make a single expression → expression substitution and re-evaluate all
    /// sub-expressions.
    ///
    /// The expression to replace must be matched verbatim in order for the
    /// substitution to be made.
    pub fn subs_single(&self, target: &S, new: &S) -> Self {
        if self == target { return new.clone(); }
        match self {
            S::Literal(x) => S::Literal(*x),
            S::Symbol(x) => S::Symbol(x.clone()),
            S::Neg(x) => -x.subs_single(target, new),
            S::Add(l, r)
                => l.subs_single(target, new) + r.subs_single(target, new),
            S::Sub(l, r)
                => l.subs_single(target, new) - r.subs_single(target, new),
            S::Mul(l, r)
                => l.subs_single(target, new) * r.subs_single(target, new),
            S::Div(l, r)
                => l.subs_single(target, new) / r.subs_single(target, new),
            S::Sqrt(x) => x.subs_single(target, new).into_sqrt(),
            S::Sin(x) => x.subs_single(target, new).into_sin(),
            S::Cos(x) => x.subs_single(target, new).into_cos(),
            S::Tan(x) => x.subs_single(target, new).into_tan(),
            S::ASin(x) => x.subs_single(target, new).into_asin(),
            S::ACos(x) => x.subs_single(target, new).into_acos(),
            S::ATan(x) => x.subs_single(target, new).into_atan(),
            S::Sinh(x) => x.subs_single(target, new).into_sinh(),
            S::Cosh(x) => x.subs_single(target, new).into_cosh(),
            S::Tanh(x) => x.subs_single(target, new).into_tanh(),
            S::ASinh(x) => x.subs_single(target, new).into_asinh(),
            S::ACosh(x) => x.subs_single(target, new).into_acosh(),
            S::ATanh(x) => x.subs_single(target, new).into_atanh(),
            S::Pow(b, e)
                => b.subs_single(target, new)
                .into_pow(e.subs_single(target, new)),
            S::Exp(x) => x.subs_single(target, new).into_exp(),
            S::Log(b, x)
                => x.subs_single(target, new)
                .into_log(b.subs_single(target, new)),
            S::Log2(x) => x.subs_single(target, new).into_log2(),
            S::Log10(x) => x.subs_single(target, new).into_log10(),
            S::Ln(x) => x.subs_single(target, new).into_ln(),
        }
    }

    /// Make a series of expression → expression substitutions and re-evaluate
    /// all sub-expressions.
    ///
    /// Expressions to replace must be matched verbatim in order for the
    /// substitution to be made.
    pub fn subs<'a, I>(&self, subs: I) -> Self
    where I: IntoIterator<Item = (&'a S, &'a S)>
    {
        let mut new_self = self.clone();
        subs.into_iter()
            .for_each(|(target, new)| {
                new_self = new_self.subs_single(target, new);
            });
        new_self
    }

    /// Make a single expression → number substitution and re-evaluate all
    /// sub-expressions.
    ///
    /// The expression to replace must be matched verbatim in order for the
    /// substitution to be made.
    pub fn subsf_single(&self, target: &S, val: f64) -> Self {
        self.subs_single(target, &val.into())
    }

    /// Make a series of expression → number substitutions and re-evaluate all
    /// sub-expressions.
    ///
    /// Expressions to replace must be matched verbatim in order for the
    /// substitution to be made.
    pub fn subsf<'a, I>(&self, subsf: I) -> Self
    where I: IntoIterator<Item = (&'a S, &'a f64)>
    {
        let mut new_self = self.clone();
        subsf.into_iter()
            .for_each(|(target, val)| {
                new_self = new_self.subsf_single(target, *val);
            });
        new_self
    }

    /// Make a single expression → number substitution and attempt to evaluate
    /// as a single number.
    ///
    /// Expressions to replace must be matched verbatim in order for the
    /// substitution to be made.
    pub fn evalf_single(&self, target: &S, val: f64) -> SResult<f64> {
        match self.subsf_single(target, val) {
            S::Literal(OrderedFloat(x)) => Ok(x),
            x => {
                let symbols: IndexSet<String>
                    = x.get_symbols()
                    .into_iter()
                    .map(|symk| {
                        let S::Symbol(sym) = symk else { unreachable!() };
                        sym.to_string()
                    })
                    .collect();
                Err(SError::Evalf(symbols))
            },
        }
    }

    /// Make a series of expression → number substitutions and attempt to
    /// evaluate as a single number.
    pub fn evalf<'a, I>(&self, subsf: I) -> SResult<f64>
    where I: IntoIterator<Item = (&'a S, &'a f64)>
    {
        match self.subsf(subsf) {
            S::Literal(OrderedFloat(x)) => Ok(x),
            x => {
                let symbols: IndexSet<String>
                    = x.get_symbols()
                    .into_iter()
                    .map(|symk| {
                        let S::Symbol(sym) = symk else { unreachable!() };
                        sym.to_string()
                    })
                    .collect();
                Err(SError::Evalf(symbols))
            },
        }
    }

    // /// Make simplifications.
    // ///
    // /// - Evaluate operations between `Literal`s
    // /// - Combine terms
    // /// - Reduce fractions
    // pub fn simplify(&self) -> Self {
    //     todo!()
    // }

    /// Render `self` as a Rust expression evaluating to a `f64`, taking all
    /// symbols to be of type `f64`.
    pub fn to_string_rust(&self) -> String {
        fn write_parens_if(out: &mut String, x: &S, test: bool) {
            if test {
                out.push('(');
                fmt_rust(out, x);
                out.push(')');
            } else {
                fmt_rust(out, x);
            }
        }

        fn write_rust_method(out: &mut String, x: &S, fn_name: &str) {
            write_parens_if(
                out, x,
                x.is_add()
                || x.is_sub()
                || x.is_mul()
                || x.is_div()
                || x.is_neg()
            );
            out.push_str(&format!(".{}()", fn_name));
        }

        fn fmt_rust(out: &mut String, x: &S) {
            match x {
                S::Literal(x) => { out.push_str(&format!("{:e}", x)); },
                S::Symbol(x) => { out.push_str(x); },
                S::Neg(x) => {
                    out.push('-');
                    write_parens_if(
                        out, x, x.is_add() || x.is_sub() || x.is_neg());
                },
                S::Add(l, r) => {
                    fmt_rust(out, l);
                    if let S::Neg(n) = r.as_ref() {
                        out.push_str(" - ");
                        write_parens_if(
                            out, n, n.is_add() || n.is_sub() || n.is_neg());
                    } else {
                        out.push_str(" + ");
                        fmt_rust(out, r);
                    }
                },
                S::Sub(l, r) => {
                    fmt_rust(out, l);
                    if let S::Neg(n) = r.as_ref() {
                        out.push_str(" + ");
                        write_parens_if(
                            out, n, n.is_add() || n.is_sub() || n.is_neg());
                    } else {
                        out.push_str(" - ");
                        fmt_rust(out, r);
                    }
                },
                S::Mul(l, r) => {
                    write_parens_if(out, l, l.is_add() || l.is_sub());
                    out.push_str(" * ");
                    write_parens_if(out, r, r.is_add() || r.is_sub());
                },
                S::Div(l, r) => {
                    write_parens_if(out, l, l.is_add() || l.is_sub());
                    out.push_str(" / ");
                    write_parens_if(
                        out, r,
                        r.is_add()
                        || r.is_sub()
                        || r.is_mul()
                        || r.is_div()
                    );
                },
                S::Sqrt(x) => write_rust_method(out, x, "sqrt"),
                S::Sin(x) => write_rust_method(out, x, "sin"),
                S::Cos(x) => write_rust_method(out, x, "cos"),
                S::Tan(x) => write_rust_method(out, x, "tan"),
                S::ASin(x) => write_rust_method(out, x, "asin"),
                S::ACos(x) => write_rust_method(out, x, "acos"),
                S::ATan(x) => write_rust_method(out, x, "atan"),
                S::Sinh(x) => write_rust_method(out, x, "sinh"),
                S::Cosh(x) => write_rust_method(out, x, "cosh"),
                S::Tanh(x) => write_rust_method(out, x, "tanh"),
                S::ASinh(x) => write_rust_method(out, x, "asinh"),
                S::ACosh(x) => write_rust_method(out, x, "acosh"),
                S::ATanh(x) => write_rust_method(out, x, "atanh"),
                S::Pow(b, e) => {
                    write_parens_if(
                        out, b,
                        b.is_add()
                        || b.is_sub()
                        || b.is_mul()
                        || b.is_div()
                        || b.is_neg()
                    );
                    out.push_str(".powf(");
                    fmt_rust(out, e);
                    out.push(')');
                },
                S::Exp(x) => write_rust_method(out, x, "exp"),
                S::Log(b, x) => {
                    write_parens_if(
                        out, x,
                        x.is_add()
                        || x.is_sub()
                        || x.is_mul()
                        || x.is_div()
                        || x.is_neg()
                    );
                    out.push_str(".log(");
                    fmt_rust(out, b);
                    out.push(')');
                },
                S::Log2(x) => write_rust_method(out, x, "log2"),
                S::Log10(x) => write_rust_method(out, x, "log10"),
                S::Ln(x) => write_rust_method(out, x, "ln"),
            }
        }

        let mut out = String::new();
        fmt_rust(&mut out, self);
        out
    }

    /// Render `self` as a Python expression evaluating to a `float`, taking all
    /// symbols to be of type `float`.
    ///
    /// Appropriate functions from the `math` module are assumed to be in scope.
    /// For NumPy functions, see [`to_string_numpy`][Self::to_string_numpy].
    pub fn to_string_python(&self) -> String {
        fn write_parens_if(out: &mut String, x: &S, test: bool) {
            if test {
                out.push('(');
                fmt_python(out, x);
                out.push(')');
            } else {
                fmt_python(out, x);
            }
        }

        fn write_math_function(out: &mut String, x: &S, fn_name: &str) {
            out.push_str(&format!("{}(", fn_name));
            fmt_python(out, x);
            out.push(')');
        }

        fn fmt_python(out: &mut String, x: &S) {
            match x {
                S::Literal(x) => { out.push_str(&format!("{}", x)); },
                S::Symbol(x) => { out.push_str(x); },
                S::Neg(x) => {
                    out.push('-');
                    write_parens_if(
                        out, x, x.is_add() || x.is_sub() || x.is_neg());
                },
                S::Add(l, r) => {
                    fmt_python(out, l);
                    if let S::Neg(n) = r.as_ref() {
                        out.push_str(" - ");
                        write_parens_if(
                            out, n, n.is_add() || n.is_sub() || n.is_neg());
                    } else {
                        out.push_str(" + ");
                        fmt_python(out, r);
                    }
                },
                S::Sub(l, r) => {
                    fmt_python(out, l);
                    if let S::Neg(n) = r.as_ref() {
                        out.push_str(" + ");
                        write_parens_if(
                            out, n, n.is_add() || n.is_sub() || n.is_neg());
                    } else {
                        out.push_str(" - ");
                        fmt_python(out, r);
                    }
                },
                S::Mul(l, r) => {
                    write_parens_if(out, l, l.is_add() || l.is_sub());
                    out.push_str(" * ");
                    write_parens_if(out, r, r.is_add() || r.is_sub());
                },
                S::Div(l, r) => {
                    write_parens_if(out, l, l.is_add() || l.is_sub());
                    out.push_str(" / ");
                    write_parens_if(
                        out, r,
                        r.is_add()
                        || r.is_sub()
                        || r.is_mul()
                        || r.is_div()
                    );
                },
                S::Sqrt(x) => write_math_function(out, x, "sqrt"),
                S::Sin(x) => write_math_function(out, x, "sin"),
                S::Cos(x) => write_math_function(out, x, "cos"),
                S::Tan(x) => write_math_function(out, x, "tan"),
                S::ASin(x) => write_math_function(out, x, "asin"),
                S::ACos(x) => write_math_function(out, x, "acos"),
                S::ATan(x) => write_math_function(out, x, "atan"),
                S::Sinh(x) => write_math_function(out, x, "sinh"),
                S::Cosh(x) => write_math_function(out, x, "cosh"),
                S::Tanh(x) => write_math_function(out, x, "tanh"),
                S::ASinh(x) => write_math_function(out, x, "asinh"),
                S::ACosh(x) => write_math_function(out, x, "acosh"),
                S::ATanh(x) => write_math_function(out, x, "atanh"),
                S::Pow(b, e) => {
                    write_parens_if(
                        out, b,
                        b.is_add()
                        || b.is_sub()
                        || b.is_mul()
                        || b.is_div()
                        || b.is_neg()
                    );
                    out.push_str("**");
                    write_parens_if(
                        out, e,
                        e.is_add()
                        || e.is_sub()
                        || e.is_mul()
                        || e.is_div()
                        || e.is_neg()
                    );
                },
                S::Exp(x) => write_math_function(out, x, "exp"),
                S::Log(b, x) => {
                    out.push_str("log(");
                    fmt_python(out, x);
                    out.push_str(", ");
                    fmt_python(out, b);
                    out.push(')');
                },
                S::Log2(x) => write_math_function(out, x, "log2"),
                S::Log10(x) => write_math_function(out, x, "log10"),
                S::Ln(x) => write_math_function(out, x, "log"),
            }
        }

        let mut out = String::new();
        fmt_python(&mut out, self);
        out
    }

    /// Render `self` as a NumPy expression evaluating to a `numpy.float64`,
    /// taking all symbols to be of type `numpy.float64`.
    ///
    /// The `numpy` module is assumed to be in scope as `np`.
    pub fn to_string_numpy(&self) -> String {
        fn write_parens_if(out: &mut String, x: &S, test: bool) {
            if test {
                out.push('(');
                fmt_numpy(out, x);
                out.push(')');
            } else {
                fmt_numpy(out, x);
            }
        }

        fn write_numpy_function(out: &mut String, x: &S, fn_name: &str) {
            out.push_str(&format!("np.{}(", fn_name));
            fmt_numpy(out, x);
            out.push(')');
        }

        fn fmt_numpy(out: &mut String, x: &S) {
            match x {
                S::Literal(x) => { out.push_str(&format!("{}", x)); },
                S::Symbol(x) => { out.push_str(x); },
                S::Neg(x) => {
                    out.push('-');
                    write_parens_if(
                        out, x, x.is_add() || x.is_sub() || x.is_neg());
                },
                S::Add(l, r) => {
                    fmt_numpy(out, l);
                    if let S::Neg(n) = r.as_ref() {
                        out.push_str(" - ");
                        write_parens_if(
                            out, n, n.is_add() || n.is_sub() || n.is_neg());
                    } else {
                        out.push_str(" + ");
                        fmt_numpy(out, r);
                    }
                },
                S::Sub(l, r) => {
                    fmt_numpy(out, l);
                    if let S::Neg(n) = r.as_ref() {
                        out.push_str(" + ");
                        write_parens_if(
                            out, n, n.is_add() || n.is_sub() || n.is_neg());
                    } else {
                        out.push_str(" - ");
                        fmt_numpy(out, r);
                    }
                },
                S::Mul(l, r) => {
                    write_parens_if(out, l, l.is_add() || l.is_sub());
                    out.push_str(" * ");
                    write_parens_if(out, r, r.is_add() || r.is_sub());
                },
                S::Div(l, r) => {
                    write_parens_if(out, l, l.is_add() || l.is_sub());
                    out.push_str(" / ");
                    write_parens_if(
                        out, r,
                        r.is_add()
                        || r.is_sub()
                        || r.is_mul()
                        || r.is_div()
                        || r.is_log()
                    );
                },
                S::Sqrt(x) => write_numpy_function(out, x, "sqrt"),
                S::Sin(x) => write_numpy_function(out, x, "sin"),
                S::Cos(x) => write_numpy_function(out, x, "cos"),
                S::Tan(x) => write_numpy_function(out, x, "tan"),
                S::ASin(x) => write_numpy_function(out, x, "arcsin"),
                S::ACos(x) => write_numpy_function(out, x, "arccos"),
                S::ATan(x) => write_numpy_function(out, x, "arctan"),
                S::Sinh(x) => write_numpy_function(out, x, "sinh"),
                S::Cosh(x) => write_numpy_function(out, x, "cosh"),
                S::Tanh(x) => write_numpy_function(out, x, "tanh"),
                S::ASinh(x) => write_numpy_function(out, x, "arcsinh"),
                S::ACosh(x) => write_numpy_function(out, x, "arccosh"),
                S::ATanh(x) => write_numpy_function(out, x, "arctanh"),
                S::Pow(b, e) => {
                    write_parens_if(
                        out, b,
                        b.is_add()
                        || b.is_sub()
                        || b.is_mul()
                        || b.is_div()
                        || b.is_neg()
                        || b.is_log()
                    );
                    out.push_str("**");
                    write_parens_if(
                        out, e,
                        e.is_add()
                        || e.is_sub()
                        || e.is_mul()
                        || e.is_div()
                        || e.is_neg()
                        || e.is_log()
                    );
                },
                S::Exp(x) => write_numpy_function(out, x, "exp"),
                S::Log(b, x) => {
                    out.push_str("np.log(");
                    fmt_numpy(out, x);
                    out.push_str(") / np.log(");
                    fmt_numpy(out, b);
                    out.push(')');
                },
                S::Log2(x) => write_numpy_function(out, x, "log2"),
                S::Log10(x) => write_numpy_function(out, x, "log10"),
                S::Ln(x) => write_numpy_function(out, x, "log"),
            }
        }

        let mut out = String::new();
        fmt_numpy(&mut out, self);
        out
    }
}

impl fmt::Display for S {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fn write_parens_if(f: &mut fmt::Formatter<'_>, x: &S, test: bool)
            -> fmt::Result
        {
            if test {
                write!(f, "(")?;
                x.fmt(f)?;
                write!(f, ")")?;
            } else {
                x.fmt(f)?;
            }
            Ok(())
        }

        fn display_function(
            fn_name: &str,
            inner: &S,
            f: &mut fmt::Formatter<'_>,
        ) -> fmt::Result {
            write!(f, "{}(", fn_name)?;
            inner.fmt(f)?;
            write!(f, ")")?;
            Ok(())
        }

        match self {
            S::Literal(x) => x.fmt(f),
            S::Symbol(x) => {
                if x.contains(' ') {
                    write!(f, "(")?;
                    x.fmt(f)?;
                    write!(f, ")")?;
                    Ok(())
                } else {
                    x.fmt(f)
                }
            },
            S::Neg(x) => {
                write!(f, "-")?;
                write_parens_if(f, x, x.is_add() || x.is_sub() || x.is_neg())?;
                Ok(())
            },
            S::Add(l, r) => {
                l.fmt(f)?;
                if let S::Neg(n) = r.as_ref() {
                    write!(f, " - ")?;
                    write_parens_if(
                        f, n, n.is_add() || n.is_sub() || n.is_neg())?;
                } else {
                    write!(f, " + ")?;
                    r.fmt(f)?;
                }
                Ok(())
            },
            S::Sub(l, r) => {
                l.fmt(f)?;
                if let S::Neg(n) = r.as_ref() {
                    write!(f, " + ")?;
                    n.fmt(f)?;
                } else {
                    write!(f, " - ")?;
                    write_parens_if(
                        f, r, r.is_add() || r.is_sub() || r.is_neg())?;
                }
                Ok(())
            },
            S::Mul(l, r) => {
                write_parens_if(f, l, l.is_add() || l.is_sub())?;
                write!(f, " * ")?;
                write_parens_if(f, r, r.is_add() || r.is_sub() || r.is_neg())?;
                Ok(())
            },
            S::Div(l, r) => {
                write_parens_if(f, l, l.is_add() || l.is_sub())?;
                write!(f, " / ")?;
                write_parens_if(
                    f, r,
                    r.is_add()
                    || r.is_sub()
                    || r.is_mul()
                    || r.is_div()
                )?;
                Ok(())
            },
            S::Sqrt(x) => {
                write!(f, "√")?;
                write_parens_if(f, x, true)?;
                Ok(())
            },
            S::Sin(x) => display_function("sin", x, f),
            S::Cos(x) => display_function("cos", x, f),
            S::Tan(x) => display_function("tan", x, f),
            S::ASin(x) => display_function("arcsin", x, f),
            S::ACos(x) => display_function("arccos", x, f),
            S::ATan(x) => display_function("arctan", x, f),
            S::Sinh(x) => display_function("sinh", x, f),
            S::Cosh(x) => display_function("cosh", x, f),
            S::Tanh(x) => display_function("tanh", x, f),
            S::ASinh(x) => display_function("arsinh", x, f),
            S::ACosh(x) => display_function("arcosh", x, f),
            S::ATanh(x) => display_function("artanh", x, f),
            S::Pow(b, e) => {
                write_parens_if(
                    f, b,
                    b.is_add()
                    || b.is_sub()
                    || b.is_mul()
                    || b.is_div()
                    || b.is_neg()
                )?;
                write!(f, "^")?;
                write_parens_if(
                    f, e,
                    e.is_add()
                    || e.is_sub()
                    || e.is_mul()
                    || e.is_div()
                    || e.is_neg()
                )?;
                Ok(())
            },
            S::Exp(x) => display_function("exp", x, f),
            S::Log(b, x) => {
                write!(f, "log_")?;
                write_parens_if(
                    f, b,
                    b.is_add()
                    || b.is_sub()
                    || b.is_mul()
                    || b.is_div()
                    || b.is_neg()
                )?;
                write_parens_if(f, x, true)?;
                Ok(())
            },
            S::Log2(x) => display_function("log2", x, f),
            S::Log10(x) => display_function("log10", x, f),
            S::Ln(x) => display_function("ln", x, f),
        }
    }
}

pub mod consts {
    use ordered_float::OrderedFloat;
    use super::S;

    /// Euler’s number (e)
    pub const E: S = S::Literal(OrderedFloat(std::f64::consts::E));

    /// 1/π
    pub const FRAC_1_PI: S = S::Literal(OrderedFloat(std::f64::consts::FRAC_1_PI));

    /// 1/sqrt(2)
    pub const FRAC_1_SQRT_2: S = S::Literal(OrderedFloat(std::f64::consts::FRAC_1_SQRT_2));

    /// 2/π
    pub const FRAC_2_PI: S = S::Literal(OrderedFloat(std::f64::consts::FRAC_2_PI));

    /// 2/sqrt(π)
    pub const FRAC_2_SQRT_PI: S = S::Literal(OrderedFloat(std::f64::consts::FRAC_2_SQRT_PI));

    /// π/2
    pub const FRAC_PI_2: S = S::Literal(OrderedFloat(std::f64::consts::FRAC_PI_2));

    /// π/3
    pub const FRAC_PI_3: S = S::Literal(OrderedFloat(std::f64::consts::FRAC_PI_3));

    /// π/4
    pub const FRAC_PI_4: S = S::Literal(OrderedFloat(std::f64::consts::FRAC_PI_4));

    /// π/6
    pub const FRAC_PI_6: S = S::Literal(OrderedFloat(std::f64::consts::FRAC_PI_6));

    /// π/8
    pub const FRAC_PI_8: S = S::Literal(OrderedFloat(std::f64::consts::FRAC_PI_8));

    /// ln(2)
    pub const LN_2: S = S::Literal(OrderedFloat(std::f64::consts::LN_2));

    /// ln(10)
    pub const LN_10: S = S::Literal(OrderedFloat(std::f64::consts::LN_10));

    /// log2(10)
    pub const LOG2_10: S = S::Literal(OrderedFloat(std::f64::consts::LOG2_10));

    /// log2(e)
    pub const LOG2_E: S = S::Literal(OrderedFloat(std::f64::consts::LOG2_E));

    /// log10(2)
    pub const LOG10_2: S = S::Literal(OrderedFloat(std::f64::consts::LOG10_2));

    /// log10(e)
    pub const LOG10_E: S = S::Literal(OrderedFloat(std::f64::consts::LOG10_E));

    /// Archimedes’ pub constant (π)
    pub const PI: S = S::Literal(OrderedFloat(std::f64::consts::PI));

    /// sqrt(2)
    pub const SQRT_2: S = S::Literal(OrderedFloat(std::f64::consts::SQRT_2));

    /// The full circle pub constant (τ)
    pub const TAU: S = S::Literal(OrderedFloat(std::f64::consts::TAU));
}

// match self {
//     S::Literal(x) => todo!(),
//     S::Symbol(x) => todo!(),
//     S::Neg(x) => todo!(),
//     S::Add(l, r) => todo!(),
//     S::Sub(l, r) => todo!(),
//     S::Mul(l, r) => todo!(),
//     S::Div(l, r) => todo!(),
//     S::Sqrt(x) => todo!(),
//     S::Sin(x) => todo!(),
//     S::Cos(x) => todo!(),
//     S::Tan(x) => todo!(),
//     S::ASin(x) => todo!(),
//     S::ACos(x) => todo!(),
//     S::ATan(x) => todo!(),
//     S::Sinh(x) => todo!(),
//     S::Cosh(x) => todo!(),
//     S::Tanh(x) => todo!(),
//     S::ASinh(x) => todo!(),
//     S::ACosh(x) => todo!(),
//     S::ATanh(x) => todo!(),
//     S::Pow(b, e) => todo!(),
//     S::Exp(x) => todo!(),
//     S::Log(b, x) => todo!(),
//     S::Log2(x) => todo!(),
//     S::Log10(x) => todo!(),
//     S::Ln(x) => todo!(),
// }

