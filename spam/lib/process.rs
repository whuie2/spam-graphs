//! Simple tools for building, traversing, and rendering process graphs.

use std::{
    fmt,
    fs,
    io::Write,
    ops::{ Deref, DerefMut },
    path::Path,
};
use petgraph::{ Directed, graphmap::GraphMap };
use thiserror::Error;
use crate::{
    symbolic::S,
};

#[derive(Debug, Error)]
pub enum ProcessError {
    // #[error("invalid transition probabilities from node [{0}]: must sum to 0 or 1")]
    // InvalidTransitionProbs(String),

    #[error("error constructing GraphViz representation: {0}")]
    GraphVizError(String),

    #[error("I/O error: {0}")]
    IOError(#[from] std::io::Error),
}
pub type ProcessResult<T> = Result<T, ProcessError>;

/// Trait for a node type used in a graph.
///
/// Nodes are ideally represented as a single `enum` type with a variant for
/// each unique node in the graph.
///
/// [`petgraph::graphmap::NodeTrait`] expands out to
/// [`Copy`]` + `[`Ord`]` + `[`Hash`].
pub trait ProcessState: petgraph::graphmap::NodeTrait {
    /// Associate a [`String`] label with each available graph node.
    fn label(&self) -> String;
}

/// [`Vec<N>`] newtype representing a path through a graph as a series of nodes.
#[derive(Clone, Debug)]
pub struct NodePath<N>(Vec<N>);

impl<N> From<Vec<N>> for NodePath<N>
{
    fn from(v: Vec<N>) -> Self { Self(v) }
}

impl<N> AsRef<Vec<N>> for NodePath<N>
{
    fn as_ref(&self) -> &Vec<N> { &self.0 }
}

impl<N> AsMut<Vec<N>> for NodePath<N>
{
    fn as_mut(&mut self) -> &mut Vec<N> { &mut self.0 }
}

impl<N> Deref for NodePath<N>
{
    type Target = Vec<N>;

    fn deref(&self) -> &Self::Target { &self.0 }
}

impl<N> DerefMut for NodePath<N>
{
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.0 }
}

impl<N> FromIterator<N> for NodePath<N>
{
    fn from_iter<I>(iter: I) -> Self
    where I: IntoIterator<Item = N>
    {
        Self(iter.into_iter().collect())
    }
}

impl<N> IntoIterator for NodePath<N>
{
    type Item = N;
    type IntoIter = <Vec<N> as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter { self.0.into_iter() }
}

impl<N> fmt::Display for NodePath<N>
where N: fmt::Display
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let n: usize = self.len();
        for (k, nk) in self.iter().enumerate() {
            write!(f, "[")?;
            nk.fmt(f)?;
            write!(f, "]")?;
            if k < n - 1 { write!(f, " -> ")?; }
        }
        Ok(())
    }
}

/// [`GraphMap<N, S, Directed>`] newtype representing a single process graph.
///
/// While this type is generic over node types `N`, each transition probability
/// should be represented as a symbolic expression [`S`].
#[derive(Clone, Debug)]
pub struct ProcessGraph<N>(GraphMap<N, S, Directed>)
where N: ProcessState;

impl<N> AsRef<GraphMap<N, S, Directed>> for ProcessGraph<N>
where N: ProcessState
{
    fn as_ref(&self) -> &GraphMap<N, S, Directed> { &self.0 }
}

impl<N> AsMut<GraphMap<N, S, Directed>> for ProcessGraph<N>
where N: ProcessState
{
    fn as_mut(&mut self) -> &mut GraphMap<N, S, Directed> { &mut self.0 }
}

impl<N> Deref for ProcessGraph<N>
where N: ProcessState
{
    type Target = GraphMap<N, S, Directed>;

    fn deref(&self) -> &Self::Target { &self.0 }
}

impl<N> DerefMut for ProcessGraph<N>
where N: ProcessState
{
    fn deref_mut(&mut self) -> &mut Self::Target { &mut self.0 }
}

impl<N> FromIterator<(N, N, S)> for ProcessGraph<N>
where N: ProcessState
{
    fn from_iter<I>(iter: I) -> Self
    where I: IntoIterator<Item = (N, N, S)>
    {
        Self(iter.into_iter().collect())
    }
}

impl<N> From<GraphMap<N, S, Directed>> for ProcessGraph<N>
where N: ProcessState
{
    fn from(graph: GraphMap<N, S, Directed>) -> Self { Self(graph) }
}

impl<N> Default for ProcessGraph<N>
where N: ProcessState
{
    fn default() -> Self { Self::new() }
}

impl<N> ProcessGraph<N>
where N: ProcessState
{
    /// Create a new, empty process graph.
    pub fn new() -> Self { Self(GraphMap::new()) }

    /// Return a list of all possible paths through a graph such that each path
    /// begins with a node in `start_nodes`.
    ///
    /// Each path is associated with an expression giving the probability that
    /// the path is taken, given that it begins with a certain node. If multiple
    /// start nodes are passed to this function, the sum of all the returned
    /// probabilities will be greater than 1.
    ///
    /// Assumes that the graph is acyclic.
    pub fn dfs_all_paths(&self, start_nodes: &[N]) -> Vec<(NodePath<N>, S)> {
        fn do_dfs<N>(graph: &ProcessGraph<N>, start: &N, term: &S)
            -> Vec<(NodePath<N>, S)>
        where N: ProcessState
        {
            let mut paths: Vec<(NodePath<N>, S)> = Vec::new();
            for (_, neighbor, prob) in graph.edges(*start) {
                do_dfs(graph, &neighbor, prob)
                    .into_iter()
                    .for_each(|(subpath, subprob)| {
                        let path: NodePath<N>
                            = [*start].into_iter()
                            .chain(subpath)
                            .collect();
                        let prob: S = term * subprob;
                        paths.push((path, prob));
                    });
            }
            if paths.is_empty() {
                paths.push((vec![*start].into(), term.clone()));
            }
            paths
        }

        start_nodes.iter()
            .flat_map(|n0| do_dfs(self, n0, &1.0_f64.into()))
            .collect()
    }

    /// Return an object containing an encoding of `self` in the
    /// [dot language][dot-lang].
    ///
    /// Rendering this object using the default formatter will render a full dot
    /// string representation of the graph.
    ///
    /// [dot-lang]: https://en.wikipedia.org/wiki/DOT_(graph_description_language)
    pub fn to_graphviz<F, G>(&self, name: &str, node_fmt: F, edge_fmt: G)
        -> ProcessResult<tabbycat::Graph>
    where
        F: Fn(N) -> tabbycat::Identity,
        G: Fn(N, N, &S) -> tabbycat::Identity,
    {
        use tabbycat::*;
        let quoted = Identity::quoted;
        let mut statements
            = StmtList::new()
            .add_attr(
                AttrType::Graph,
                AttrList::new()
                    .add(quoted("nodesep"), 0.5.into())
                    .add(quoted("ranksep"), 1.0.into())
                    .add(quoted("rankdir"), quoted("LR"))
            )
            .add_attr(
                AttrType::Node,
                AttrList::new()
                    .add(quoted("shape"), quoted("box"))
                    .add(quoted("fontname"), quoted("DejaVu Sans"))
            )
            .add_attr(
                AttrType::Edge,
                AttrList::new()
                    .add(quoted("fontname"), quoted("DejaVu Sans"))
            );
        for node in self.nodes() {
            statements = statements.add_node(node_fmt(node), None, None);
        }
        for (n0, n1, prob) in self.all_edges() {
            statements
                = statements.add_edge(
                    Edge::head_node(node_fmt(n0), None)
                        .arrow_to_node(node_fmt(n1), None)
                        .add_attribute(
                            quoted("label"), edge_fmt(n0, n1, prob))
                );
        }
        GraphBuilder::default()
            .graph_type(GraphType::DiGraph)
            .strict(false)
            .id(Identity::quoted(name))
            .stmts(statements)
            .build()
            .map_err(ProcessError::GraphVizError)
    }

    /// Like [`Self::to_graphviz`], but render directly to a string and write it
    /// to `path`.
    pub fn save_graphviz<F, G, P>(
        &self,
        name: &str,
        node_fmt: F,
        edge_fmt: G,
        path: P,
    ) -> ProcessResult<()>
    where
        F: Fn(N) -> tabbycat::Identity,
        G: Fn(N, N, &S) -> tabbycat::Identity,
        P: AsRef<Path>,
    {
        let graphviz = self.to_graphviz(name, node_fmt, edge_fmt)?;
        fs::OpenOptions::new()
            .write(true)
            .append(false)
            .create(true)
            .truncate(true)
            .open(path)?
            .write_all(format!("{}", graphviz).as_bytes())?;
        Ok(())
    }
}

impl<N> fmt::Display for ProcessGraph<N>
where N: ProcessState
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let n: usize = self.edge_count();
        for (k, (n0, n1, w)) in self.all_edges().enumerate() {
            write!(f, "[")?;
            n0.label().fmt(f)?;
            write!(f, "] -> [")?;
            n1.label().fmt(f)?;
            write!(f, "]: ")?;
            w.fmt(f)?;
            if k < n - 1 { writeln!(f)?; }
        }
        Ok(())
    }
}

/// Simple macro to concisely create a process graph.
///
/// ```ignore
/// use spam::symbolic::S;
///
/// enum Node { A, B, C, D }
///
/// fn main() {
///     let p = S::new("p");
///     //   B---D
///     //  / p
///     // A
///     //  \ 1 - p
///     //   C
///     let graph = process_graph!(
///         A => {
///             B ; p.clone(),
///             C ; 1.0 - &p,
///         },
///         B => { D ; S::new(1.0) },
///     );
/// }
/// ```
#[macro_export]
macro_rules! process_graph {
    ( $( $n0:expr => { $( $n1:expr ; $w:expr ),* $(,)? } ),* $(,)? ) => {
        {
            #[allow(unused_mut)]
            let mut data = $crate::process::ProcessGraph::new();
            $($(
                data.add_edge($n0, $n1, $w);
            )*)*
            data
        }
    }
}

