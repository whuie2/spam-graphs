#![allow(dead_code, non_snake_case, non_upper_case_globals)]

use tabbycat::Identity;
use nalgebra as na;
use ndarray as nd;
use spam::{
    process::{ ProcessState, ProcessGraph },
    symbolic::S,
};

#[allow(non_camel_case_types)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum Node {
    Start,
    Init_0,
    Init_1,
    Init_E,
    ImageA_B0,
    ImageA_D0,
    ImageA_B1,
    ImageA_D1,
    ImageA_BE,
    ImageA_DE,
    SurvA_0,
    SurvA_1,
    SurvA_E,
    ClockPiA_0,
    ClockPiA_1,
    ClockPiA_C0,
    ClockPiA_E,
    Raman_C0,
    Raman_C1,
    Raman_0,
    Raman_1,
    Raman_E,
    ClockPiB_0,
    ClockPiB_1,
    ClockPiB_C0,
    ClockPiB_C1,
    ClockPiB_E,
    Pump_0,
    Pump_1,
    Pump_C0,
    Pump_C1,
    Pump_E,
    Depump_C0,
    Depump_C1,
    Depump_0,
    Depump_1,
    Depump_E,
    ImageB_B0,
    ImageB_D0,
    ImageB_B1, 
    ImageB_D1, 
    ImageB_BC0,
    ImageB_DC0,
    ImageB_BC1,
    ImageB_DC1,
    ImageB_BE,
    ImageB_DE,
}

impl ProcessState for Node {
    fn label(&self) -> String { format!("{:?}", self) }
}

#[derive(Copy, Clone, Debug)]
struct Vars {
    h_clockpi: f64,
    P_raman: f64,
}

impl From<Vars> for nd::Array1<f64> {
    fn from(v: Vars) -> Self { nd::array![v.h_clockpi, v.P_raman] }
}

impl From<nd::Array1<f64>> for Vars {
    fn from(a: nd::Array1<f64>) -> Self {
        if a.len() == 2 {
            Self { h_clockpi: a[0], P_raman: a[1] }
        } else {
            panic!("invalid conversion from Array1 to Vars");
        }
    }
}

impl From<Vars> for na::SVector<f64, 2> {
    fn from(v: Vars) -> Self { na::SVector::from([v.h_clockpi, v.P_raman]) }
}

impl From<na::SVector<f64, 2>> for Vars {
    fn from(a: na::SVector<f64, 2>) -> Self {
        Self { h_clockpi: a[0], P_raman: a[1] }
    }
}

#[derive(Copy, Clone, Debug)]
struct Consts {
    p: f64,
    h_OP: f64,
    F_0: f64,
    F_1: f64,
    P_01: f64,
    P_10: f64,
    h_surv0: f64,
    h_surv1: f64,
    P_depump: f64,
}

fn gen_graph() -> ProcessGraph<Node> {
    use Node::*;
    let p = S::new("p");
    let h_OP = S::new("h_OP");
    let F_0 = S::new("F_0");
    let F_1 = S::new("F_1");
    let P_01 = S::new("P_01");
    let P_10 = S::new("P_10");
    let h_surv0 = S::new("h_survB");
    let h_surv1 = S::new("h_survD");
    let h_clockpi = S::new("h_clockpi");
    let P_depump = S::new("P_depump");
    let P_raman = S::new("P_raman");
    spam::process_graph!(
        // start -> load
        Start => {
            Init_0 ; &p * &h_OP,
            Init_1 ; &p * (1.0 - &h_OP),
            Init_E ; 1.0 - &p,
        },
        // -> image A
        Init_0 => {
            ImageA_B0 ; F_0.clone(),
            ImageA_D0 ; 1.0 - &F_0,
        },
        Init_1 => {
            ImageA_B1 ; 1.0 - &F_1,
            ImageA_D1 ; F_1.clone(),
        },
        Init_E => {
            ImageA_BE ; 1.0 - &F_1,
            ImageA_DE ; F_1.clone(),
        },
        // -> post-image A survival
        ImageA_B0 => {
            SurvA_0 ; &h_surv0 * (1.0 - &P_01),
            SurvA_1 ; &h_surv0 * &P_01,
            SurvA_E ; 1.0 - &h_surv0,
        },
        ImageA_D0 => {
            SurvA_0 ; &h_surv0 * (1.0 - &P_01),
            SurvA_1 ; &h_surv0 * &P_01,
            SurvA_E ; 1.0 - &h_surv0,
        },
        ImageA_B1 => {
            SurvA_0 ; &h_surv1 * &P_10,
            SurvA_1 ; &h_surv1 * (1.0 - &P_10),
            SurvA_E ; 1.0 - &h_surv1,
        },
        ImageA_B1 => {
            SurvA_0 ; &h_surv1 * &P_10,
            SurvA_1 ; &h_surv1 * (1.0 - &P_10),
            SurvA_E ; 1.0 - &h_surv1,
        },
        ImageA_BE => { SurvA_E ; 1.0_f64.into() },
        ImageA_DE => { SurvA_E ; 1.0_f64.into() },
        // -> clock pi A
        SurvA_0 => { ClockPiA_0 ; 1.0_f64.into() },
        SurvA_1 => {
            ClockPiA_1 ; 1.0 - &h_clockpi,
            ClockPiA_C0 ; h_clockpi.clone(),
        },
        SurvA_E => { ClockPiA_E ; 1.0_f64.into() },
        // -> raman
        ClockPiA_0 => { Raman_0 ; 1.0_f64.into() },
        ClockPiA_1 => { Raman_1 ; 1.0_f64.into() },
        ClockPiA_C0 => {
            Raman_C0 ; 1.0 - &P_raman,
            Raman_C1 ; P_raman.clone(),
        },
        ClockPiA_E => { Raman_E ; 1.0_f64.into() },
        // -> clock pi B
        Raman_0 => { ClockPiB_0 ; 1.0_f64.into() },
        Raman_1 => {
            ClockPiB_1 ; 1.0 - &h_clockpi,
            ClockPiB_C0 ; h_clockpi.clone(),
        },
        Raman_C0 => {
            ClockPiB_1 ; h_clockpi.clone(),
            ClockPiB_C0 ; 1.0 - &h_clockpi,
        },
        Raman_C1 => { ClockPiB_C1 ; 1.0_f64.into() },
        Raman_E => { ClockPiB_E ; 1.0_f64.into() },
        // -> pre-image B pump
        ClockPiB_0 => {
            Pump_0 ; h_OP.clone(),
            Pump_1 ; 1.0 - &h_OP,
        },
        ClockPiB_1 => {
            Pump_0 ; h_OP.clone(),
            Pump_1 ; 1.0 - &h_OP,
        },
        ClockPiB_C0 => { Pump_C0 ; 1.0_f64.into() },
        ClockPiB_C1 => { Pump_C1 ; 1.0_f64.into() },
        ClockPiB_E => { Pump_E ; 1.0_f64.into() },
        // -> pre-image B tweezer depump
        Pump_0 => { Depump_0 ; 1.0_f64.into() },
        Pump_1 => { Depump_1 ; 1.0_f64.into() },
        Pump_C0 => {
            Depump_0 ; &P_depump / 2.0,
            Depump_1 ; &P_depump / 2.0,
            Depump_C0 ; 1.0 - &P_depump,
        },
        Pump_C1 => {
            Depump_0 ; &P_depump / 2.0,
            Depump_1 ; &P_depump / 2.0,
            Depump_C1 ; 1.0 - &P_depump,
        },
        Pump_E => { Depump_E ; 1.0_f64.into() },
        // -> image B
        Depump_0 => {
            ImageB_B0 ; F_0.clone(),
            ImageB_D0 ; 1.0 - &F_0,
        },
        Depump_1 => {
            ImageB_B1 ; 1.0 - &F_1,
            ImageB_D1 ; F_1.clone(),
        },
        Depump_C0 => {
            ImageB_BC0 ; 1.0 - &F_1,
            ImageB_DC0 ; F_1.clone(),
        },
        Depump_C1 => {
            ImageB_BC1 ; 1.0 - &F_1,
            ImageB_DC1 ; F_1.clone(),
        },
        Depump_E => {
            ImageB_BE ; 1.0 - &F_1,
            ImageB_DE ; F_1.clone(),
        },
    )
}

fn main() {
    let graph = gen_graph();
    graph
        .save_graphviz(
            "clock-raman",
            |n| Identity::quoted(n.label()),
            |_, _, p| Identity::quoted(format!("{}", p)),
            "clock-raman.gv",
        )
        .expect("problem writing graphviz file");
    let all_paths = graph.dfs_all_paths(&[Node::Start]);
    for (path, prob) in all_paths.iter() {
        println!("{:?}", path);
        println!("  {}", prob.to_string_rust());
    }
}
