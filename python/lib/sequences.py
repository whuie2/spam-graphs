"""
Defines the general structure `Sequence` of a sequence model, as well as a
handful of instantiations. If run as a script, verify the construction of these
instantiations and render their graphs with graphviz.
"""

import sys
import graphviz
import numpy as np
import numpy.linalg as la
import sympy as sy
from itertools import product
try:
    from .graph import Node, dfs_all_paths
except ImportError:
    from graph import Node, dfs_all_paths
try:
    from .symbols import S, latexify
except ImportError:
    from symbols import S, latexify

np.random.seed(10547)

def symbol_format(s: str) -> str:
    """
    Apply graphviz-simplified HTML text formatting to stringified sympy
    expressions.
    """
    return (
        "<" + (
            s
            .replace("-", "–") # hypen to en-dash
            .replace("*", " ") # replace explicit multiplication
            .replace("F0", "<I>F</I><SUB>0</SUB>")
            .replace("F1", "<I>F</I><SUB>1</SUB>")
            .replace("hB", "<I>η</I><SUB>B</SUB>")
            .replace("hD", "<I>η</I><SUB>D</SUB>")
            .replace("PDB", "<I>P</I><SUB>D → B</SUB>")
            .replace("PBD", "<I>P</I><SUB>B → D</SUB>")
            .replace("hOP", "<I>η</I><SUB>OP</SUB>")
            .replace("hpi", "<I>η</I><SUB>π</SUB>")
            .replace("hsurv", "<I>η</I><SUB>surv</SUB>")
            .replace("hcpi", "<I>η</I><SUB>cπ</SUB>")
            .replace("p", "<I>p</I>")
        ) + ">"
    )

# def symbol_format(s: str) -> str:
#     """
#     Apply LaTeX math formatting to stringified sympy expressions.
#     """
#     return (
#         "$" + (
#             s
#             .replace("*", " ") # replace explicit multiplication
#             .replace("(", r"\left(")
#             .replace(")", r"\right)")
#             .replace("F0", r"F_0")
#             .replace("F1", r"F_1")
#             .replace("hB", r"\eta_\text{surv}^\text{B}")
#             .replace("hD", r"\eta_\text{surv}^\text{D}")
#             .replace("PDB", r"P_\text{depol}^\text{D\,$\rightarrow$\,B}")
#             .replace("PBD", r"P_\text{depol}^\text{B\,$\rightarrow$\,D}")
#             .replace("hOP", r"\eta_\text{OP}")
#             .replace("hpi", r"\eta_\pi")
#             .replace("p", r"p")
#         ) + "$"
#     )

class Sequence:
    """
    Abstract definition for all specific sequence graphs. Comes with methods to
    render the graph using graphviz and to verify that all edgeweights from any
    node sum to either 1 or 0.

    Attributes
    ----------
    name : str
        String name of the sequence, used for graph rendering.
    graph : dict[symbols.Node, dict[symbols.Node, sympy.Symbol]]
        Defines the sequence graph. In this structure, the outer `dict` defines
        an edge start point, and an inner `dict` defines edge endpoints with
        edge weights.
    """
    name: str
    graph: dict[Node, dict[Node, sy.Symbol]] # Once-nested dictionary of `Node`s

    def render(self, filename: str=None, fmt: str="pdf"):
        """
        Render the graph with graphviz, with output formatted as a `fmt`.
        """
        dot = graphviz.Digraph(
            self.name,
            graph_attr={
                "nodesep": "0.5",
                "ranksep": "1.0",
                "rankdir": "LR",
            },
            node_attr={
                "shape": "box",
                "fontname": "DejaVu Sans",
                # "fontname": "Myriad Pro",
            },
            edge_attr={
                "fontname": "DejaVu Sans",
                # "fontname": "Myriad Pro",
            },
        )
        for node in self.graph.keys():
            dot.node(node.label)
        for node0, neighbors in self.graph.items():
            for node1, prob in neighbors.items():
                dot.edge(node0.label, node1.label, symbol_format(str(prob)))
        dot.format = fmt
        dot.render()

    def verify_graph(self):
        """
        Verify that the weights of all edges coming from each node sum to 1. If
        they sum to 0, which should be the case for any terminal node, emit a
        warning instead.
        """
        tails = set(self.graph.keys())
        ex = {
            S.F0: 0.5,
            S.F1: 0.5,
            S.p: 0.5,
            S.hB: 0.5,
            S.hD: 0.5,
            S.PBD: 0.5,
            S.PDB: 0.5,
            S.hOP: 0.5,
            S.hpi: 0.5,
            S.hsurv: 0.5,
            S.hcpi: 0.5,
        }
        for node0, neighbors in self.graph.items():
            prob_sum = sum(w.evalf(subs=ex) for w in neighbors.values())
            if not abs(prob_sum - 1) < 1e-6:
                if abs(prob_sum) < 1e-6:
                    print(
                        f"warning: possibly malconstructed edge weights from"
                        f" node {node0}: {prob_sum}",
                        file=sys.stderr,
                    )
                else:
                    raise Exception(
                        f"malconstructed edge weights from node {node0}:"
                        f" {prob_sum}"
                    )
            acc = sy.S(0)
            for node1, prob in neighbors.items():
                if node1 not in tails:
                    raise Exception(f"missing tail {node1}")

class OneShot(Sequence):
    name = "one-shot"

    # init
    start = Node("Start")
    # pump 0
    pump0_0 = Node("Load and\\npump to ∣0⟩")
    pump0_1 = Node("Load and\\npump to ∣1⟩")
    pump0_e = Node("Empty\\ntweezer")
    # readout 0
    read0_B_0 = Node("Readout\\nBright in ∣0⟩)")
    read0_D_0 = Node("Readout\\nDark in ∣0⟩")
    read0_B_1 = Node("Readout\\nBright in ∣1⟩")
    read0_D_1 = Node("Readout\\nDark in ∣1⟩")
    read0_B_e = Node("Readout\\nBright in ∣∅⟩")
    read0_D_e = Node("Readout\\nDark in ∣∅⟩")

    graph = {
        start: {
            pump0_0: S.p * S.hOP,
            pump0_1: S.p * (1 - S.hOP),
            pump0_e: 1 - S.p,
        },
        pump0_0: {
            read0_B_0: S.F1,
            read0_D_0: 1 - S.F1,
        },
        pump0_1: {
            read0_B_1: 1 - S.F0,
            read0_D_1: S.F0,
        },
        pump0_e: {
            read0_B_e: 1 - S.F0,
            read0_D_e: S.F0,
        },
        read0_B_0: { },
        read0_D_0: { },
        read0_B_1: { },
        read0_D_1: { },
        read0_B_e: { },
        read0_D_e: { },
    }

class TwoShot(Sequence):
    name = "two-shot"

    # init
    start = Node("Start")
    # pump 0
    pump0_0 = Node("Load and\\npump to ∣0⟩")
    pump0_1 = Node("Load and\\npump to ∣1⟩")
    pump0_e = Node("Empty\\ntweezer")
    # readout 0
    read0_B_0 = Node("Readout 0\\nBright in ∣0⟩")
    read0_D_0 = Node("Readout 0\\nDark in ∣0⟩")
    read0_B_1 = Node("Readout 0\\nBright in ∣1⟩")
    read0_D_1 = Node("Readout 0\\nDark in ∣1⟩")
    read0_B_e = Node("Readout 0\\nBright in ∣∅⟩")
    read0_D_e = Node("Readout 0\\nDark in ∣∅⟩")
    # post-readout 0
    surv0_0 = Node("Survived\\nin ∣0⟩")
    surv0_1 = Node("Survived\\nin ∣1⟩")
    surv0_x = Node("Survived")
    surv0_l = Node("Lost")
    # pump 1
    pump1_0 = Node("Pump\\nto ∣0⟩")
    pump1_1 = Node("Pump\\nto ∣1⟩")
    # readout 1
    read1_B_0 = Node("Readout 1\\nBright in ∣0⟩")
    read1_D_0 = Node("Readout 1\\nDark in ∣0⟩")
    read1_B_1 = Node("Readout 1\\nBright in ∣1⟩")
    read1_D_1 = Node("Readout 1\\nDark in ∣1⟩")
    read1_B_l = Node("Readout 1\\nBright in ∣∅⟩")
    read1_D_l = Node("Readout 1\\nDark in ∣∅⟩")

    graph = {
        start: {
            pump0_0: S.p * S.hOP,
            pump0_1: S.p * (1 - S.hOP),
            pump0_e: 1 - S.p,
        },
        pump0_0: {
            read0_B_0: S.F1,
            read0_D_0: 1 - S.F1,
        },
        pump0_1: {
            read0_B_1: 1 - S.F0,
            read0_D_1: S.F0,
        },
        pump0_e: {
            read0_B_e: 1 - S.F0,
            read0_D_e: S.F0,
        },
        read0_B_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_l: 1 - S.hB,
        },
        read0_D_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_l: 1 - S.hB,
        },
        read0_B_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_l: 1 - S.hD,
        },
        read0_D_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_l: 1 - S.hD,
        },
        read0_B_e: {
            surv0_l: sy.S(1),
        },
        read0_D_e: {
            surv0_l: sy.S(1),
        },
        surv0_0: {
            surv0_x: sy.S(1),
        },
        surv0_1: {
            surv0_x: sy.S(1),
        },
        surv0_x: {
            pump1_0: S.hOP,
            pump1_1: 1 - S.hOP,
        },
        surv0_l: {
            read1_B_l: 1 - S.F0,
            read1_D_l: S.F0,
        },
        pump1_0: {
            read1_B_0: S.F1,
            read1_D_0: 1 - S.F1,
        },
        pump1_1: {
            read1_B_1: 1 - S.F0,
            read1_D_1: S.F0,
        },
        read1_B_0: { },
        read1_D_0: { },
        read1_B_1: { },
        read1_D_1: { },
        read1_B_l: { },
        read1_D_l: { },
    }

class ThreeShot(Sequence):
    name = "three-shot"

    # init
    start = Node("Start")
    start_0 = Node("Load ∣0⟩")
    start_1 = Node("Load ∣1⟩")
    start_e = Node("Empty\\ntweezer")
    # readout 0
    read0_B_0 = Node("Readout 0\\nBright in ∣0⟩")
    read0_D_0 = Node("Readout 0\\nDark in ∣0⟩")
    read0_B_1 = Node("Readout 0\\nBright in ∣1⟩")
    read0_D_1 = Node("Readout 0\\nDark in ∣1⟩")
    read0_B_e = Node("Readout 0\\nBright in ∣∅⟩")
    read0_D_e = Node("Readout 0\\nDark in ∣∅⟩")
    # post-readout 0
    surv0_0 = Node("Survived (0)\\nin ∣0⟩")
    surv0_1 = Node("Survived (0)\\nin ∣1⟩")
    surv0_l = Node("Lost (0)")
    # readout 1
    read1_B_0 = Node("Readout 1\\nBright in ∣0⟩")
    read1_D_0 = Node("Readout 1\\nDark in ∣0⟩")
    read1_B_1 = Node("Readout 1\\nBright in ∣1⟩")
    read1_D_1 = Node("Readout 1\\nDark in ∣1⟩")
    read1_B_l = Node("Readout 1\\nBright in ∣∅⟩")
    read1_D_l = Node("Readout 1\\nDark in ∣∅⟩")
    # post-readout 1
    surv1_0 = Node("Survived (1)\\nin ∣0⟩")
    surv1_1 = Node("Survived (1)\\nin ∣1⟩")
    surv1_x = Node("Survived (1)")
    surv1_l = Node("Lost (1)")
    # pump
    pump_0 = Node("Pump\\nto ∣0⟩")
    pump_1 = Node("Pump\\nto ∣1⟩")
    # readout 2
    read2_B_0 = Node("Readout 2\\nBright in ∣0⟩")
    read2_D_0 = Node("Readout 2\\nDark in ∣0⟩")
    read2_B_1 = Node("Readout 2\\nBright in ∣1⟩")
    read2_D_1 = Node("Readout 2\\nDark in ∣1⟩")
    read2_B_l = Node("Readout 2\\nBright in ∣∅⟩")
    read2_D_l = Node("Readout 2\\nDark in ∣∅⟩")

    graph = {
        start: {
            start_0: S.p / 2,
            start_1: S.p / 2,
            start_e: 1 - S.p,
        },
        start_0: {
            read0_B_0: S.F1,
            read0_D_0: 1 - S.F1,
        },
        start_1: {
            read0_B_1: 1 - S.F0,
            read0_D_1: S.F0,
        },
        start_e: {
            read0_B_e: 1 - S.F0,
            read0_D_e: S.F0,
        },
        read0_B_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_l: 1 - S.hB,
        },
        read0_D_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_l: 1 - S.hB,
        },
        read0_B_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_l: 1 - S.hD,
        },
        read0_D_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_l: 1 - S.hD,
        },
        read0_B_e: {
            surv0_l: sy.S(1),
        },
        read0_D_e: {
            surv0_l: sy.S(1),
        },
        surv0_0: {
            read1_B_0: S.F1,
            read1_D_0: 1 - S.F1,
        },
        surv0_1: {
            read1_B_1: 1 - S.F0,
            read1_D_1: S.F0,
        },
        surv0_l: {
            read1_B_l: 1 - S.F0,
            read1_D_l: S.F0,
        },
        read1_B_0: {
            surv1_0: S.hB * (1 - S.PBD),
            surv1_1: S.hB * S.PBD,
            surv1_l: 1 - S.hB,
        },
        read1_D_0: {
            surv1_0: S.hB * (1 - S.PBD),
            surv1_1: S.hB * S.PBD,
            surv1_l: 1 - S.hB,
        },
        read1_B_1: {
            surv1_0: S.hD * S.PDB,
            surv1_1: S.hD * (1 - S.PDB),
            surv1_l: 1 - S.hD,
        },
        read1_D_1: {
            surv1_0: S.hD * S.PDB,
            surv1_1: S.hD * (1 - S.PDB),
            surv1_l: 1 - S.hD,
        },
        read1_B_l: {
            surv1_l: sy.S(1),
        },
        read1_D_l: {
            surv1_l: sy.S(1),
        },
        surv1_0: {
            surv1_x: sy.S(1),
        },
        surv1_1: {
            surv1_x: sy.S(1),
        },
        surv1_x: {
            pump_0: S.hOP,
            pump_1: 1 - S.hOP,
        },
        surv1_l: {
            read2_B_l: 1 - S.F0,
            read2_D_l: S.F0,
        },
        pump_0: {
            read2_B_0: S.F1,
            read2_D_0: 1 - S.F1,
        },
        pump_1: {
            read2_B_1: 1 - S.F0,
            read2_D_1: S.F0,
        },
        read2_B_0: { },
        read2_D_0: { },
        read2_B_1: { },
        read2_D_1: { },
        read2_B_l: { },
        read2_D_l: { },
    }

class FourShot(Sequence):
    name = "four-shot"

    # init
    start = Node("Start")
    # pump 0
    pump0_0 = Node("Load and\\npump to ∣0⟩")
    pump0_1 = Node("Load and\\npump to ∣1⟩")
    pump0_e = Node("Empty\\ntweezer")
    # readout 0
    read0_B_0 = Node("Readout 0\\nBright in ∣0⟩")
    read0_D_0 = Node("Readout 0\\nDark in ∣0⟩")
    read0_B_1 = Node("Readout 0\\nBright in ∣1⟩")
    read0_D_1 = Node("Readout 0\\nDark in ∣1⟩")
    read0_B_e = Node("Readout 0\\nBright in ∣∅⟩")
    read0_D_e = Node("Readout 0\\nDark in ∣∅⟩")
    # post-readout 0
    surv0_0 = Node("Survived (0)\\nin ∣0⟩")
    surv0_1 = Node("Survived (0)\\nin ∣1⟩")
    surv0_l = Node("Lost (0)")
    # readout 1
    read1_B_0 = Node("Readout 1\\nBright in ∣0⟩")
    read1_D_0 = Node("Readout 1\\nDark in ∣0⟩")
    read1_B_1 = Node("Readout 1\\nBright in ∣1⟩")
    read1_D_1 = Node("Readout 1\\nDark in ∣1⟩")
    read1_B_l = Node("Readout 1\\nBright in ∣∅⟩")
    read1_D_l = Node("Readout 1\\nDark in ∣∅⟩")
    # post-readout 1
    surv1_0 = Node("Survived (1)\\nin ∣0⟩")
    surv1_1 = Node("Survived (1)\\nin ∣1⟩")
    surv1_l = Node("Lost (1)")
    # readout 2
    read2_B_0 = Node("Readout 2\\nBright in ∣0⟩")
    read2_D_0 = Node("Readout 2\\nDark in ∣0⟩")
    read2_B_1 = Node("Readout 2\\nBright in ∣1⟩")
    read2_D_1 = Node("Readout 2\\nDark in ∣1⟩")
    read2_B_l = Node("Readout 2\\nBright in ∣∅⟩")
    read2_D_l = Node("Readout 2\\nDark in ∣∅⟩")
    # post-readout 2
    surv2_0 = Node("Survived (2)\\nin ∣0⟩")
    surv2_1 = Node("Survived (2)\\nin ∣1⟩")
    surv2_x = Node("Survived (2)")
    surv2_l = Node("Lost (2)")
    # pump 1
    pump1_0 = Node("Pump\\nto ∣0⟩")
    pump1_1 = Node("Pump\\nto ∣1⟩")
    # readout 3
    read3_B_0 = Node("Readout 3\\nBright in ∣0⟩")
    read3_D_0 = Node("Readout 3\\nDark in ∣0⟩")
    read3_B_1 = Node("Readout 3\\nBright in ∣1⟩")
    read3_D_1 = Node("Readout 3\\nDark in ∣1⟩")
    read3_B_l = Node("Readout 3\\nBright in ∣∅⟩")
    read3_D_l = Node("Readout 3\\nDark in ∣∅⟩")

    graph = {
        start: {
            pump0_0: S.p * S.hOP,
            pump0_1: S.p * (1 - S.hOP),
            pump0_e: 1 - S.p,
        },
        pump0_0: {
            read0_B_0: S.F1,
            read0_D_0: 1 - S.F1,
        },
        pump0_1: {
            read0_B_1: 1 - S.F0,
            read0_D_1: S.F0,
        },
        pump0_e: {
            read0_B_e: 1 - S.F0,
            read0_D_e: S.F0,
        },
        read0_B_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_l: 1 - S.hB,
        },
        read0_D_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_l: 1 - S.hB,
        },
        read0_B_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_l: 1 - S.hD,
        },
        read0_D_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_l: 1 - S.hD,
        },
        read0_B_e: {
            surv0_l: sy.S(1),
        },
        read0_D_e: {
            surv0_l: sy.S(1),
        },
        surv0_0: {
            read1_B_0: S.F1,
            read1_D_0: 1 - S.F1,
        },
        surv0_1: {
            read1_B_1: 1 - S.F0,
            read1_D_1: S.F0,
        },
        surv0_l: {
            read1_B_l: 1 - S.F0,
            read1_D_l: S.F0,
        },
        read1_B_0: {
            surv1_0: S.hB * (1 - S.PBD),
            surv1_1: S.hB * S.PBD,
            surv1_l: 1 - S.hB,
        },
        read1_D_0: {
            surv1_0: S.hB * (1 - S.PBD),
            surv1_1: S.hB * S.PBD,
            surv1_l: 1 - S.hB,
        },
        read1_B_1: {
            surv1_0: S.hD * S.PDB,
            surv1_1: S.hD * (1 - S.PDB),
            surv1_l: 1 - S.hD,
        },
        read1_D_1: {
            surv1_0: S.hD * S.PDB,
            surv1_1: S.hD * (1 - S.PDB),
            surv1_l: 1 - S.hD,
        },
        read1_B_l: {
            surv1_l: sy.S(1),
        },
        read1_D_l: {
            surv1_l: sy.S(1),
        },
        surv1_0: {
            read2_B_0: S.F1,
            read2_D_0: 1 - S.F1,
        },
        surv1_1: {
            read2_B_1: 1 - S.F0,
            read2_D_1: S.F0,
        },
        surv1_l: {
            read2_B_l: 1 - S.F0,
            read2_D_l: S.F0,
        },
        read2_B_0: {
            surv2_0: S.hB * (1 - S.PBD),
            surv2_1: S.hB * S.PBD,
            surv2_l: 1 - S.hB,
        },
        read2_D_0: {
            surv2_0: S.hB * (1 - S.PBD),
            surv2_1: S.hB * S.PBD,
            surv2_l: 1 - S.hB,
        },
        read2_B_1: {
            surv2_0: S.hD * S.PDB,
            surv2_1: S.hD * (1 - S.PDB),
            surv2_l: 1 - S.hD,
        },
        read2_D_1: {
            surv2_0: S.hD * S.PDB,
            surv2_1: S.hD * (1 - S.PDB),
            surv2_l: 1 - S.hD,
        },
        read2_B_l: {
            surv2_l: sy.S(1),
        },
        read2_D_l: {
            surv2_l: sy.S(1),
        },
        surv2_0: {
            surv2_x: sy.S(1),
        },
        surv2_1: {
            surv2_x: sy.S(1),
        },
        surv2_x: {
            pump1_0: S.hOP,
            pump1_1: 1 - S.hOP,
        },
        surv2_l: {
            read3_B_l: 1 - S.F0,
            read3_D_l: S.F0,
        },
        pump1_0: {
            read3_B_0: S.F1,
            read3_D_0: 1 - S.F1,
        },
        pump1_1: {
            read3_B_1: 1 - S.F0,
            read3_D_1: S.F0,
        },
        read3_B_0: { },
        read3_D_0: { },
        read3_B_1: { },
        read3_D_1: { },
        read3_B_l: { },
        read3_D_l: { },
    }

class PiPulse(Sequence):
    name = "pi-pulse"

    # init
    start = Node("Start")
    start_0 = Node("Load ∣0⟩")
    start_1 = Node("Load ∣1⟩")
    start_e = Node("Empty\\ntweezer")
    # readout 0
    read0_B_0 = Node("Readout 0\\nBright in ∣0⟩")
    read0_D_0 = Node("Readout 0\\nDark in ∣0⟩")
    read0_B_1 = Node("Readout 0\\nBright in ∣1⟩")
    read0_D_1 = Node("Readout 0\\nDark in ∣1⟩")
    read0_B_e = Node("Readout 0\\nBright in ∣∅⟩")
    read0_D_e = Node("Readout 0\\nDark in ∣∅⟩")
    # post-readout 0
    surv0_0 = Node("Survived (0)\\nin ∣0⟩")
    surv0_1 = Node("Survived (0)\\nin ∣1⟩")
    surv0_l = Node("Lost (0)")
    # pi-pulse
    pipulse_0 = Node("π-pulse\\nto ∣0⟩")
    pipulse_1 = Node("π-pulse\\nto ∣1⟩")
    # readout 1
    read1_B_0 = Node("Readout 1\\nBright in ∣0⟩")
    read1_D_0 = Node("Readout 1\\nDark in ∣0⟩")
    read1_B_1 = Node("Readout 1\\nBright in ∣1⟩")
    read1_D_1 = Node("Readout 1\\nDark in ∣1⟩")
    read1_B_l = Node("Readout 1\\nBright in ∣∅⟩")
    read1_D_l = Node("Readout 1\\nDark in ∣∅⟩")
    # post-readout 1
    surv1_0 = Node("Survived (1)\\nin ∣0⟩")
    surv1_1 = Node("Survived (1)\\nin ∣1⟩")
    surv1_x = Node("Survived (1)")
    surv1_l = Node("Lost (1)")
    # pump
    pump_0 = Node("Pump\\nto ∣0⟩")
    pump_1 = Node("Pump\\nto ∣1⟩")
    # readout 2
    read2_B_0 = Node("Readout 2\\nBright in ∣0⟩")
    read2_D_0 = Node("Readout 2\\nDark in ∣0⟩")
    read2_B_1 = Node("Readout 2\\nBright in ∣1⟩")
    read2_D_1 = Node("Readout 2\\nDark in ∣1⟩")
    read2_B_l = Node("Readout 2\\nBright in ∣∅⟩")
    read2_D_l = Node("Readout 2\\nDark in ∣∅⟩")

    graph = {
        start: {
            start_0: S.p / 2,
            start_1: S.p / 2,
            start_e: 1 - S.p,
        },
        start_0: {
            read0_B_0: S.F1,
            read0_D_0: 1 - S.F1,
        },
        start_1: {
            read0_B_1: 1 - S.F0,
            read0_D_1: S.F0,
        },
        start_e: {
            read0_B_e: 1 - S.F0,
            read0_D_e: S.F0,
        },
        read0_B_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_l: 1 - S.hB,
        },
        read0_D_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_l: 1 - S.hB,
        },
        read0_B_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_l: 1 - S.hD,
        },
        read0_D_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_l: 1 - S.hD,
        },
        read0_B_e: {
            surv0_l: sy.S(1),
        },
        read0_D_e: {
            surv0_l: sy.S(1),
        },
        surv0_0: {
            pipulse_0: 1 - S.hpi,
            pipulse_1: S.hpi,
        },
        surv0_1: {
            pipulse_0: S.hpi,
            pipulse_1: 1 - S.hpi,
        },
        surv0_l: {
            read1_B_l: 1 - S.F0,
            read1_D_l: S.F0,
        },
        pipulse_0: {
            read1_B_0: S.F1,
            read1_D_0: 1 - S.F1,
        },
        pipulse_1: {
            read1_B_1: 1 - S.F0,
            read1_D_1: S.F0,
        },
        read1_B_0: {
            surv1_0: S.hB * (1 - S.PBD),
            surv1_1: S.hB * S.PBD,
            surv1_l: 1 - S.hB,
        },
        read1_D_0: {
            surv1_0: S.hB * (1 - S.PBD),
            surv1_1: S.hB * S.PBD,
            surv1_l: 1 - S.hB,
        },
        read1_B_1: {
            surv1_0: S.hD * S.PDB,
            surv1_1: S.hD * (1 - S.PDB),
            surv1_l: 1 - S.hD,
        },
        read1_D_1: {
            surv1_0: S.hD * S.PDB,
            surv1_1: S.hD * (1 - S.PDB),
            surv1_l: 1 - S.hD,
        },
        read1_B_l: {
            surv1_l: sy.S(1),
        },
        read1_D_l: {
            surv1_l: sy.S(1),
        },
        surv1_0: {
            surv1_x: sy.S(1),
        },
        surv1_1: {
            surv1_x: sy.S(1),
        },
        surv1_x: {
            pump_0: S.hOP,
            pump_1: 1 - S.hOP,
        },
        surv1_l: {
            read2_B_l: 1 - S.F0,
            read2_D_l: S.F0,
        },
        pump_0: {
            read2_B_0: S.F1,
            read2_D_0: 1 - S.F1,
        },
        pump_1: {
            read2_B_1: 1 - S.F0,
            read2_D_1: S.F0,
        },
        read2_B_0: { },
        read2_D_0: { },
        read2_B_1: { },
        read2_D_1: { },
        read2_B_l: { },
        read2_D_l: { },
    }

class Rampdown(Sequence):
    name = "ramp-down"

    # init
    start = Node("Start")
    start_0 = Node("Load ∣0⟩")
    start_1 = Node("Load ∣1⟩")
    start_e = Node("Empty\\ntweezer")
    # readout 0
    read0_B_0 = Node("Readout 0\\nBright in ∣0⟩")
    read0_D_0 = Node("Readout 0\\nDark in ∣0⟩")
    read0_B_1 = Node("Readout 0\\nBright in ∣1⟩")
    read0_D_1 = Node("Readout 0\\nDark in ∣1⟩")
    read0_B_e = Node("Readout 0\\nBright in ∣∅⟩")
    read0_D_e = Node("Readout 0\\nDark in ∣∅⟩")
    # post-readout 0
    surv0_0 = Node("Survived (0)\\nin ∣0⟩")
    surv0_1 = Node("Survived (0)\\nin ∣1⟩")
    surv0_e = Node("Lost (0)")
    # rampdown
    rampdown_0 = Node("Survived (ramp-down)\\nin ∣0⟩")
    rampdown_1 = Node("Survived (ramp-down)\\nin ∣1⟩")
    rampdown_e = Node("Lost (ramp-down)")
    # readout 1
    read1_B_0 = Node("Readout 1\\nBright in ∣0⟩")
    read1_D_0 = Node("Readout 1\\nDark in ∣0⟩")
    read1_B_1 = Node("Readout 1\\nBright in ∣1⟩")
    read1_D_1 = Node("Readout 1\\nDark in ∣1⟩")
    read1_B_e = Node("Readout 1\\nBright in ∣∅⟩")
    read1_D_e = Node("Readout 1\\nDark in ∣∅⟩")

    graph = {
        start: {
            start_0: S.p * S.hOP,
            start_1: S.p * (1 - S.hOP),
            start_e: 1 - S.p,
        },
        start_0: {
            read0_B_0: S.F1,
            read0_D_0: 1 - S.F1,
        },
        start_1: {
            read0_B_1: 1 - S.F0,
            read0_D_1: S.F0,
        },
        start_e: {
            read0_B_e: 1 - S.F0,
            read0_D_e: S.F0,
        },
        read0_B_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_e: 1 - S.hB,
        },
        read0_D_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_e: 1 - S.hB,
        },
        read0_B_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_e: 1 - S.hD,
        },
        read0_D_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_e: 1 - S.hD,
        },
        read0_B_e: {
            surv0_e: sy.S(1),
        },
        read0_D_e: {
            surv0_e: sy.S(1),
        },
        surv0_0: {
            rampdown_0: S.hsurv,
            rampdown_e: 1 - S.hsurv,
        },
        surv0_1: {
            rampdown_1: S.hsurv,
            rampdown_e: 1 - S.hsurv,
        },
        surv0_e: {
            rampdown_e: sy.S(1),
        },
        rampdown_0: {
            read1_B_0: S.F1,
            read1_D_0: 1 - S.F1,
        },
        rampdown_1: {
            read1_B_1: 1 - S.F0,
            read1_D_1: S.F0,
        },
        rampdown_e: {
            read1_B_e: 1 - S.F0,
            read1_D_e: S.F0,
        },
        read1_B_0: { },
        read1_D_0: { },
        read1_B_1: { },
        read1_D_1: { },
        read1_B_e: { },
        read1_D_e: { },
    }

class ClockPi(Sequence):
    name = "clock-pi"

    # init
    start = Node("Start")
    start_0 = Node("Load ∣0⟩")
    start_1 = Node("Load ∣1⟩")
    start_e = Node("Empty\\ntweezer")
    # readout 0
    read0_B_0 = Node("Readout 0\\nBright in ∣0⟩")
    read0_D_0 = Node("Readout 0\\nDark in ∣0⟩")
    read0_B_1 = Node("Readout 0\\nBright in ∣1⟩")
    read0_D_1 = Node("Readout 0\\nDark in ∣1⟩")
    read0_B_e = Node("Readout 0\\nBright in ∣∅⟩")
    read0_D_e = Node("Readout 0\\nDark in ∣∅⟩")
    # post-readout 0
    surv0_0 = Node("Survived (0)\\nin ∣0⟩")
    surv0_1 = Node("Survived (0)\\nin ∣1⟩")
    surv0_e = Node("Lost (0)")
    # rampdown
    rampdown_0 = Node("Survived (ramp-down)\\nin ∣0⟩")
    rampdown_1 = Node("Survived (ramp-down)\\nin ∣1⟩")
    rampdown_e = Node("Lost (ramp-down)")
    # clock pulse
    clock_c = Node("π-pulse\\nto ∣c⟩")
    clock_0 = Node("π-pulse\\nto ∣0⟩")
    clock_1 = Node("π-pulse\\nto ∣1⟩")
    # readout 1
    read1_B_c = Node("Readout 1\\nBright in ∣c⟩")
    read1_D_c = Node("Readout 1\\nDark in ∣c⟩")
    read1_B_0 = Node("Readout 1\\nBright in ∣0⟩")
    read1_D_0 = Node("Readout 1\\nDark in ∣0⟩")
    read1_B_1 = Node("Readout 1\\nBright in ∣1⟩")
    read1_D_1 = Node("Readout 1\\nDark in ∣1⟩")
    read1_B_e = Node("Readout 1\\nBright in ∣∅⟩")
    read1_D_e = Node("Readout 1\\nDark in ∣∅⟩")

    graph = {
        start: {
            start_0: S.p * S.hOP,
            start_1: S.p * (1 - S.hOP),
            start_e: 1 - S.p,
        },
        start_0: {
            read0_B_0: S.F1,
            read0_D_0: 1 - S.F1,
        },
        start_1: {
            read0_B_1: 1 - S.F0,
            read0_D_1: S.F0,
        },
        start_e: {
            read0_B_e: 1 - S.F0,
            read0_D_e: S.F0,
        },
        read0_B_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_e: 1 - S.hB,
        },
        read0_D_0: {
            surv0_0: S.hB * (1 - S.PBD),
            surv0_1: S.hB * S.PBD,
            surv0_e: 1 - S.hB,
        },
        read0_B_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_e: 1 - S.hD,
        },
        read0_D_1: {
            surv0_0: S.hD * S.PDB,
            surv0_1: S.hD * (1 - S.PDB),
            surv0_e: 1 - S.hD,
        },
        read0_B_e: {
            surv0_e: sy.S(1),
        },
        read0_D_e: {
            surv0_e: sy.S(1),
        },
        surv0_0: {
            rampdown_0: S.hsurv,
            rampdown_e: 1 - S.hsurv,
        },
        surv0_1: {
            rampdown_1: S.hsurv,
            rampdown_e: 1 - S.hsurv,
        },
        surv0_e: {
            rampdown_e: sy.S(1),
        },
        rampdown_0: {
            clock_c: S.hcpi,
            clock_0: 1 - S.hcpi,
        },
        rampdown_1: {
            clock_1: sy.S(1),
        },
        clock_c: {
            read1_B_c: 1 - S.F0,
            read1_D_c: S.F0,
        },
        clock_0: {
            read1_B_0: S.F1,
            read1_D_0: 1 - S.F1,
        },
        clock_1: {
            read1_B_1: 1 - S.F0,
            read1_D_1: S.F0,
        },
        rampdown_e: {
            read1_B_e: 1 - S.F0,
            read1_D_e: S.F0,
        },
        read1_B_c: { },
        read1_D_c: { },
        read1_B_0: { },
        read1_D_0: { },
        read1_B_1: { },
        read1_D_1: { },
        read1_B_e: { },
        read1_D_e: { },
    }

_paths_oneshot = dfs_all_paths(OneShot.graph, [OneShot.start])
# N-R solver expression for fill fraction p, implicitly set equal to 0
_lhs_p = sum(
    prob for (path, prob) in _paths_oneshot
    if (
        OneShot.read0_B_0 in path
        or OneShot.read0_B_1 in path
        or OneShot.read0_B_e in path
    )
) - S.mp

_paths_twoshot = dfs_all_paths(TwoShot.graph, [TwoShot.start])
# N-R solver expression for bright survival hB, implicitly set equal to 0
_lhs_hB = (
    sum(
        prob for (path, prob) in _paths_twoshot
        if (
            TwoShot.read0_B_0 in path
            or TwoShot.read0_B_1 in path
            or TwoShot.read0_B_e in path
        ) and (
            TwoShot.read1_B_0 in path
            or TwoShot.read1_B_1 in path
            or TwoShot.read1_B_l in path
        )
    ) / sum(
        prob for (path, prob) in _paths_twoshot
        if (
            TwoShot.read0_B_0 in path
            or TwoShot.read0_B_1 in path
            or TwoShot.read0_B_e in path
        )
    )
) - S.mhB
# N-R solver expression for OP efficiency hOP, implicitly set equal to 0
_lhs_hOP = (
    sum(
        prob for (path, prob) in _paths_twoshot
        if (
            TwoShot.read0_B_0 in path
            or TwoShot.read0_B_1 in path
            or TwoShot.read0_B_e in path
        ) and (
            TwoShot.read1_B_0 in path
            or TwoShot.read1_B_1 in path
            or TwoShot.read1_B_l in path
        )
    ) / sum(
        prob for (path, prob) in _paths_twoshot
        if (
            TwoShot.read1_B_0 in path
            or TwoShot.read1_B_1 in path
            or TwoShot.read1_B_l in path
        )
    )
) - S.mhOP

_paths_threeshot = dfs_all_paths(ThreeShot.graph, [ThreeShot.start])
# N-R solver expression for P(B|D) depolarization probability PDB, implicitly set equal to 0
_lhs_PDB = (
    sum(
        prob for (path, prob) in _paths_threeshot
        if (
            ThreeShot.read0_D_0 in path
            or ThreeShot.read0_D_1 in path
            or ThreeShot.read0_D_e in path
        ) and (
            ThreeShot.read1_B_0 in path
            or ThreeShot.read1_B_1 in path
            or ThreeShot.read1_B_l in path
        ) and (
            ThreeShot.read2_B_0 in path
            or ThreeShot.read1_B_1 in path
            or ThreeShot.read1_B_l in path
        )
    ) / sum(
        prob for (path, prob) in _paths_threeshot
        if (
            ThreeShot.read0_D_0 in path
            or ThreeShot.read0_D_1 in path
            or ThreeShot.read0_D_e in path
        ) and (
            ThreeShot.read2_B_0 in path
            or ThreeShot.read2_B_1 in path
            or ThreeShot.read2_B_l in path
        )
    )
) - S.mPDB
# N-R solver expression for P(D|B) depolarization probability PBD, implicitly set equal to 0
_lhs_PBD = (
    sum(
        prob for (path, prob) in _paths_threeshot
        if (
            ThreeShot.read0_B_0 in path
            or ThreeShot.read0_B_1 in path
            or ThreeShot.read0_B_e in path
        ) and (
            ThreeShot.read1_D_0 in path
            or ThreeShot.read1_D_1 in path
            or ThreeShot.read1_D_l in path
        ) and (
            ThreeShot.read2_B_0 in path
            or ThreeShot.read2_B_1 in path
            or ThreeShot.read2_B_l in path
        )
    ) / sum(
        prob for (path, prob) in _paths_threeshot
        if (
            ThreeShot.read0_B_0 in path
            or ThreeShot.read0_B_1 in path
            or ThreeShot.read0_B_e in path
        ) and (
            ThreeShot.read2_B_0 in path
            or ThreeShot.read1_B_1 in path
            or ThreeShot.read1_B_l in path
        )
    )
) - S.mPBD

_paths_fourshot = dfs_all_paths(FourShot.graph, [FourShot.start])
# N-R solver expression for dark survival hD, implicitly set equal to 0
_lhs_hD = (
    sum(
        prob for (path, prob) in _paths_fourshot
        if (
            FourShot.read0_B_0 in path
            or FourShot.read0_B_1 in path
            or FourShot.read0_D_e in path
        ) and (
            FourShot.read1_D_0 in path
            or FourShot.read1_D_1 in path
            or FourShot.read1_D_l in path
        ) and (
            FourShot.read2_D_0 in path
            or FourShot.read2_D_1 in path
            or FourShot.read2_D_l in path
        ) and (
            FourShot.read3_B_0 in path
            or FourShot.read3_B_1 in path
            or FourShot.read3_B_l in path
        )
    ) / sum(
        prob for (path, prob) in _paths_fourshot
        if (
            FourShot.read0_B_0 in path
            or FourShot.read0_B_1 in path
            or FourShot.read0_B_e in path
        ) and (
            FourShot.read1_D_0 in path
            or FourShot.read1_D_1 in path
            or FourShot.read1_D_l in path
        ) and (
            FourShot.read2_D_0 in path
            or FourShot.read2_D_1 in path
            or FourShot.read2_D_l in path
        )
    )
) - S.mhD

_paths_pipulse = dfs_all_paths(PiPulse.graph, [PiPulse.start])
# N-R solver expression for pi-pulse probability hpi, implicitly set equal to 0
_lhs_hpi = (
    sum(
        prob for (path, prob) in _paths_pipulse
        if (
            PiPulse.read0_B_0 in path
            or PiPulse.read0_B_1 in path
            or PiPulse.read0_B_e in path
        ) and (
            PiPulse.read1_D_0 in path
            or PiPulse.read1_D_1 in path
            or PiPulse.read1_D_l in path
        ) and (
            PiPulse.read2_B_0 in path
            or PiPulse.read2_B_1 in path
            or PiPulse.read2_B_l in path
        )
    ) + sum(
        prob for (path, prob) in _paths_pipulse
        if (
            PiPulse.read0_D_0 in path
            or PiPulse.read0_D_1 in path
            or PiPulse.read0_D_e in path
        ) and (
            PiPulse.read1_B_0 in path
            or PiPulse.read1_B_1 in path
            or PiPulse.read1_B_l in path
        ) and (
            PiPulse.read2_B_0 in path
            or PiPulse.read2_B_1 in path
            or PiPulse.read2_B_l in path
        )
    )
) / sum(
    prob for (path, prob) in _paths_pipulse
    if (
        PiPulse.read2_B_0 in path
        or PiPulse.read2_B_1 in path
        or PiPulse.read2_B_l in path
    )
) - S.mhpi

_paths_rampdown = dfs_all_paths(Rampdown.graph, [Rampdown.start])
# N-R solver expression for ramp-down survival probability hsurv, implicitly set
# equal to 0
_lhs_hsurv = (
    sum(
        prob for (path, prob) in _paths_rampdown
        if (
            Rampdown.read0_B_0 in path
            or Rampdown.read0_B_1 in path
            or Rampdown.read0_B_e in path
        ) and (
            Rampdown.read1_B_0 in path
            or Rampdown.read1_B_1 in path
            or Rampdown.read1_B_e in path
        )
    ) / sum(
        prob for (path, prob) in _paths_rampdown
        if (
            Rampdown.read0_B_0 in path
            or Rampdown.read0_B_1 in path
            or Rampdown.read0_B_e in path
        )
    )
) - S.mhsurv

_paths_clockpi = dfs_all_paths(ClockPi.graph, [ClockPi.start])
# N-R solver expression for clock pi-pulse probability hpi, implictly set equal
# to 0
_lhs_hcpi = (
    sum(
        prob for (path, prob) in _paths_clockpi
        if (
            ClockPi.read0_B_0 in path
            or ClockPi.read0_B_1 in path
            or ClockPi.read0_B_e in path
        ) and (
            ClockPi.read1_B_c in path
            or ClockPi.read1_B_0 in path
            or ClockPi.read1_B_1 in path
            or ClockPi.read1_B_e in path
        )
    ) / sum(
        prob for (path, prob) in _paths_clockpi
        if (
            ClockPi.read0_B_0 in path
            or ClockPi.read0_B_1 in path
            or ClockPi.read0_B_e in path
        )
    )
) - S.mhcpi


# collect all N-R solver expressions for easy looping
# _lhs_exprs = [_lhs_p, _lhs_hB, _lhs_hD, _lhs_hOP, _lhs_PDB, _lhs_PBD]
# _lhs_exprs = [_lhs_p, _lhs_hB, _lhs_hOP, _lhs_PDB, _lhs_PBD, _lhs_hpi]
_lhs_exprs = {
    "p": _lhs_p,
    "hB": _lhs_hB,
    "hOP": _lhs_hOP,
    "PDB": _lhs_PDB,
    "PBD": _lhs_PBD,
    "hsurv": _lhs_hsurv,
    "hcpi": _lhs_hcpi,
}

# all variables to solve for
# _vars = [S.p, S.hB, S.hOP, S.PDB, S.PBD, S.hpi]
_vars = {
    "p": S.p,
    "hB": S.hB,
    "hOP": S.hOP,
    "PDB": S.PDB,
    "PBD": S.PBD,
    "hsurv": S.hsurv,
    "hcpi": S.hcpi,
}

# All possible derivates in the system for the Jacobian
# _diffs = [[expr.diff(var) for var in _vars] for expr in _lhs_exprs]
_diffs = {
    (expr_name, var_name): expr.diff(var)
    for ((expr_name, expr), (var_name, var))
    in product(_lhs_exprs.items(), _vars.items())
}

if __name__ == "__main__":
    # if run as a script, verify all sequences and render their graphs.
    fmt = "png"

    # print("render/verify graphs:", file=sys.stderr)

    # print("one-shot", file=sys.stderr)
    # OneShot().verify_graph()
    # OneShot().render(fmt=fmt)

    # print("two-shot", file=sys.stderr)
    # TwoShot().verify_graph()
    # TwoShot().render(fmt=fmt)

    # print("three-shot", file=sys.stderr)
    # ThreeShot().verify_graph()
    # ThreeShot().render(fmt=fmt)

    # print("four-shot", file=sys.stderr)
    # FourShot().verify_graph()
    # FourShot().render(fmt=fmt)

    # print("pi-pulse", file=sys.stderr)
    # PiPulse().verify_graph()
    # PiPulse().render(fmt=fmt)

    # print("ramp-down", file=sys.stderr)
    # Rampdown().verify_graph()
    # Rampdown().render(fmt=fmt)

    # print("clock-pi", file=sys.stderr)
    # ClockPi().verify_graph()
    # ClockPi().render(fmt=fmt)

    # optionally print all expressions if you want to translate them to a
    # different program
    print("print expressions:")
    print("LHS:")
    for (lhs_name, lhs) in _lhs_exprs.items():
        print(lhs_name, ":", lhs)
    print("Diffs:")
    for (diff_name, diff) in _diffs.items():
        print(diff_name, ":", diff)

