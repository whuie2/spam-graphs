"""
Defines the class `S` to hold sympy representations of all relevant variables
and a function to translate a given symbolic expression to a LaTeX string.
"""

import sympy as sy

class S:
    """
    Holds sympy representations of relevant variables:
        - F0: dark classification fidelity
        - F1: bright classification fidelity
        - p: loading fraction
        - hB: bright state survival (defined to include state flip events)
        - hD: dark state survival (defined to include state flip events)
        - PBD: P(D|B) = P(B -> D) depolarization probability
        - PDB: P(B|D) = P(D -> B) depolarization probability
        - hOP: optical pumping efficiency
        - hpi: pi-pulse probability
    Each name `X` listed above also includes `X_err` for the uncertainty in the
    quantity. Except for `F0` and `F1`, each one also includes `mX` and `mX_err`
    for corresponding experimentially measured values.
    """
    (F0, F0_err) = sy.symbols("F0 F0_err")
    (F1, F1_err) = sy.symbols("F1 F1_err")
    (p, p_err, mp, mp_err) = sy.symbols("p p_err mp mp_err")
    (hB, hB_err, mhB, mhB_err) = sy.symbols("hB hB_err mhB mhB_err")
    (hD, hD_err, mhD, mhD_err) = sy.symbols("hD hD_err mhD mhD_err")
    (PBD, PBD_err, mPBD, mPBD_err) = sy.symbols("PBD PBD_err mPBD mPBD_err")
    (PDB, PDB_err, mPDB, mPDB_err) = sy.symbols("PDB PDB_err mPDB mPDB_err")
    (hOP, hOP_err, mhOP, mhOP_err) = sy.symbols("hOP hOP_err mhOP mhOP_err")
    (hpi, hpi_err, mhpi, mhpi_err) = sy.symbols("hpi hpi_err mhpi mhpi_err")
    (hsurv, hsurv_err, mhsurv, mhsurv_err) = sy.symbols("hsurv hsurv_err mhsurv mhsurv_err")
    (hcpi, hcpi_err, mhcpi, mhcpi_err) = sy.symbols("hcpi hcpi_err mhcpi mhcpi_err")

def latexify(expr, simplify: bool=False) -> str:
    """
    Translate a given sympy expression to corresponding LaTeX.
    """
    latex_replace = {
        S.mp: r'{{}^*\mkern-2.5mu{p}',
        S.mhB: r'{{}^*\mkern-2.5mu{\eta_\text{B}}}',
        S.mhD: r'{{}^*\mkern-2.5mu{\eta_\text{D}}}',
        S.mPBD: r'{{}^*\mkern-2.5mu{P_\text{B$\rightarrow$D}}}',
        S.mPDB: r'{{}^*\mkern-2.5mu{P_\text{D$\rightarrow$B}}}',
        S.mhOP: r'{{}^*\mkern-2.5mu{\eta_\text{OP}}}',
        S.mhpi: r'{{}^*\mkern-2.5mu{\eta_\pi}}',
        S.mhsurv: r'{{}^*\mkern-2.5mu{\eta_\text{surv}}}',
        S.mhcpi: r'{{}^*\mkern-2.5mu{\eta_{\text{c}\pi}}}',
        S.F0: r'\mathcal{F}_0',
        S.F1: r'\mathcal{F}_1',
        S.hB: r'\eta_\text{B}',
        S.hD: r'\eta_\text{D}',
        S.PBD: r'P_\text{B$\rightarrow$D}',
        S.PDB: r'P_\text{D$\rightarrow$B}',
        S.hOP: r'\eta_\text{OP}',
        S.hpi: r'\eta_\pi',
        S.hsurv: r'\eta_\text{surv}',
        S.hcpi: r'\eta_{\text{c}\pi}',
    }
    if simplify:
        return sy.latex(expr.simplify(), symbol_names=latex_replace)
    else:
        return sy.latex(expr, symbol_names=latex_replace)

