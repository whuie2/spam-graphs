"""
Provides the `Node` class, which defines a simple type to hold a node that can
be used to construct a sequence graph, as well as the DFS path-finding algorithm
to traverse such a graph.
"""

import sympy as sy

class Node:
    """
    Simple data class to hold a node with a string label. Each node is
    identified and hashed by its label, so two nodes stored in the same `dict`
    can have identical labels.
    """
    label: str

    def __init__(self, label: str):
        self.label = label

    def __hash__(self):
        return hash(self.label)

    def __str__(self):
        return self.label

    def __repr__(self):
        return f"Node({self.label})"

def dfs_all_paths(
    graph: dict[Node, dict[Node, sy.Symbol]],
    start_nodes: list[Node],
) -> list[(list[Node], sy.Product)]:
    """
    Use depth-first search to find all possible paths through `graph` starting
    from a list of initial nodes, with associated probabilities. Assumes `graph`
    is acyclic.
    """

    def do_dfs(
        graph: dict[Node, dict[Node, sy.Symbol]],
        start: Node,
        term: sy.Symbol | sy.Product,
    ) -> list[(list[Node], sy.Product)]:
        if len(graph[start]) == 0:
            return [([start], term)]
        else:
            paths = list()
            for (neighbor, prob) in graph[start].items():
                paths += [
                    ([start] + subpath, term * subprob)
                    for (subpath, subprob) in do_dfs(graph, neighbor, prob)
                ]
            return paths

    paths = list()
    for start_node in start_nodes:
        paths += do_dfs(graph, start_node, sy.S(1))
    return paths

