import numpy as np
import lmfit
from pathlib import Path
import whooie.pyplotdefs as pd
from whooie.analysis import ExpVal
import matplotlib.colors as mplcolors
site_colors = mplcolors.LinearSegmentedColormap.from_list(
    "vibrant",
    [
        (0.000, "#012d5e"),
        (0.125, "#0039a7"),
        (0.250, "#1647cf"),
        (0.375, "#6646ff"),
        (0.500, "#bc27ff"),
        (0.600, "#dc47af"),
        (0.800, "#f57548"),
        (0.900, "#f19e00"),
        (0.950, "#fbb800"),
        (1.000, "#fec800"),
    ],
)

def model(
	params: lmfit.Parameters,
	x: np.ndarray,
) -> np.ndarray:
	A = params["A"].value
	f = params["f"].value
	ph = params["ph"].value
	y = params["y"].value
	B = params["B"].value
	b = params["b"].value
	a = params["a"].value
	return (
		A * np.exp(-2 * np.pi * y * x)
			* np.cos(2 * np.pi * f * x + np.pi / 180 * ph)
		+ b * x
		+ B * np.exp(-2 * np.pi * a * x)
	)

def residuals(
	params: lmfit.Parameters,
	x: np.ndarray,
	y: np.ndarray,
	err: np.ndarray,
) -> np.ndarray:
	m = model(params, x)
	return ((m - y) / err)**2

outdir = Path("output")
infile = outdir.joinpath("clockpi_20231108_020.npz")

data = np.load(str(infile))
tau = data["tau"]
hcpi = data["hcpi"]
hcpi_err = data["hcpi_err"]

# noff_data = np.load("input/clock-oscillations/20231024_clock-rabi-noff_031/survival_data.npz")
# nt = noff_data["sliced"].shape[0]
# noff_tau = noff_data["tau_clock"]
# noff_surv = noff_data["sliced"].mean(axis=0)
# noff_surv_err = np.sqrt((noff_data["sliced_err"]**2).sum(axis=0)) / nt

# noff_data = np.load(str(outdir.joinpath("clockpi_20231030_015_noff.npz")))
# noff_tau = noff_data["tau"]
# noff_surv = 1 - noff_data["hcpi"]
# noff_surv_err = data["hcpi_err"]

# pd.Plotter().errorbar(tau, hcpi, hcpi_err).show().close()

params = lmfit.Parameters()
params.add("A", value=0.3, min=0.0, max=0.5)
params.add("f", value=100.0, min=0.0) # kHz
params.add("ph", value=0.0, vary=True) # deg
params.add("y", value=1.0, min=0.0) # kHz
params.add("B", value=0.5, min=0.0, max=1.0)
params.add("b", value=0.0, vary=False) # /ms
params.add("a", value=0.0, vary=False) # kHz

fit = lmfit.minimize(residuals, params, args=(tau, hcpi, hcpi_err))
if not fit.success:
	raise Exception("fit failed")

xplot = np.linspace(tau.min(), tau.max(), 1000)
yplot = model(fit.params, xplot)

ampl = ExpVal(fit.params["A"].value, fit.params["A"].stderr)
rabi_freq = ExpVal(fit.params["f"].value, fit.params["f"].stderr)
decay = ExpVal(fit.params["y"].value, fit.params["y"].stderr)

vals = f"""
amplitude = {ampl.value_str()}
rabi freq = {rabi_freq.value_str()} kHz
decay rate = {decay.value_str()} kHz
"""[1:-1]
print(vals)

# noff_params = lmfit.Parameters()
# noff_params.add("A", value=0.3, min=0.0, max=0.5)
# noff_params.add("f", value=100.0, min=0.0)
# noff_params.add("ph", value=0.0, vary=True)
# noff_params.add("y", value=1.0, min=0.0)
# noff_params.add("B", value=0.5, min=0.0, max=1.0)
# noff_params.add("b", value=0.0, vary=False)
# noff_params.add("a", value=0.0, vary=False)

# noff_fit = lmfit.minimize(residuals, noff_params, args=(noff_tau, noff_surv, noff_surv_err))
# if not noff_fit.success:
#     raise Exception("noFF fit failed")

# noff_xplot = np.linspace(noff_tau.min(), noff_tau.max(), 1000)
# noff_yplot = model(noff_fit.params, noff_xplot)

(
    pd.Plotter()
    .plot(
        xplot * 1e3, yplot,
        marker="", linestyle="-", color="k",
        clip_on=False,
    )
    .errorbar(
        tau * 1e3, hcpi, hcpi_err,
        marker="o", linestyle="", color="C6",
        clip_on=False,
    )
    .ggrid()
    .set_ylim(0.00, +1.00)
    .set_xlabel("Pulse time [μs]")
    .set_ylabel("Π-pulse probability")
    .savefig(outdir.joinpath("clockpi_hcpi.png"))
    .close()
)
(
    pd.Plotter()
    # .plot(
    #     noff_xplot * 1e3, noff_yplot,
    #     marker="", linestyle="-", color="C8",
    #     clip_on=False,
    # )
    # .errorbar(
    #     noff_tau * 1e3, noff_surv, noff_surv_err,
    #     marker="o", linestyle="", color="C8",
    #     label="FF off",
    #     clip_on=False,
    # )
    .plot(
        xplot * 1e3, 1 - yplot,
        marker="", linestyle="-", color="C5",
        clip_on=False,
    )
    .errorbar(
        tau * 1e3, 1 - hcpi, hcpi_err,
        marker="o", linestyle="", color="C5",
        label="FF on",
        clip_on=False,
    )
    .ggrid()
    # .legend(fontsize="xx-small")
    .set_ylim(-0.00, +1.00)
    .set_xlabel("Pulse time [μs]")
    .set_ylabel("Population in ∣0⟩")
    .savefig(outdir.joinpath("clockpi_surv.png"))
    .close()
)

