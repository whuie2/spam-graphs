#![allow(non_snake_case)]

#![allow(unused_imports, unused_variables, unused_assignments)]

use std::{
    path::PathBuf,
};
use anyhow::Result;
use itertools::Itertools;
use ndarray as nd;
use whooie::{
    mkdir,
    read_npz,
    write_npz,
    config::TomlConfigUnver,
};
use lib::{
    value_str,
    solver::nr_corr,
    systems::clockpi::{ Vars, Consts, lhs, jacobian },
};

fn main() -> Result<()> {
    // pull data from output files written in other scripts
    let bootstrap_path
        = PathBuf::from("input/bootstrap_results.toml");
    let bootstrap_data = TomlConfigUnver::from_file(bootstrap_path)?;
    let depolarization_path
        = PathBuf::from("input/depolarization_results.toml");
    let depolarization_data = TomlConfigUnver::from_file(depolarization_path)?;
    let dark_survival_path
        = PathBuf::from("input/dark_state_survival.toml");
    let dark_surv_data = TomlConfigUnver::from_file(dark_survival_path)?;

    let F_data: Vec<f64> = bootstrap_data.get_into_ok("flattened 5-array -3/2.fidelity")?;
    let F0_data: Vec<f64>
        = bootstrap_data.get_into_ok("flattened 5-array -3/2.F0")?;
    let F1_data: Vec<f64>
        = bootstrap_data.get_into_ok("flattened 5-array -3/2.F1")?;
    let p_raw: Vec<f64>
        = bootstrap_data.get_into_ok("flattened 5-array -3/2.fill")?;
    let hB_raw: Vec<f64>
        = bootstrap_data.get_into_ok("flattened 5-array -3/2.survival_raw")?;
    let hOP_raw: Vec<f64>
        = bootstrap_data.get_into_ok("flattened 5-array -3/2.op_raw")?;
    let PDB_raw: Vec<f64>
        = depolarization_data.get_into_ok("flattened 5-array -3/2.P(D -> B)")?;
    let PBD_raw: Vec<f64>
        = depolarization_data.get_into_ok("flattened 5-array -3/2.P(B -> D)")?;
    let hsurv_raw: Vec<f64>
        = vec![0.8125712344369482, 0.012011151960361717];
    let hD_raw: Vec<f64>
        = dark_surv_data.get_into_ok("flattened 5-array -3/2.survD_raw")?;

    let outdir = PathBuf::from("output");
    if !outdir.is_dir() { mkdir!(outdir); }
    let outfile = outdir.join("clockpi.npz");

    let clock_data_path
        = PathBuf::from("input/clock-oscillations/20231108_clock-rabi_020")
        .join("survival_data.npz");
    let (tau, surv, surv_err):
        (nd::Array1<f64>, nd::Array2<f64>, nd::Array2<f64>)
        = read_npz!(
            clock_data_path,
            arrays: {
                "tau_clock.npy",
                "sliced.npy",
                "sliced_err.npy",
            }
        );
    let nt: usize = surv.shape()[0];
    let surv: nd::Array1<f64> = surv.mean_axis(nd::Axis(0)).unwrap();
    let surv_err: nd::Array1<f64>
        = surv_err.mapv(|e| (e / nt as f64).powi(2))
        .sum_axis(nd::Axis(0))
        .mapv(f64::sqrt);

    // let mut p_lim: Vec<f64>;
    // let mut hB_lim: Vec<f64>;
    // let mut hOP_lim: Vec<f64>;
    // let mut PDB_lim: Vec<f64>;
    // let mut PBD_lim: Vec<f64>;
    // let mut hsurv_lim: Vec<f64>;
    let mut hcpi_lim: Vec<f64>;

    // calculate uncertainties in a result `v`, given a series of uncertainty
    // bound solves `p_lim` -- if `v < 0`, filter `v_lim` down to only positive
    // values and replace `v` as the mean of those; if `v > 1`, filter `v_lim`
    // down to only values less than 1 and replace `v` as the mean of those. In
    // any case, calculate uncertainty as the RMS devation from `v`.
    let process_lim = |v: &mut f64, v_lim: Vec<f64>| -> f64 {
        if *v < 0.0 {
            let lim_positive: Vec<f64>
                = v_lim.into_iter().filter(|vk| *vk > 0.0).collect();
            let N: f64 = lim_positive.len() as f64;
            *v = lim_positive.iter().copied().sum::<f64>() / N;
            (
                lim_positive.into_iter()
                .map(|vk| (vk - *v).powi(2))
                .sum::<f64>() / N
            ).sqrt()
        } else if *v > 1.0 {
            let lim_lt_one: Vec<f64>
                = v_lim.into_iter().filter(|vk| *vk < 1.0).collect();
            let N: f64 = lim_lt_one.len() as f64;
            *v = lim_lt_one.iter().copied().sum::<f64>() / N;
            (
                lim_lt_one.into_iter()
                .map(|vk| (vk - *v).powi(2))
                .sum::<f64>() / N
            ).sqrt()
        } else {
            let N: f64 = v_lim.len() as f64;
            (
                v_lim.into_iter()
                .map(|vk| (vk - *v).powi(2))
                .sum::<f64>() / N
            ).sqrt()
        }
    };

    let mut hcpi_corr: Vec<f64> = Vec::new();
    let mut hcpi_corr_err: Vec<f64> = Vec::new();
    for (&s, &e) in surv.iter().zip(&surv_err) {
        let raw
            = Vars {
                p: p_raw[0],
                hB: hB_raw[0],
                hOP: hOP_raw[0],
                PDB: PDB_raw[0],
                PBD: PBD_raw[0],
                hsurv: hsurv_raw[0],
                hcpi: s,
            };
        let consts
            = Consts {
                F0: F0_data[0],
                F1: F1_data[0],
                hD: hD_raw[0],
                mp: p_raw[0],
                mhB: hB_raw[0],
                mhOP: hOP_raw[0],
                mPDB: PDB_raw[0],
                mPBD: PBD_raw[0],
                mhsurv: hsurv_raw[0],
                mhcpi: s,
            };
        let mut nom_corr
            = nr_corr(
                raw,
                consts,
                lhs,
                jacobian,
                Some(10),
                Some(1e-6),
                false,
            )?;

        // p_lim = Vec::new();
        // hB_lim = Vec::new();
        // hOP_lim = Vec::new();
        // PDB_lim = Vec::new();
        // PBD_lim = Vec::new();
        // hsurv_lim = Vec::new();
        hcpi_lim = Vec::new();
        let prod_iter
            = [
                &F0_data,
                &F1_data,
                &p_raw,
                &hB_raw,
                &hOP_raw,
                &PDB_raw,
                &PBD_raw,
                &hsurv_raw,
                &vec![s, e],
                &hD_raw,
            ].into_iter()
            .map(|data| [data[0] - data[1], data[0] + data[1]])
            .multi_cartesian_product()
            .enumerate();
        for (k, vals) in prod_iter {
            let bF0 = vals[0];
            let bF1 = vals[1];
            let bp = vals[2];
            let bhB = vals[3];
            let bhOP = vals[4];
            let bPDB = vals[5];
            let bPBD = vals[6];
            let bhsurv = vals[7];
            let bhcpi = vals[8];
            let bhD = vals[9];

            let lim_consts
                = Consts {
                    F0: bF0,
                    F1: bF1,
                    hD: bhD,
                    mp: bp,
                    mhB: bhB,
                    mhOP: bhOP,
                    mPDB: bPDB,
                    mPBD: bPBD,
                    mhsurv: bhsurv,
                    mhcpi: bhcpi,
                };
            let lim_corr
                = nr_corr(
                    nom_corr,
                    lim_consts,
                    lhs,
                    jacobian,
                    Some(10),
                    Some(1e-6),
                    false,
                )?;
            hcpi_lim.push(lim_corr.hcpi);
        }
        let hcpi_err: f64 = process_lim(&mut nom_corr.hcpi, hcpi_lim);
        hcpi_corr.push(nom_corr.hcpi);
        hcpi_corr_err.push(hcpi_err);
    }

    let hcpi_corr = nd::Array1::from_vec(hcpi_corr);
    let hcpi_corr_err = nd::Array1::from_vec(hcpi_corr_err);
    write_npz!(
        outfile,
        arrays: {
            "tau" => &tau,
            "hcpi" => &hcpi_corr,
            "hcpi_err" => &hcpi_corr_err,
        }
    );

    Ok(())
}

