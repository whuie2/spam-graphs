#![allow(dead_code, non_snake_case, non_upper_case_globals)]
#![allow(clippy::needless_return)]

use std::{
    collections::HashSet,
    path::PathBuf,
};
use anyhow::Result;
use itertools::Itertools;
use whooie::config::TomlConfigUnver;
#[allow(unused_imports)]
use lib::{
    value_str,
    solver_old::{
        Consts,
        Vars,
        VarsSingle,
        all_corr,
        single_corr,
    },
};

#[allow(unused_variables, unused_assignments)]
fn main() -> Result<()> {
    // pull data from output files written in other scripts
    let bootstrap_path
        = PathBuf::from("../raw/bootstrap_results.toml");
    let bootstrap_data = TomlConfigUnver::from_file(bootstrap_path)?;
    let depolarization_path
        = PathBuf::from("../raw/depolarization_results.toml");
    let depolarization_data = TomlConfigUnver::from_file(depolarization_path)?;
    let dark_survival_path
        = PathBuf::from("../raw/dark_state_survival.toml");
    let dark_surv_data = TomlConfigUnver::from_file(dark_survival_path)?;

    // take the intersection of all their keys
    let bootstrap_keys: HashSet<String>
        = bootstrap_data.as_ref().keys().cloned().collect();
    let depolarization_keys: HashSet<String>
        = depolarization_data.as_ref().keys().cloned().collect();
    let dark_surv_keys: HashSet<String>
        = dark_surv_data.as_ref().keys().cloned().collect();
    let mut keys: Vec<String>
        = bootstrap_keys.intersection(&depolarization_keys)
        .cloned()
        .collect::<HashSet<String>>()
        .intersection(&dark_surv_keys)
        .cloned()
        .collect();
    keys.sort();

    let mut F_data: Vec<f64>;
    let mut F0_data: Vec<f64>;
    let mut F1_data: Vec<f64>;
    let mut p_raw: Vec<f64>;
    let mut hB_raw: Vec<f64>;
    let mut hOP_raw: Vec<f64>;
    let mut PDB_raw: Vec<f64>;
    let mut PBD_raw: Vec<f64>;
    let mut hpi_raw: Vec<f64>;
    let mut hD_raw: Vec<f64>;

    let mut p_lim: Vec<f64>;
    let mut hB_lim: Vec<f64>;
    let mut hOP_lim: Vec<f64>;
    let mut PDB_lim: Vec<f64>;
    let mut PBD_lim: Vec<f64>;
    let mut hpi_lim: Vec<f64>;

    // calculate uncertainties in a result `v`, given a series of uncertainty
    // bound solves `p_lim` -- if `v < 0`, filter `v_lim` down to only positive
    // values and replace `v` as the mean of those. In either case, calculate
    // uncertainty as the RMS devation from `v`.
    let process_lim = |v: &mut f64, v_lim: Vec<f64>| -> f64 {
        if *v < 0.0 {
            let lim_positive: Vec<f64>
                = v_lim.into_iter().filter(|vk| *vk > 0.0).collect();
            let N: f64 = lim_positive.len() as f64;
            *v = lim_positive.iter().copied().sum::<f64>() / N;
            (
                lim_positive.into_iter()
                .map(|vk| (vk - *v).powi(2))
                .sum::<f64>() / N
            ).sqrt()
        } else {
            let N: f64 = v_lim.len() as f64;
            (
                v_lim.into_iter()
                .map(|vk| (vk - *v).powi(2))
                .sum::<f64>() / N
            ).sqrt()
        }
    };
    // essentially a HashMap with write methods
    let mut results = TomlConfigUnver::new();
    for key in keys.into_iter() {
        eprintln!("{}", key);

        F_data = bootstrap_data.get_into_ok(&(key.clone() + ".fidelity"))?;
        F0_data = bootstrap_data.get_into_ok(&(key.clone() + ".F0"))?;
        F1_data = bootstrap_data.get_into_ok(&(key.clone() + ".F1"))?;
        p_raw = bootstrap_data.get_into_ok(&(key.clone() + ".fill"))?;
        hB_raw = bootstrap_data.get_into_ok(&(key.clone() + ".survival_raw"))?;
        hOP_raw = bootstrap_data.get_into_ok(&(key.clone() + ".op_raw"))?;
        PDB_raw = depolarization_data.get_into_ok(&(key.clone() + ".P(D -> B)"))?;
        PBD_raw = depolarization_data.get_into_ok(&(key.clone() + ".P(B -> D)"))?;
        hD_raw = dark_surv_data.get_into_ok(&(key.clone() + ".survD_raw"))?;
        // hpi is only used for array-averaged numbers
        hpi_raw = vec![0.9693396226415094, 0.008372281115795138];

        // if the key corresponds to an array-averaged set of data, solve for
        // all variables, otherwise leave out hpi. Processing is otherwise
        // identical
        if key.contains("flattened") {
            eprintln!("find nominal corrected values");
            let Vars { mut p, mut hB, mut hOP, mut PDB, mut PBD, mut hpi }
                = all_corr(
                    Vars {
                        p: p_raw[0],
                        hB: hB_raw[0],
                        hOP: hOP_raw[0],
                        PDB: PDB_raw[0],
                        PBD: PBD_raw[0],
                        hpi: hpi_raw[0],
                    },
                    Consts {
                        F0: F0_data[0],
                        F1: F1_data[0],
                        hD: hD_raw[0],
                        mp: p_raw[0],
                        mhB: hB_raw[0],
                        mhOP: hOP_raw[0],
                        mPDB: PDB_raw[0],
                        mPBD: PBD_raw[0],
                        mhpi: hpi_raw[0],
                    },
                    Some(10),
                    Some(1e-6),
                    false,
                )?;

            eprintln!("find correction bounds");
            p_lim = Vec::new();
            hB_lim = Vec::new();
            hOP_lim = Vec::new();
            PDB_lim = Vec::new();
            PBD_lim = Vec::new();
            hpi_lim = Vec::new();
            let prod_iter
                = [
                    &F0_data,
                    &F1_data,
                    &p_raw,
                    &hB_raw,
                    &hD_raw,
                    &hOP_raw,
                    &PDB_raw,
                    &PBD_raw,
                    &hpi_raw,
                ].into_iter()
                .map(|data| [data[0] - data[1], data[0] + data[1]].into_iter())
                .multi_cartesian_product()
                .enumerate();
            for (k, vals) in prod_iter {
                eprint!("  {}          \r", k);
                let bF0 = vals[0];
                let bF1 = vals[1];
                let bp = vals[2];
                let bhB = vals[3];
                let bhD = vals[4];
                let bhOP = vals[5];
                let bPDB = vals[6];
                let bPBD = vals[7];
                let bhpi = vals[8];
                let Vars {
                    p: bps,
                    hB: bhBs,
                    hOP: bhOPs,
                    PDB: bPDBs,
                    PBD: bPBDs,
                    hpi: bhpis,
                }
                    = all_corr(
                        Vars { p, hB, hOP, PDB, PBD, hpi },
                        Consts {
                            F0: bF0,
                            F1: bF1,
                            hD: bhD,
                            mp: bp,
                            mhB: bhB,
                            mhOP: bhOP,
                            mPDB: bPDB,
                            mPBD: bPBD,
                            mhpi: bhpi,
                        },
                        Some(10),
                        Some(1e-6),
                        false,
                    )?;
                p_lim.push(bps);
                hB_lim.push(bhBs);
                hOP_lim.push(bhOPs);
                PDB_lim.push(bPDBs);
                PBD_lim.push(bPBDs);
                hpi_lim.push(bhpis);
            }
            eprintln!();
            // calculate uncertainties
            let p_err: f64 = process_lim(&mut p, p_lim);
            let hB_err: f64 = process_lim(&mut hB, hB_lim);
            let hOP_err: f64 = process_lim(&mut hOP, hOP_lim);
            let PDB_err: f64 = process_lim(&mut PDB, PDB_lim);
            let PBD_err: f64 = process_lim(&mut PBD, PBD_lim);
            let hpi_err: f64 = process_lim(&mut hpi, hpi_lim);

            println!("{}", key);
            println!("fidelity                         = {}", value_str!(F_data[0], F_data[1])  );
            println!("F0                               = {}", value_str!(F0_data[0], F0_data[1]));
            println!("F1                               = {}", value_str!(F1_data[0], F1_data[1]));
            println!("p (raw)                          = {}", value_str!(p_raw[0], p_raw[1])    );
            println!("p (corrected)                    = {}", value_str!(p, p_err)              );
            println!("P(D -> B) (raw)                  = {}", value_str!(PDB_raw[0], PDB_raw[1]));
            println!("P(D -> B) (corrected)            = {}", value_str!(PDB, PDB_err)          );
            println!("P(B -> D) (raw)                  = {}", value_str!(PBD_raw[0], PBD_raw[1]));
            println!("P(B -> D) (corrected)            = {}", value_str!(PBD, PBD_err)          );
            println!("dark survival (raw)              = {}", value_str!(hD_raw[0], hD_raw[1])  );
            println!("OP efficiency (raw)              = {}", value_str!(hOP_raw[0], hOP_raw[1]));
            println!("OP efficiency (corrected)        = {}", value_str!(hOP, hOP_err)          );
            println!("bright survival (raw)            = {}", value_str!(hB_raw[0], hB_raw[1])  );
            println!("bright survival (corrected)      = {}", value_str!(hB, hB_err)            );
            println!("pi-pulse probability (raw)       = {}", value_str!(hpi_raw[0], hpi_raw[1]));
            println!("pi-pulse probability (corrected) = {}", value_str!(hpi, hpi_err)          );

            let mut results_key = TomlConfigUnver::new();
            results_key.insert("F",         F_data            );
            results_key.insert("F0",        F0_data           );
            results_key.insert("F1",        F1_data           );
            results_key.insert("p_raw",     p_raw             );
            results_key.insert("p",         vec![p, p_err]    );
            results_key.insert("hB_raw",    hB_raw            );
            results_key.insert("hB",        vec![hB, hB_err]  );
            results_key.insert("hD_raw",    hD_raw            );
            results_key.insert("hOP_raw",   hOP_raw           );
            results_key.insert("hOP",       vec![hOP, hOP_err]);
            results_key.insert("PDB_raw",   PDB_raw           );
            results_key.insert("PDB",       vec![PDB, PDB_err]);
            results_key.insert("PBD_raw",   PBD_raw           );
            results_key.insert("PBD",       vec![PBD, PBD_err]);
            results_key.insert("hpi_raw",   hpi_raw           );
            results_key.insert("hpi",       vec![hpi, hpi_err]);
            results.insert(&key, results_key.into_raw());
        } else {
            eprintln!("find nominal corrected values");
            let VarsSingle { mut p, mut hB, mut hOP, mut PDB, mut PBD }
                = single_corr(
                    VarsSingle {
                        p: p_raw[0],
                        hB: hB_raw[0],
                        hOP: hOP_raw[0],
                        PDB: PDB_raw[0],
                        PBD: PBD_raw[0],
                    },
                    Consts {
                        F0: F0_data[0],
                        F1: F1_data[0],
                        hD: hD_raw[0],
                        mp: p_raw[0],
                        mhB: hB_raw[0],
                        mhOP: hOP_raw[0],
                        mPDB: PDB_raw[0],
                        mPBD: PBD_raw[0],
                        mhpi: hpi_raw[0],
                    },
                    Some(10),
                    Some(1e-6),
                    false,
                )?;

            eprintln!("find correction bounds");
            p_lim = Vec::new();
            hB_lim = Vec::new();
            hOP_lim = Vec::new();
            PDB_lim = Vec::new();
            PBD_lim = Vec::new();
            let prod_iter
                = [
                    &F0_data,
                    &F1_data,
                    &p_raw,
                    &hB_raw,
                    &hD_raw,
                    &hOP_raw,
                    &PDB_raw,
                    &PBD_raw,
                ].into_iter()
                .map(|data| [data[0] - data[1], data[0] + data[1]].into_iter())
                .multi_cartesian_product()
                .enumerate();
            for (k, vals) in prod_iter {
                eprint!("  {}          \r", k);
                let bF0 = vals[0];
                let bF1 = vals[1];
                let bp = vals[2];
                let bhB = vals[3];
                let bhD = vals[4];
                let bhOP = vals[5];
                let bPDB = vals[6];
                let bPBD = vals[7];
                let VarsSingle {
                    p: bps,
                    hB: bhBs,
                    hOP: bhOPs,
                    PDB: bPDBs,
                    PBD: bPBDs,
                }
                    = single_corr(
                        VarsSingle { p, hB, hOP, PDB, PBD },
                        Consts {
                            F0: bF0,
                            F1: bF1,
                            hD: bhD,
                            mp: bp,
                            mhB: bhB,
                            mhOP: bhOP,
                            mPDB: bPDB,
                            mPBD: bPBD,
                            mhpi: hpi_raw[0],
                        },
                        Some(10),
                        Some(1e-6),
                        false,
                    )?;
                p_lim.push(bps);
                hB_lim.push(bhBs);
                hOP_lim.push(bhOPs);
                PDB_lim.push(bPDBs);
                PBD_lim.push(bPBDs);
            }
            eprintln!();
            let p_err: f64 = process_lim(&mut p, p_lim);
            let hB_err: f64 = process_lim(&mut hB, hB_lim);
            let hOP_err: f64 = process_lim(&mut hOP, hOP_lim);
            let PDB_err: f64 = process_lim(&mut PDB, PDB_lim);
            let PBD_err: f64 = process_lim(&mut PBD, PBD_lim);

            println!("{}", key);
            println!("fidelity                         = {}", value_str!(F_data[0], F_data[1])  );
            println!("F0                               = {}", value_str!(F0_data[0], F0_data[1]));
            println!("F1                               = {}", value_str!(F1_data[0], F1_data[1]));
            println!("p (raw)                          = {}", value_str!(p_raw[0], p_raw[1])    );
            println!("p (corrected)                    = {}", value_str!(p, p_err)              );
            println!("P(D -> B) (raw)                  = {}", value_str!(PDB_raw[0], PDB_raw[1]));
            println!("P(D -> B) (corrected)            = {}", value_str!(PDB, PDB_err)          );
            println!("P(B -> D) (raw)                  = {}", value_str!(PBD_raw[0], PBD_raw[1]));
            println!("P(B -> D) (corrected)            = {}", value_str!(PBD, PBD_err)          );
            println!("dark survival (raw)              = {}", value_str!(hD_raw[0], hD_raw[1])  );
            println!("OP efficiency (raw)              = {}", value_str!(hOP_raw[0], hOP_raw[1]));
            println!("OP efficiency (corrected)        = {}", value_str!(hOP, hOP_err)          );
            println!("bright survival (raw)            = {}", value_str!(hB_raw[0], hB_raw[1])  );
            println!("bright survival (corrected)      = {}", value_str!(hB, hB_err)            );

            let mut results_key = TomlConfigUnver::new();
            results_key.insert("F",         F_data            );
            results_key.insert("F0",        F0_data           );
            results_key.insert("F1",        F1_data           );
            results_key.insert("p_raw",     p_raw             );
            results_key.insert("p",         vec![p, p_err]    );
            results_key.insert("hB_raw",    hB_raw            );
            results_key.insert("hB",        vec![hB, hB_err]  );
            results_key.insert("hD_raw",    hD_raw            );
            results_key.insert("hOP_raw",   hOP_raw           );
            results_key.insert("hOP",       vec![hOP, hOP_err]);
            results_key.insert("PDB_raw",   PDB_raw           );
            results_key.insert("PDB",       vec![PDB, PDB_err]);
            results_key.insert("PBD_raw",   PBD_raw           );
            results_key.insert("PBD",       vec![PBD, PBD_err]);
            results.insert(&key, results_key.into_raw());
        }
        println!()
    }

    results.write_toml(PathBuf::from("output/results.toml"), false)?;

    return Ok(());
}

