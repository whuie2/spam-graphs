//! Implements a numerical SPAM correction solver for the relevant variables
//! using multidimensional Newton-Raphson (N-R).
//!
//! All `lhs_*` functions define LHS expressions for the N-R solver and are
//! implicitly set equal to zero.
//!
//! All `d*__d*` functions define derivatives of the LHS expressions for use in
//! calculating the Jacobian.
//!
//! `*_single` versions of functions are provided to solve for single-site
//! numbers, for which we don't have π-pulse data.

use ndarray as nd;
use ndarray_linalg::{
    Determinant,
    Inverse,
    Norm,
};
use thiserror::Error;
use whooie::print_flush;

#[derive(Debug, Error)]
pub enum SolverError {
    #[error("linalg error: '{0}'")]
    Linalg(#[from] ndarray_linalg::error::LinalgError),

    #[error("encountered singular Jacobian")]
    SingularJacobian,

    #[error("failed to converge")]
    NoConverge,
}
pub type SolverResult<T> = Result<T, SolverError>;

/// Default N-R iteration limit.
pub const MAXITERS: usize = 1000;

/// Default N-R stepsize termination threshold.
pub const EPSILON: f64 = 1e-6;

pub trait Variables
where Self: Into<nd::Array1<f64>> + From<nd::Array1<f64>> + Copy + Clone
{ }

pub trait Constants
where Self: Copy + Clone
{ }

pub fn nr_corr<V, C, F, J>(
    vars_init: V,
    consts: C,
    lhs: F,
    jacobian: J,
    maxiters: Option<usize>,
    epsilon: Option<f64>,
    printflag: bool,
) -> SolverResult<V>
where
    V: Variables,
    C: Constants,
    F: Fn(V, C) -> nd::Array1<f64>,
    J: Fn(V, C) -> nd::Array2<f64>,
{
    let eps: f64 = epsilon.unwrap_or(EPSILON);
    let mut x: nd::Array1<f64> = vars_init.into();
    let mut dx: nd::Array1<f64>;
    let mut y: nd::Array1<f64>;
    let mut J: nd::Array2<f64>;
    for k in 0..maxiters.unwrap_or(MAXITERS) {
        if printflag {
            print_flush!("  {} {}          \r", x, k);
        }
        y = lhs(x.clone().into(), consts);
        J = jacobian(x.clone().into(), consts);
        if J.det()?.abs() < 1e-15 {
            return Err(SolverError::SingularJacobian);
        }
        dx = -J.inv()?.dot(&y);
        x += &dx;
        // x.iter_mut()
        //     .for_each(|xk| *xk = xk.max(0.0).min(1.0));
        if dx.norm() < eps {
            if printflag { println!(); }
            return Ok(x.into());
        }
    }
    if printflag { println!(); }
    return Err(SolverError::NoConverge);
}

