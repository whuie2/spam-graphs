#![allow(dead_code, non_snake_case, non_upper_case_globals)]
#![allow(clippy::needless_return)]

pub mod solver_old;
pub mod solver;
pub mod systems;

use std::cmp;

/// String formatting for a real number with associated uncertainty.
///
/// - `dec` is the maximum number of decimal places to display;
/// - `trunc` controls the format of the uncertainty (`X.XX(Y)` if `trunc` else
///   `X.XX +/- 0.0Y`)
/// - `latex` adds LaTeX `$`'s and replaces `+/-` with `\pm`.
#[allow(clippy::collapsible_else_if)]
pub fn value_str(
    x: f64,
    err: f64,
    dec: usize,
    trunc: bool,
    latex: bool,
) -> String
{
    let _err: f64 = if err.is_finite() { err } else { -1.0 };
    let z: i32 = if _err > 0.0 { -_err.log10().floor() as i32 } else { 5 };
    let z: i32 = cmp::max(cmp::min(dec as i32, z), 0);
    let outstr: String
        = if trunc {
            if _err > 0.0 {
                format!("{:.w$}({:.0})", x, 10.0_f64.powi(z) * _err, w = z as usize)
            } else {
                format!("{:.w$}(~)", x, w = z as usize)
            }
        } else {
            if _err > 0.0 {
                format!("{:.w$} +/- {:.w$}", x, _err, w = z as usize)
            } else {
                format!("{:.w$} +/- [~]", x, w = z as usize)
            }
        };
    return if latex {
        "$".to_string() + &outstr.replace("+/-", "\\pm") + "$"
    } else {
        outstr
    };
}

/// Simple macro for more concise calls to [`value_str()`].
///
/// - `dec` defaults to `5`;
/// - `trunc` defaults to `true`;
/// - `latex` defaults to `false`.
#[macro_export]
macro_rules! value_str {
    {
        $x:expr,
        $err:expr,
        dec: $dec:expr,
        trunc: $trunc:expr,
        latex: $latex:expr $(,)?
    } => {
        value_str($x, $err, $dec, $trunc, $latex)
    };
    {
        $x:expr,
        $err:expr,
        dec: $dec:expr,
        trunc: $trunc:expr $(,)?
    } => {
        value_str($x, $err, $dec, $trunc, false)
    };
    {
        $x:expr,
        $err:expr,
        dec: $dec:expr,
        latex: $latex:expr $(,)?
    } => {
        value_str($x, $err, $dec, true, $latex)
    };
    {
        $x:expr,
        $err:expr,
        trunc: $trunc:expr,
        latex: $latex:expr $(,)?
    } => {
        value_str($x, $err, 5, $trunc, $latex)
    };
    {
        $x:expr,
        $err:expr,
        dec: $dec:expr $(,)?
    } => {
        value_str($x, $err, $dec, true, false)
    };
    {
        $x:expr,
        $err:expr,
        latex: $latex:expr $(,)?
    } => {
        value_str($x, $err, 5 true, $latex)
    };
    {
        $x:expr,
        $err:expr $(,)?
    } => {
        value_str($x, $err, 5, true, false)
    };
}

