#![allow(unused_parens)]

#![allow(unused_variables)]

use ndarray::{ self as nd, array };
use crate::solver::{ Variables, Constants };

#[derive(Copy, Clone, Debug)]
pub struct Vars {
    pub p: f64,
    pub hB: f64,
    pub hOP: f64,
    pub PDB: f64,
    pub PBD: f64,
    pub hsurv: f64,
    pub hcpi: f64,
}

impl From<Vars> for nd::Array1<f64> {
    fn from(vars: Vars) -> Self {
        array![
            vars.p,
            vars.hB,
            vars.hOP,
            vars.PDB,
            vars.PBD,
            vars.hsurv,
            vars.hcpi,
        ]
    }
}

impl From<nd::Array1<f64>> for Vars {
    fn from(arr: nd::Array1<f64>) -> Self {
        if arr.len() != 7 {
            panic!(
                "Vars::from(ndarray::Array1<f64>): array has incorrect length"
            );
        }
        Self {
            p: arr[0],
            hB: arr[1],
            hOP: arr[2],
            PDB: arr[3],
            PBD: arr[4],
            hsurv: arr[5],
            hcpi: arr[6],
        }
    }
}

impl Variables for Vars { }

#[derive(Copy, Clone, Debug)]
pub struct Consts {
    pub F0: f64,
    pub F1: f64,
    pub hD: f64,
    pub mp: f64,
    pub mhB: f64,
    pub mhOP: f64,
    pub mPDB: f64,
    pub mPBD: f64,
    pub mhsurv: f64,
    pub mhcpi: f64,
}

impl Constants for Consts { }

fn lhs_p(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        F1 * hOP * p
        - mp
        + p * (1.0 - F0) * (1.0 - hOP)
        + (1.0 - F0) * (1.0 - p)
    )
}

fn lhs_hB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        -mhB
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    )
}

fn lhs_hOP(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        -mhOP
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    )
}

fn lhs_PDB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        -mPDB
        + (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
    )
}

fn lhs_PBD(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        -mPBD
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
    )
}

fn lhs_hsurv(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        -mhsurv
        + (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    )
}

fn lhs_hcpi(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        -mhcpi
        + (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    )
}

pub fn lhs(vars: Vars, consts: Consts) -> nd::Array1<f64> {
    array![
        lhs_p(vars, consts),
        lhs_hB(vars, consts),
        lhs_hOP(vars, consts),
        lhs_PDB(vars, consts),
        lhs_PBD(vars, consts),
        lhs_hsurv(vars, consts),
        lhs_hcpi(vars, consts),
    ]
}

fn dlhs_p__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        F0
        + F1 * hOP
        + (1.0 - F0) * (1.0 - hOP)
        - 1.0
    )
}

fn dlhs_p__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_p__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        F1 * p
        - p * (1.0 - F0)
    )
}

fn dlhs_p__dPDB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_p__dPBD(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_p__dhsurv(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_p__dhcpi(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_hB__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * PBD * hB * hOP.powi(2)
            + F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            + F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP) - (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            -F0 * F1 * PBD * hB * hOP * (1.0 - hOP)
            - F0 * F1 * hB * hOP * (1.0 - PBD) * (1.0 - hOP)
            - F0 * F1 * hOP * (1.0 - hB) - F0 * PDB * hD * (1.0 - F0) * (1.0 - hOP).powi(2)
            - F0 * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - F0 * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0)
            - F1.powi(2) * PBD * hB * hOP.powi(2)
            - F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            - F1 * PBD * hB * hOP.powi(2) * (1.0 - F1)
            - F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP.powi(2) * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * hOP * (1.0 - F0) * (1.0 - hB)
            - PDB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            - hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            - hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hB__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * PBD * hOP.powi(2) * p
            + F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hOP * p * (1.0 - F0)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F0 * F1 * PBD * hOP * p * (1.0 - hOP)
            - F0 * F1 * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p
            - F1.powi(2) * PBD * hOP.powi(2) * p
            - F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            - F1 * PBD * hOP.powi(2) * p * (1.0 - F1)
            - F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0)
        ) * (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hB__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            F0 * F1 * PBD * hB * hOP * p
            - F0 * F1 * PBD * hB * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD)
            - F0 * F1 * hB * p * (1.0 - PBD) * (1.0 - hOP)
            - F0 * F1 * p * (1.0 - hB)
            - F0 * PDB * hD * p * (1.0 - F0) * 2.0 * (hOP - 1.0)
            - F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * 2.0 * (hOP - 1.0)
            + F0 * p * (1.0 - F0) * (1.0 - hD)
            - 2.0 * F1.powi(2) * PBD * hB * hOP * p
            - 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0)
            - 2.0 * F1 * PBD * hB * hOP * p * (1.0 - F1)
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0)
            - F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            - 2.0 * F1 * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            - F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            - PDB * hD * p * (1.0 - F0).powi(2) * 2.0 * (hOP - 1.0)
            - PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB)
            - hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * 2.0 * (hOP - 1.0)
            - hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
        + (
            2.0 * F1.powi(2) * PBD * hB * hOP * p
            + 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            - F1 * PBD * hB * hOP * p * (1.0 - F0)
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * p * (1.0 - F0)
            + F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            + F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * 2.0 * (hOP - 1.0)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * 2.0 * (hOP - 1.0)
            - p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    )
}

fn dlhs_hB__dPDB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_hB__dPBD(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_hB__dhsurv(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_hB__dhcpi(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_hOP__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * PBD * hB * hOP.powi(2)
            + F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            + F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            - (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            -F0 * F1 * PDB * hD * hOP * (1.0 - hOP)
            - F0 * F1 * hD * hOP * (1.0 - PDB) * (1.0 - hOP)
            - F0 * PDB * hD * (1.0 - F0) * (1.0 - hOP).powi(2)
            - F0 * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - F0 * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0)
            - F1.powi(2) * PBD * hB * hOP.powi(2)
            - F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            - F1 * PBD * hB * hOP.powi(2) * (1.0 - F1)
            - F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP.powi(2) * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * hOP * (1.0 - F0) * (1.0 - hB)
            - PBD * hB * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            - hB * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            - (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hOP__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * PBD * hOP.powi(2) * p
            + F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hOP * p * (1.0 - F0)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F1.powi(2) * PBD * hOP.powi(2) * p
            - F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            - F1 * PBD * hOP.powi(2) * p * (1.0 - F1)
            - F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0)
            - PBD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hOP * p * (1.0 - F0) * (1.0 - F1)
        ) * (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hOP__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            F0 * F1 * PDB * hD * hOP * p
            - F0 * F1 * PDB * hD * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB)
            - F0 * F1 * hD * p * (1.0 - PDB) * (1.0 - hOP)
            - F0 * PDB * hD * p * (1.0 - F0) * 2.0 * (hOP - 1.0)
            - F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * 2.0 * (hOP - 1.0)
            + F0 * p * (1.0 - F0) * (1.0 - hD)
            - 2.0 * F1.powi(2) * PBD * hB * hOP * p
            - 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0)
            - 2.0 * F1 * PBD * hB * hOP * p * (1.0 - F1)
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0)
            - F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            - 2.0 * F1 * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            - F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1)
            - PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - PDB * hD * p * (1.0 - F0).powi(2) * 2.0 * (hOP - 1.0)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD)
            - hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * 2.0 * (hOP - 1.0)
            + p * (1.0 - F0).powi(2) * (1.0 - hD)
            - p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
        + (
            2.0 * F1.powi(2) * PBD * hB * hOP * p
            + 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            - F1 * PBD * hB * hOP * p * (1.0 - F0)
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * p * (1.0 - F0)
            + F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            + F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * 2.0 * (hOP - 1.0)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * 2.0 * (hOP - 1.0)
            - p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    )
}

fn dlhs_hOP__dPDB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_hOP__dPBD(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_hOP__dhsurv(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_hOP__dhcpi(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_PDB__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F0.powi(2) * PDB * hD.powi(2) * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0.powi(2) * (1.0 - F0)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F0 * (1.0 - F0).powi(2)
            + F0 * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * (1.0 - PDB).powi(2) / 2.0
            - F0.powi(2) * PDB * hD.powi(2) * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD.powi(2) * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - F0.powi(2) * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0)
            - F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            - F0 * F1.powi(2) * PDB * hB * hD * hOP * (1.0 - PBD) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * hOP * (1.0 - F1)
            - F0 * F1 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hB * hD * hOP * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hB * hD * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hB * hD * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hD * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - F0 * PBD * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * PBD * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F0 * PDB * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * PDB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            - F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2)
            - F0 * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PBD * hB.powi(2) * hOP * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F1 * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * hB.powi(2) * hOP * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            - F1 * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            - F1 * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            - PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - PBD * hB * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - hB.powi(2) * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            - hB * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            - (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    )
}

fn dlhs_PDB__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F0 * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            + F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * PDB * hD * hOP * p * (1.0 - PBD) / 2.0
            - F0 * F1 * PBD * PDB * hD * hOP * p * (1.0 - F1)
            - F0 * F1 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) / 2.0
            - F0 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - F0 * PBD * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * PBD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            - F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PBD * hB * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD)
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * hB * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2)
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP)
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            - PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - PBD * hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP)
            - PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - PBD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP)
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            - p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    )
}

fn dlhs_PDB__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            -F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            - PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            -F0.powi(2) * F1 * PDB * hD.powi(2) * p * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            - F0 * F1.powi(2) * PDB * hB * hD * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F1)
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hB * hD * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB.powi(2) * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
        ) * (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    )
}

fn dlhs_PDB__dPDB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            -F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) * 2.0 * (PDB - 1.0) / 2.0
            - F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * 2.0 * (PDB - 1.0) / 2.0
            - F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * 2.0 * (PDB - 1.0) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * 2.0 * (PDB - 1.0) / 2.0
            - F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * p * 2.0 * (PDB - 1.0) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) * 2.0 * (PDB - 1.0) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            - F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) / 2.0
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            - F0 * F1 * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * 2.0 * (PDB - 1.0) / 2.0
            - F0 * F1 * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * 2.0 * (PDB - 1.0) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F0 * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    )
}

fn dlhs_PDB__dPBD(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * 2.0 * (PBD - 1.0) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            -F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) / 2.0
            - F0 * F1 * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * 2.0 * (PBD - 1.0) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            - F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * 2.0 * (PBD - 1.0) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * 2.0 * (PBD - 1.0) / 2.0
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            - PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) * 2.0 * (PBD - 1.0) / 2.0
            - hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
        ) * (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    )
}

fn dlhs_PDB__dhsurv(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_PDB__dhcpi(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_PBD__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F0 * (1.0 - F0).powi(2)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * hOP * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hB * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hB * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * F1 * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2)
            - F1.powi(3) * PBD * hB.powi(2) * hOP * (1.0 - PBD) / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * (1.0 - PBD).powi(2) / 2.0
            - F1.powi(2) * PBD * PDB * hB * hD * hOP * (1.0 - F0)
            - F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1)
            - F1 * PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F1 * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * PBD * hB * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F1 * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * PDB * hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            - F1 * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            - PDB * hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - PDB * hD.powi(2) * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            - hD.powi(2) * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - hD * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3)
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_PBD__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0 * F1 * p * (1.0 - F0) / 2.0
            + F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            + F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP)
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            -F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * PBD * hD * hOP * p * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * p * (1.0 - F0) / 2.0
            - F1.powi(3) * PBD * hB * hOP * p * (1.0 - PBD)
            - F1.powi(3) * hB * hOP * p * (1.0 - PBD).powi(2)
            - F1.powi(2) * PBD * PDB * hD * hOP * p * (1.0 - F0)
            - F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1.powi(2) * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            - F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            - F1 * PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F1 * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * PBD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * p * (1.0 - F0).powi(2) / 2.0
        ) * (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_PBD__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * p * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) / 2.0
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            - PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            - F1.powi(3) * PBD * hB.powi(2) * p * (1.0 - PBD) / 2.0
            - F1.powi(3) * hB.powi(2) * p * (1.0 - PBD).powi(2) / 2.0
            - F1.powi(2) * PBD * PDB * hB * hD * p * (1.0 - F0)
            - F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) / 2.0
            - PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) / 2.0
            - hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_PBD__dPDB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            -F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * 2.0 * (PDB - 1.0) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * 2.0 * (PDB - 1.0) / 2.0
            - F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * 2.0 * (PDB - 1.0) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * 2.0 * (PDB - 1.0) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) / 2.0
            - F1.powi(2) * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) / 2.0
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * 2.0 * (PDB - 1.0) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            - hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * 2.0 * (PDB - 1.0) / 2.0
            - hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - hOP) * 2.0 * (PDB - 1.0) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
        ) * (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_PBD__dPBD(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F0 * F1.powi(2) * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * 2.0 * (PBD - 1.0) / 2.0
            - F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * 2.0 * (PBD - 1.0) / 2.0
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            -F0 * F1.powi(2) * PDB * hB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * p * 2.0 * (PBD - 1.0) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            - F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * 2.0 * (PBD - 1.0) / 2.0
            - F1.powi(2) * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
        ) * (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_PBD__dhsurv(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_PBD__dhcpi(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_hsurv__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * hB * hOP * hsurv * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * (1.0 - F0)
            + F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            - (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            -F0 * F1 * PBD * hB * hOP * hsurv
            - F0 * F1 * PBD * hB * hOP * (1.0 - hsurv)
            - F0 * F1 * hB * hOP * (1.0 - PBD) * (1.0 - hsurv)
            - F0 * F1 * hOP * (1.0 - hB)
            - F0 * PDB * hD * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            - F0 * hD * hsurv * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F0 * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            - F0 * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0)
            - F1.powi(2) * hB * hOP * hsurv * (1.0 - PBD)
            - F1 * PBD * hB * hOP * hsurv * (1.0 - F0)
            - F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hsurv)
            - F1 * PDB * hD * hsurv * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP * hsurv * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            - F1 * hOP * (1.0 - F0) * (1.0 - hB)
            - PDB * hD * hsurv * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            - hD * hsurv * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            - hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            - (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hsurv__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            - F1 * hOP * p * (1.0 - F0)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F0 * F1 * PBD * hOP * hsurv * p
            - F0 * F1 * PBD * hOP * p * (1.0 - hsurv)
            - F0 * F1 * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p
            - F1.powi(2) * hOP * hsurv * p * (1.0 - PBD)
            - F1 * PBD * hOP * hsurv * p * (1.0 - F0)
            - F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            - F1 * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0)
        ) * (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hsurv__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * hB * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hsurv)
            - F1 * PDB * hD * hsurv * p * (1.0 - F0)
            + F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * p * (1.0 - F0) * (1.0 - hB)
            - PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hsurv)
            - hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB)
            - hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hsurv)
            - p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            -F0 * F1 * PBD * hB * hsurv * p
            - F0 * F1 * PBD * hB * p * (1.0 - hsurv)
            - F0 * F1 * hB * p * (1.0 - PBD) * (1.0 - hsurv)
            - F0 * F1 * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD)
            - F1.powi(2) * hB * hsurv * p * (1.0 - PBD)
            - F1 * PBD * hB * hsurv * p * (1.0 - F0)
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0)
            - F1 * hB * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            - F1 * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hsurv__dPDB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1 * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            - hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            - hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
        ) * (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hsurv__dPBD(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            -F1.powi(2) * hB * hOP * hsurv * p
            + F1 * hB * hOP * hsurv * p * (1.0 - F0)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F0 * F1 * hB * hOP * hsurv * p
            + F1.powi(2) * hB * hOP * hsurv * p
            - F1 * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1)
        ) * (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hsurv__dhsurv(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            + F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            - PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F0 * F1 * hB * hOP * p * (1.0 - PBD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            - F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            - F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            - F1 * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            - PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
        ) * (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hsurv__dhcpi(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    0.0
}

fn dlhs_hcpi__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * hB * hOP * hsurv * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * (1.0 - F0)
            + F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            - (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            -F0 * F1 * PBD * hB * hOP * hsurv
            - F0 * F1 * PBD * hB * hOP * (1.0 - hsurv)
            - F0 * F1 * hB * hOP * hcpi * hsurv * (1.0 - PBD)
            - F0 * F1 * hB * hOP * (1.0 - PBD) * (1.0 - hsurv)
            - F0 * F1 * hOP * (1.0 - hB)
            - F0 * PDB * hD * hcpi * hsurv * (1.0 - F0) * (1.0 - hOP)
            - F0 * PDB * hD * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            - F0 * hD * hsurv * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F0 * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            - F0 * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0)
            - F1.powi(2) * hB * hOP * hsurv * (1.0 - PBD) * (1.0 - hcpi)
            - F1 * PBD * hB * hOP * hsurv * (1.0 - F0)
            - F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hsurv)
            - F1 * PDB * hD * hsurv * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            - F1 * hB * hOP * hcpi * hsurv * (1.0 - F0) * (1.0 - PBD)
            - F1 * hB * hOP * hsurv * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            - F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            - F1 * hOP * (1.0 - F0) * (1.0 - hB)
            - PDB * hD * hcpi * hsurv * (1.0 - F0).powi(2) * (1.0 - hOP)
            - PDB * hD * hsurv * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            - PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            - hD * hsurv * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            - hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            - (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hcpi__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            - F1 * hOP * p * (1.0 - F0)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F0 * F1 * PBD * hOP * hsurv * p
            - F0 * F1 * PBD * hOP * p * (1.0 - hsurv)
            - F0 * F1 * hOP * hcpi * hsurv * p * (1.0 - PBD)
            - F0 * F1 * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p
            - F1.powi(2) * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            - F1 * PBD * hOP * hsurv * p * (1.0 - F0)
            - F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            - F1 * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            - F1 * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            - F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0)
        ) * (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hcpi__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * hB * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hsurv)
            - F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hcpi)
            + F1 * hB * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * p * (1.0 - F0) * (1.0 - hB)
            - PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2)
            - PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hsurv)
            - hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB)
            - hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hsurv)
            - p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            -F0 * F1 * PBD * hB * hsurv * p
            - F0 * F1 * PBD * hB * p * (1.0 - hsurv)
            - F0 * F1 * hB * hcpi * hsurv * p * (1.0 - PBD)
            - F0 * F1 * hB * p * (1.0 - PBD) * (1.0 - hsurv)
            - F0 * F1 * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD)
            - F1.powi(2) * hB * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            - F1 * PBD * hB * hsurv * p * (1.0 - F0)
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hcpi)
            - F1 * hB * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            - F1 * hB * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            - F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            - F1 * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hcpi__dPDB(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1 * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            - hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F0 * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            - hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            - hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
        ) * (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hcpi__dPBD(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            -F1.powi(2) * hB * hOP * hsurv * p * (1.0 - hcpi)
            - F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0)
            + F1 * hB * hOP * hsurv * p * (1.0 - F0)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F0 * F1 * hB * hOP * hcpi * hsurv * p
            - F0 * F1 * hB * hOP * hsurv * p
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0)
            - F1 * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - hcpi)
        ) * (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hcpi__dhsurv(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            F1.powi(2) * hB * hOP * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * p * (1.0 - F0) * (1.0 - PBD)
            - F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            + PDB * hD * hcpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            - PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F0 * F1 * hB * hOP * hcpi * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD)
            - F0 * PDB * hD * hcpi * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            - F1.powi(2) * hB * hOP * p * (1.0 - PBD) * (1.0 - hcpi)
            - F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            - F1 * hB * hOP * hcpi * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            - F1 * hB * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            - PDB * hD * hcpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            - PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
        ) * (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

fn dlhs_hcpi__dhcpi(vars: Vars, consts: Consts) -> f64 {
    let Vars { p, hB, hOP, PDB, PBD, hsurv, hcpi } = vars;
    let Consts { F0, F1, hD, mp, mhB, mhOP, mPDB, mPBD, mhsurv, mhcpi } = consts;
    (
        (
            -F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            - F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + PDB * hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F0 * F1 * hB * hOP * hsurv * p * (1.0 - PBD)
            - F0 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD)
            - PDB * hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
        ) * (
            F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * hsurv * p
            + F0 * F1 * PBD * hB * hOP * p * (1.0 - hsurv)
            + F0 * F1 * hB * hOP * hcpi * hsurv * p * (1.0 - PBD)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hsurv)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * hcpi * hsurv * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * hD * hsurv * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * hB * hOP * hsurv * p * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * PBD * hB * hOP * hsurv * p * (1.0 - F0)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hsurv)
            + F1 * PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hcpi)
            + F1 * hB * hOP * hcpi * hsurv * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * hOP * hsurv * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hcpi)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hsurv)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hcpi * hsurv * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + PDB * hD * hsurv * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hcpi)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hsurv)
            + hD * hsurv * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hsurv)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    )
}

pub fn jacobian(vars: Vars, consts: Consts) -> nd::Array2<f64> {
    array![
        [
            dlhs_p__dp(vars, consts),
            dlhs_p__dhB(vars, consts),
            dlhs_p__dhOP(vars, consts),
            dlhs_p__dPDB(vars, consts),
            dlhs_p__dPBD(vars, consts),
            dlhs_p__dhsurv(vars, consts),
            dlhs_p__dhcpi(vars, consts),
        ],
        [
            dlhs_hB__dp(vars, consts),
            dlhs_hB__dhB(vars, consts),
            dlhs_hB__dhOP(vars, consts),
            dlhs_hB__dPDB(vars, consts),
            dlhs_hB__dPBD(vars, consts),
            dlhs_hB__dhsurv(vars, consts),
            dlhs_hB__dhcpi(vars, consts),
        ],
        [
            dlhs_hOP__dp(vars, consts),
            dlhs_hOP__dhB(vars, consts),
            dlhs_hOP__dhOP(vars, consts),
            dlhs_hOP__dPDB(vars, consts),
            dlhs_hOP__dPBD(vars, consts),
            dlhs_hOP__dhsurv(vars, consts),
            dlhs_hOP__dhcpi(vars, consts),
        ],
        [
            dlhs_PDB__dp(vars, consts),
            dlhs_PDB__dhB(vars, consts),
            dlhs_PDB__dhOP(vars, consts),
            dlhs_PDB__dPDB(vars, consts),
            dlhs_PDB__dPBD(vars, consts),
            dlhs_PDB__dhsurv(vars, consts),
            dlhs_PDB__dhcpi(vars, consts),
        ],
        [
            dlhs_PBD__dp(vars, consts),
            dlhs_PBD__dhB(vars, consts),
            dlhs_PBD__dhOP(vars, consts),
            dlhs_PBD__dPDB(vars, consts),
            dlhs_PBD__dPBD(vars, consts),
            dlhs_PBD__dhsurv(vars, consts),
            dlhs_PBD__dhcpi(vars, consts),
        ],
        [
            dlhs_hsurv__dp(vars, consts),
            dlhs_hsurv__dhB(vars, consts),
            dlhs_hsurv__dhOP(vars, consts),
            dlhs_hsurv__dPDB(vars, consts),
            dlhs_hsurv__dPBD(vars, consts),
            dlhs_hsurv__dhsurv(vars, consts),
            dlhs_hsurv__dhcpi(vars, consts),
        ],
        [
            dlhs_hcpi__dp(vars, consts),
            dlhs_hcpi__dhB(vars, consts),
            dlhs_hcpi__dhOP(vars, consts),
            dlhs_hcpi__dPDB(vars, consts),
            dlhs_hcpi__dPBD(vars, consts),
            dlhs_hcpi__dhsurv(vars, consts),
            dlhs_hcpi__dhcpi(vars, consts),
        ],
    ]
}

