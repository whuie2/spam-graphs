//! Implements a numerical SPAM correction solver for the relevant variables
//! using multidimensional Newton-Raphson (N-R).
//!
//! All `lhs_*` functions define LHS expressions for the N-R solver and are
//! implicitly set equal to zero.
//!
//! All `d*__d*` functions define derivatives of the LHS expressions for use in
//! calculating the Jacobian.
//!
//! `*_single` versions of functions are provided to solve for single-site
//! numbers, for which we don't have π-pulse data.

use ndarray::{
    self as nd,
    array,
};
use ndarray_linalg::{
    Determinant,
    Inverse,
    Norm,
};
use thiserror::Error;
use whooie::print_flush;

#[derive(Debug, Error)]
pub enum SolverError {
    #[error("linalg error: '{0}'")]
    Linalg(#[from] ndarray_linalg::error::LinalgError),

    #[error("encountered singular Jacobian")]
    SingularJacobian,

    #[error("failed to converge")]
    NoConverge,
}
pub type SolverResult<T> = Result<T, SolverError>;

/// Default N-R iteration limit.
pub const MAXITERS: usize = 1000;

/// Default N-R stepsize termination threshold.
pub const EPSILON: f64 = 1e-6;

/// Holds values for all the variables being solved for.
#[derive(Copy, Clone, Debug)]
pub struct Vars {
    pub p: f64,
    pub hB: f64,
    // pub hD: f64,
    pub hOP: f64,
    pub PDB: f64,
    pub PBD: f64,
    pub hpi: f64,
}

impl From<Vars> for nd::Array1<f64> {
    fn from(vars: Vars) -> Self {
        return array![
            vars.p,
            vars.hB,
            vars.hOP,
            vars.PDB,
            vars.PBD,
            vars.hpi,
        ];
    }
}

/// *Panics* if `arr` has the wrong length.
impl From<nd::Array1<f64>> for Vars {
    fn from(arr: nd::Array1<f64>) -> Self {
        if arr.len() != 6 {
            panic!(
                "Vars::from(ndarray::Array1<f64>): array has incorrect \
                length"
            );
        }
        return Self {
            p: arr[0],
            hB: arr[1],
            hOP: arr[2],
            PDB: arr[3],
            PBD: arr[4],
            hpi: arr[5],
        };
    }
}

/// Holds values for all quantities held constant.
#[derive(Copy, Clone, Debug)]
pub struct Consts {
    pub F0: f64,
    pub F1: f64,
    pub hD: f64,
    pub mp: f64,
    pub mhB: f64,
    // pub mhD: f64,
    pub mhOP: f64,
    pub mPDB: f64,
    pub mPBD: f64,
    pub mhpi: f64,
}

pub fn lhs_p(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB: _,
        hOP,
        PDB: _,
        PBD: _,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD: _,
        mp,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        -mp
        + F1 * hOP * p
        + p * (1.0 - F0) * (1.0 - hOP)
        + (1.0 - F0) * (1.0 - p)
    ;
}

pub fn lhs_hB(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        -mhB
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB) + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    ;
}

pub fn lhs_hOP(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        -mhOP
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    ;
}

pub fn lhs_PDB(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        -mPDB
        + (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0 
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
    ;
}

pub fn lhs_PBD(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD,
        mhpi: _,
    } = consts;
    return
        -mPBD
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
    ;
}

pub fn lhs_hpi(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi,
    } = consts;
    return
        -mhpi
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
    ;
}

pub fn dlhs_p__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p: _,
        hB: _,
        hOP,
        PDB: _,
        PBD: _,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD: _,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        F0
        + F1 * hOP
        + (1.0 - F0) * (1.0 - hOP)
        - 1.0
    ;
}
pub fn dlhs_p__dhB(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_p__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB: _,
        hOP: _,
        PDB: _,
        PBD: _,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD: _,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        F1 * p
        - p * (1.0 - F0)
    ;
}

pub fn dlhs_p__dPDB(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_p__dPBD(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_p__dhpi(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_hB__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hB * hOP.powi(2)
            + F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            + F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP) - (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            -F0 * F1 * PBD * hB * hOP * (1.0 - hOP)
            - F0 * F1 * hB * hOP * (1.0 - PBD) * (1.0 - hOP)
            - F0 * F1 * hOP * (1.0 - hB)
            - F0 * PDB * hD * (1.0 - F0) * (1.0 - hOP).powi(2)
            - F0 * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - F0 * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0)
            - F1.powi(2) * PBD * hB * hOP.powi(2)
            - F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            - F1 * PBD * hB * hOP.powi(2) * (1.0 - F1)
            - F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP.powi(2) * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * hOP * (1.0 - F0) * (1.0 - hB)
            - PDB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            - hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            - hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hB__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hOP.powi(2) * p
            + F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hOP * p * (1.0 - F0)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F0 * F1 * PBD * hOP * p * (1.0 - hOP)
            - F0 * F1 * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p
            - F1.powi(2) * PBD * hOP.powi(2) * p
            - F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            - F1 * PBD * hOP.powi(2) * p * (1.0 - F1)
            - F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0)
        ) * (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hB__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            F0 * F1 * PBD * hB * hOP * p
            - F0 * F1 * PBD * hB * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD)
            - F0 * F1 * hB * p * (1.0 - PBD) * (1.0 - hOP)
            - F0 * F1 * p * (1.0 - hB)
            - F0 * PDB * hD * p * (1.0 - F0) * (2.0 * hOP - 2.0)
            - F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            + F0 * p * (1.0 - F0) * (1.0 - hD)
            - 2.0 * F1.powi(2) * PBD * hB * hOP * p
            - 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0)
            - 2.0 * F1 * PBD * hB * hOP * p * (1.0 - F1)
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0)
            - F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            - 2.0 * F1 * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            - F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            - PDB * hD * p * (1.0 - F0).powi(2) * (2.0 * hOP - 2.0)
            - PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB)
            - hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            - hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
        + (
            2.0 * F1.powi(2) * PBD * hB * hOP * p
            + 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            - F1 * PBD * hB * hOP * p * (1.0 - F0)
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * p * (1.0 - F0)
            + F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            + F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (2.0 * hOP - 2.0)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            - p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    ;
}

pub fn dlhs_hB__dPDB(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_hB__dPBD(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_hB__dhpi(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_hOP__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hB * hOP.powi(2)
            + F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            + F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            - (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            -F0 * F1 * PDB * hD * hOP * (1.0 - hOP)
            - F0 * F1 * hD * hOP * (1.0 - PDB) * (1.0 - hOP)
            - F0 * PDB * hD * (1.0 - F0) * (1.0 - hOP).powi(2)
            - F0 * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - F0 * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0)
            - F1.powi(2) * PBD * hB * hOP.powi(2)
            - F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            - F1 * PBD * hB * hOP.powi(2) * (1.0 - F1)
            - F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP.powi(2) * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * hOP * (1.0 - F0) * (1.0 - hB)
            - PBD * hB * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            - hB * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            - (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hOP__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hOP.powi(2) * p
            + F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hOP * p * (1.0 - F0)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F1.powi(2) * PBD * hOP.powi(2) * p
            - F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            - F1 * PBD * hOP.powi(2) * p * (1.0 - F1)
            - F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0)
            - PBD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hOP * p * (1.0 - F0) * (1.0 - F1)
        ) * (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hOP__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            F0 * F1 * PDB * hD * hOP * p
            - F0 * F1 * PDB * hD * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB)
            - F0 * F1 * hD * p * (1.0 - PDB) * (1.0 - hOP)
            - F0 * PDB * hD * p * (1.0 - F0) * (2.0 * hOP - 2.0)
            - F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            + F0 * p * (1.0 - F0) * (1.0 - hD)
            - 2.0 * F1.powi(2) * PBD * hB * hOP * p
            - 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0)
            - 2.0 * F1 * PBD * hB * hOP * p * (1.0 - F1)
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0)
            - F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            - 2.0 * F1 * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            - F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1)
            - PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - PDB * hD * p * (1.0 - F0).powi(2) * (2.0 * hOP - 2.0)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD)
            - hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            + p * (1.0 - F0).powi(2) * (1.0 - hD)
            - p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
        + (
            2.0 * F1.powi(2) * PBD * hB * hOP * p
            + 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            - F1 * PBD * hB * hOP * p * (1.0 - F0)
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * p * (1.0 - F0)
            + F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            + F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (2.0 * hOP - 2.0)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            - p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    ;
}

pub fn dlhs_hOP__dPDB(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_hOP__dPBD(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_hOP__dhpi(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_PDB__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0.powi(2) * PDB * hD.powi(2) * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0.powi(2) * (1.0 - F0)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0 - F0 * (1.0 - F0).powi(2)
            + F0 * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0 + F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * (1.0 - PDB).powi(2) / 2.0
            - F0.powi(2) * PDB * hD.powi(2) * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD.powi(2) * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - F0.powi(2) * (1.0 - F0) * (1.0 - hD) / 2.0 + F0.powi(2) * (1.0 - F0)
            - F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            - F0 * F1.powi(2) * PDB * hB * hD * hOP * (1.0 - PBD) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * hOP * (1.0 - F1)
            - F0 * F1 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hB * hD * hOP * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hB * hD * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hB * hD * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hD * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - F0 * PBD * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * PBD * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F0 * PDB * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * PDB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            - F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0 + F0 * (1.0 - F0).powi(2)
            - F0 * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PBD * hB.powi(2) * hOP * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F1 * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * hB.powi(2) * hOP * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            - F1 * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            - F1 * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            - PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - PBD * hB * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - hB.powi(2) * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            - hB * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            - (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    ;
}

pub fn dlhs_PDB__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F0 * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            + F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * PDB * hD * hOP * p * (1.0 - PBD) / 2.0
            - F0 * F1 * PBD * PDB * hD * hOP * p * (1.0 - F1)
            - F0 * F1 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) / 2.0
            - F0 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - F0 * PBD * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * PBD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            - F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PBD * hB * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD)
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * hB * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2)
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP)
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            - PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - PBD * hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP)
            - PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - PBD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP)
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            - p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    ;
}

pub fn dlhs_PDB__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            -F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            - PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            -F0.powi(2) * F1 * PDB * hD.powi(2) * p * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            - F0 * F1.powi(2) * PDB * hB * hD * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F1)
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hB * hD * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB.powi(2) * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
        ) * (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    ;
}

pub fn dlhs_PDB__dPDB(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            -F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            - F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (2.0 * PDB - 2.0) / 2.0
            - F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (2.0 * PDB - 2.0) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            - F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * p * (2.0 * PDB - 2.0) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            - F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) / 2.0
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            - F0 * F1 * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (2.0 * PDB - 2.0) / 2.0
            - F0 * F1 * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F0 * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    ;
}

pub fn dlhs_PDB__dPBD(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (2.0 * PBD - 2.0) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            -F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) / 2.0
            - F0 * F1 * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (2.0 * PBD - 2.0) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            - F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (2.0 * PBD - 2.0) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (2.0 * PBD - 2.0) / 2.0
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            - PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) * (2.0 * PBD - 2.0) / 2.0
            - hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
        ) * (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    ;
}

pub fn dlhs_PDB__dhpi(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_PBD__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F0 * (1.0 - F0).powi(2)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * hOP * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hB * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hB * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * F1 * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2)
            - F1.powi(3) * PBD * hB.powi(2) * hOP * (1.0 - PBD) / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * (1.0 - PBD).powi(2) / 2.0
            - F1.powi(2) * PBD * PDB * hB * hD * hOP * (1.0 - F0)
            - F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1)
            - F1 * PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F1 * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * PBD * hB * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F1 * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * PDB * hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            - F1 * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            - PDB * hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - PDB * hD.powi(2) * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            - hD.powi(2) * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - hD * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3)
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_PBD__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0 * F1 * p * (1.0 - F0) / 2.0
            + F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            + F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP)
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            -F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * PBD * hD * hOP * p * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * p * (1.0 - F0) / 2.0
            - F1.powi(3) * PBD * hB * hOP * p * (1.0 - PBD)
            - F1.powi(3) * hB * hOP * p * (1.0 - PBD).powi(2)
            - F1.powi(2) * PBD * PDB * hD * hOP * p * (1.0 - F0)
            - F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1.powi(2) * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            - F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            - F1 * PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F1 * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * PBD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * p * (1.0 - F0).powi(2) / 2.0
        ) * (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_PBD__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * p * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) / 2.0
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            - PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            - F1.powi(3) * PBD * hB.powi(2) * p * (1.0 - PBD) / 2.0
            - F1.powi(3) * hB.powi(2) * p * (1.0 - PBD).powi(2) / 2.0
            - F1.powi(2) * PBD * PDB * hB * hD * p * (1.0 - F0)
            - F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) / 2.0
            - PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) / 2.0
            - hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_PBD__dPDB(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            -F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (2.0 * PDB - 2.0) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            - F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (2.0 * PDB - 2.0) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) / 2.0
            - F1.powi(2) * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) / 2.0
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (2.0 * PDB - 2.0) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            - hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (2.0 * PDB - 2.0) / 2.0
            - hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
        ) * (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_PBD__dPBD(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (2.0 * PBD - 2.0) / 2.0
            - F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (2.0 * PBD - 2.0) / 2.0
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            -F0 * F1.powi(2) * PDB * hB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * p * (2.0 * PBD - 2.0) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            - F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (2.0 * PBD - 2.0) / 2.0
            - F1.powi(2) * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
        ) * (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_PBD__dhpi(_vars: Vars, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_hpi__dp(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hpi * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hpi * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hpi * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * hpi * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * (1.0 - F0).powi(2) * (1.0 - hD)
            - 2.0 * F0 * (1.0 - F0).powi(2)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * (1.0 - F1)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PDB * hB * hD * hOP * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB * hD * hpi * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB * hB * hD * hpi * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hD * hpi * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi / 2.0
            - F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            - F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0.powi(2) * PDB * hD.powi(2) * hpi * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0.powi(2) * PDB * hD.powi(2) * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0.powi(2) * PDB * hD * hpi * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0.powi(2) * hD.powi(2) * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0.powi(2) * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - F0.powi(2) * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0)
            - F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * (1.0 - hpi)
            - F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * (1.0 - PDB) / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * hOP * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * (1.0 - PBD) / 2.0
            - F0 * F1.powi(2) * PDB * hB * hD * hOP * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F0 * F1.powi(2) * hB * hD * hOP * hpi * (1.0 - PBD) * (1.0 - PDB)
            - F0 * F1 * PBD * PDB * hB * hD * hOP * (1.0 - F1) * (1.0 - hpi)
            - F0 * F1 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            - F0 * F1 * PBD * hB * hD * hOP * hpi * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * hB * hD * hOP * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F0 * F1 * PBD * hB * hD * hpi * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hB * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * F1 * PBD * hB * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * (1.0 - F0)
            - F0 * F1 * PDB * hB * hD * hOP * hpi * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hB * hD * hOP * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F0 * F1 * PDB * hB * hD * hpi * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * F1 * PDB * hB * hD * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hOP * hpi * (1.0 - F0) * (1.0 - PDB)
            - F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            - F0 * F1 * PDB * hD * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            - F0 * F1 * hB * hD * hOP * hpi * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            - F0 * F1 * hB * hD * hpi * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            - F0 * F1 * hB * hpi * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            - F0 * F1 * hD * hpi * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            - F0 * F1 * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            - F0 * PBD * hB * hD * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * PBD * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * PBD * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - F0 * PDB.powi(2) * hD.powi(2) * hpi * (1.0 - F0).powi(2) * (1.0 - hOP)
            - F0 * PDB * hB * hD * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * PDB * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * PDB * hD.powi(2) * hpi * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            - F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            - F0 * PDB * hD * hpi * (1.0 - F0).powi(2) * (1.0 - hD)
            - F0 * PDB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            - F0 * hB * hD * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            - F0 * hB * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            - F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            - F0 * hD * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            - F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            - F0 * (1.0 - F0).powi(2) * (1.0 - hD)
            + 2.0 * F0 * (1.0 - F0).powi(2)
            - F0 * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            - F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi / 2.0
            - F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * (1.0 - PBD) / 2.0
            - F1.powi(3) * PBD * hB.powi(2) * hOP * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            - F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * (1.0 - F1)
            - F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F1.powi(2) * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - hpi)
            - F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * (1.0 - F1) * (1.0 - PBD)
            - F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            - F1.powi(2) * PBD * hB.powi(2) * hpi * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F1.powi(2) * PBD * hB * hD * hOP * hpi * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F1.powi(2) * PBD * hB * hpi * (1.0 - F0) * (1.0 - hB) / 2.0
            - F1.powi(2) * PDB * hB * hD * hOP * hpi * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            - F1.powi(2) * hB.powi(2) * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F1.powi(2) * hB * hD * hOP * hpi * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            - F1.powi(2) * hB * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            - F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * (1.0 - F1).powi(2) / 2.0
            - F1 * PBD.powi(2) * hB.powi(2) * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            - F1 * PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            - F1 * PBD * hB.powi(2) * hOP * hpi * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB.powi(2) * hOP * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1 * PBD * hB.powi(2) * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            - F1 * PBD * hB * hD * hOP * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F1 * PBD * hB * hD * hpi * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F1 * PBD * hB * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            - F1 * PBD * hB * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * (1.0 - F0).powi(2) / 2.0
            - F1 * PDB * hB * hD * hOP * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1 * PDB * hB * hD * hpi * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F1 * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F1 * PDB * hD.powi(2) * hOP * hpi * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * PDB * hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F1 * PDB * hD * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            - F1 * hB.powi(2) * hOP * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            - F1 * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            - F1 * hB * hD * hOP * hpi * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            - F1 * hB * hD * hpi * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * hB * hpi * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            - F1 * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            - F1 * hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            - F1 * hD * hpi * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            - F1 * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            - PBD.powi(2) * hB.powi(2) * hpi * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            - PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            - PBD * hB.powi(2) * hpi * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - PBD * hB * hD * hpi * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - PBD * hB * hpi * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            - PBD * hB * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - PDB.powi(2) * hD.powi(2) * hpi * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            - PDB * hB * hD * hpi * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - PDB * hD.powi(2) * hpi * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - PDB * hD.powi(2) * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - PDB * hD * hpi * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            - PDB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            - hB.powi(2) * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - hB * hD * hpi * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            - hB * hpi * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            - hB * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            - hD.powi(2) * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - hD * hpi * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            - hD * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3)
            - (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hpi__dhB(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PBD * PDB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hpi) / 2.0
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            - F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            - F0 * F1 * p * (1.0 - F0) / 2.0
            + 2.0 * F1.powi(2) * PBD.powi(2) * hB * hOP * hpi * p * (1.0 - F1)
            + 2.0 * F1.powi(2) * PBD * hB * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + 2.0 * F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + 2.0 * F1 * PBD.powi(2) * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + 2.0 * F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1)
            + 2.0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PDB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + 2.0 * F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1 * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            - hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            - p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0 * F1.powi(2) * PBD * PDB * hD * hOP * p * (1.0 - hpi)
            - F0 * F1.powi(2) * PBD * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            - F0 * F1.powi(2) * PBD * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F0 * F1.powi(2) * PDB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            - F0 * F1.powi(2) * PDB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F0 * F1.powi(2) * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            - F0 * F1 * PBD * PDB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            - F0 * F1 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            - F0 * F1 * PBD * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F0 * F1 * PBD * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * F1 * PBD * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - F0 * F1 * PDB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F0 * F1 * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hpi) / 2.0
            - F0 * F1 * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            - F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * p * (1.0 - F0) / 2.0
            - F0 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            - F0 * PBD * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * PBD * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * PBD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - F0 * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi) / 2.0
            - F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1.powi(3) * PBD.powi(2) * hB * hOP * hpi * p
            - F1.powi(3) * PBD * hB * hOP * hpi * p * (1.0 - PBD)
            - F1.powi(3) * PBD * hB * hOP * p * (1.0 - PBD) * (1.0 - hpi)
            - F1.powi(3) * hB * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi)
            - 2.0 * F1.powi(2) * PBD.powi(2) * hB * hOP * hpi * p * (1.0 - F1)
            - F1.powi(2) * PBD.powi(2) * hB * hpi * p * (1.0 - F0) * (1.0 - hOP)
            - F1.powi(2) * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            - 2.0 * F1.powi(2) * PBD * hB * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            - 2.0 * F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            - F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) / 2.0
            - F1.powi(2) * PBD * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            - F1.powi(2) * PBD * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F1.powi(2) * PBD * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            - F1.powi(2) * PDB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            - F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1.powi(2) * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            - F1.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            - F1 * PBD.powi(2) * hB * hOP * hpi * p * (1.0 - F1).powi(2)
            - 2.0 * F1 * PBD.powi(2) * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            - F1 * PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            - F1 * PBD * hB * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD)
            - F1 * PBD * hB * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi)
            - 2.0 * F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1)
            - 2.0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            - F1 * PBD * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F1 * PBD * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F1 * PBD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            - F1 * PBD * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - F1 * PDB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hpi) / 2.0
            - F1 * hB * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            - 2.0 * F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            - F1 * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            - F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * p * (1.0 - F0).powi(2) / 2.0
            - F1 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            - PBD.powi(2) * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP)
            - PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            - PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP)
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            - PBD * hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            - PBD * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - PBD * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            - PBD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hpi) / 2.0
            - hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            - p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hpi__dhOP(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            - F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hpi)
            - F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0)
            - F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            - F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            - F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2)
            - F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB)
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi)
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            - F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            - F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD)
            - F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            - PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hpi)
            - PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hpi * p / 2.0
            - F0.powi(2) * F1 * PDB * hD.powi(2) * hpi * p * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * PDB * hD.powi(2) * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            - F0 * F1.powi(2) * PBD * PDB * hB * hD * p * (1.0 - hpi)
            - F0 * F1.powi(2) * PBD * hB * hD * hpi * p * (1.0 - PDB) / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F0 * F1.powi(2) * PDB * hB * hD * hpi * p * (1.0 - PBD) / 2.0
            - F0 * F1.powi(2) * PDB * hB * hD * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F0 * F1.powi(2) * hB * hD * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hpi)
            - F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F0 * F1 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F0 * F1 * PDB * hB * hD * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB)
            - F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            - F0 * F1 * hB * hD * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            - F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            - F1.powi(3) * PBD.powi(2) * hB.powi(2) * hpi * p / 2.0
            - F1.powi(3) * PBD * hB.powi(2) * hpi * p * (1.0 - PBD) / 2.0
            - F1.powi(3) * PBD * hB.powi(2) * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1.powi(3) * hB.powi(2) * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) / 2.0
            - F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F1)
            - F1.powi(2) * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            - F1.powi(2) * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F1.powi(2) * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            - F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            - F1.powi(2) * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1)
            - F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hpi)
            - F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD)
            - F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            - F1 * PBD * hB.powi(2) * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F1 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) / 2.0
            - F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            - F1 * hB.powi(2) * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB)
            - F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            - F1 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hpi__dPDB(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            -F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - hpi) / 2.0
            - F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            - F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - hpi)
            - F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - hpi) * (2.0 * PDB - 2.0)
            - F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi) * (2.0 * PDB - 2.0)
            + F0 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            - F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi)
            - F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi) / 2.0
            - F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - hpi) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - hpi) * (2.0 * PDB - 2.0) / 2.0
            - F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi) * (2.0 * PDB - 2.0) / 2.0
            - F0.powi(2) * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            - F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) / 2.0
            - F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - hpi) * (2.0 * PDB - 2.0)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * F1 * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi) * (2.0 * PDB - 2.0)
            - F0 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi)
            - F0 * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) / 2.0
            - F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F1 * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F1 * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - hpi) * (2.0 * PDB - 2.0) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            - F1 * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - hOP) * (1.0 - hpi) * (2.0 * PDB - 2.0) / 2.0
            - hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hpi__dPBD(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            -F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - hpi) / 2.0
            - F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            - F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - hpi) * (2.0 * PBD - 2.0)
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            - F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi) * (2.0 * PBD - 2.0)
            - F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi)
            - PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p / 2.0
            - F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            - F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) / 2.0
            - F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * F1 * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0 * F1 * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F0 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            - F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - hpi) / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - hpi) * (2.0 * PBD - 2.0) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - hpi)
            - F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) / 2.0
            - F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - hpi) * (2.0 * PBD - 2.0)
            - F1.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi) * (2.0 * PBD - 2.0) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            - F1.powi(2) * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            - F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - hpi) / 2.0
            - F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - F1 * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            - F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            - F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - hpi) * (2.0 * PBD - 2.0) / 2.0
            - F1 * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi) * (2.0 * PBD - 2.0)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F1 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            - F1 * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi)
            - PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            - hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) * (1.0 - hpi) * (2.0 * PBD - 2.0) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            - hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hpi__dhpi(vars: Vars, consts: Consts) -> f64 {
    let Vars {
        p,
        hB,
        hOP,
        PDB,
        PBD,
        hpi,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            -F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p
            + F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - PDB)
            - F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP)
            - F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * p * (1.0 - F0)
            - F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2)
            + F0 * F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hD)
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP)
            - F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1)
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            + F1 * PBD.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP)
            + F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB)
            - PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP)
            - PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * p / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            - F0.powi(2) * PDB.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0.powi(2) * PDB * hD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p
            - F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP)
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * p * (1.0 - F0)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * F1 * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            - F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            - F0 * F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2)
            - F0 * F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F0 * PDB.powi(2) * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            - F0 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            - F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            - F0 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP)
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD)
            - F0 * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            - F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * p / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            - F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1)
            - F1.powi(2) * PBD.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            - F1.powi(2) * PBD * hB * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            - F1.powi(2) * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) / 2.0
            - F1 * PBD.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) / 2.0
            - F1 * PBD.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            - F1 * PDB.powi(2) * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP)
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            - F1 * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * hB * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            - F1 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            - PBD.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP)
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - PBD * hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            - PDB.powi(2) * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            - PDB * hD * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            - hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            - hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * PDB * hD * hpi * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - hpi)
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - PDB) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - hpi)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0)
            + F0 * F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB)
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi)
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * F1 * hB * hD * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F0 * F1 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * F1 * hB * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hpi)
            + F0 * F1 * hD * hpi * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PBD * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F0 * PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - hOP)
            + F0 * PDB * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F0 * PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * PDB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F0 * hB * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F0 * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F0 * hD * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi)
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD)
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + 2.0 * F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1)
            + F1.powi(2) * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi)
            + F1.powi(2) * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1.powi(2) * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hpi)
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F0) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1.powi(2) * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - PDB)
            + F1.powi(2) * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hpi)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB.powi(2) * hOP * hpi * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * PBD * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + F1 * PDB.powi(2) * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) / 2.0
            + F1 * PDB * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hpi) / 2.0
            + F1 * PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hpi) / 2.0
            + F1 * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi)
            + F1 * hB * hD * hOP * hpi * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB)
            + F1 * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi)
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hpi) / 2.0
            + F1 * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PBD.powi(2) * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) * (1.0 - hpi)
            + PBD * hB.powi(2) * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PBD * hB * hpi * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + PDB.powi(2) * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD.powi(2) * hpi * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + PDB * hD * hpi * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hB * hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - PDB) * (1.0 - hOP)
            + hB * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) * (1.0 - hpi) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) * (1.0 - hpi) / 2.0
            + hD * hpi * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hB) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) * (1.0 - hpi) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn lhs(vars: Vars, consts: Consts) -> nd::Array1<f64> {
    return array![
        lhs_p(vars, consts),
        lhs_hB(vars, consts),
        lhs_hOP(vars, consts),
        lhs_PDB(vars, consts),
        lhs_PBD(vars, consts),
        lhs_hpi(vars, consts),
    ];
}

pub fn jacobian(vars: Vars, consts: Consts) -> nd::Array2<f64> {
    return array![
        [
            dlhs_p__dp(vars, consts),
            dlhs_p__dhB(vars, consts),
            dlhs_p__dhOP(vars, consts),
            dlhs_p__dPDB(vars, consts),
            dlhs_p__dPBD(vars, consts),
            dlhs_p__dhpi(vars, consts),
        ],
        [
            dlhs_hB__dp(vars, consts),
            dlhs_hB__dhB(vars, consts),
            dlhs_hB__dhOP(vars, consts),
            dlhs_hB__dPDB(vars, consts),
            dlhs_hB__dPBD(vars, consts),
            dlhs_hB__dhpi(vars, consts),
        ],
        [
            dlhs_hOP__dp(vars, consts),
            dlhs_hOP__dhB(vars, consts),
            dlhs_hOP__dhOP(vars, consts),
            dlhs_hOP__dPDB(vars, consts),
            dlhs_hOP__dPBD(vars, consts),
            dlhs_hOP__dhpi(vars, consts),
        ],
        [
            dlhs_PDB__dp(vars, consts),
            dlhs_PDB__dhB(vars, consts),
            dlhs_PDB__dhOP(vars, consts),
            dlhs_PDB__dPDB(vars, consts),
            dlhs_PDB__dPBD(vars, consts),
            dlhs_PDB__dhpi(vars, consts),
        ],
        [
            dlhs_PBD__dp(vars, consts),
            dlhs_PBD__dhB(vars, consts),
            dlhs_PBD__dhOP(vars, consts),
            dlhs_PBD__dPDB(vars, consts),
            dlhs_PBD__dPBD(vars, consts),
            dlhs_PBD__dhpi(vars, consts),
        ],
        [
            dlhs_hpi__dp(vars, consts),
            dlhs_hpi__dhB(vars, consts),
            dlhs_hpi__dhOP(vars, consts),
            dlhs_hpi__dPDB(vars, consts),
            dlhs_hpi__dPBD(vars, consts),
            dlhs_hpi__dhpi(vars, consts),
        ],
    ];
}

pub fn all_corr(
    vars_init: Vars,
    consts: Consts,
    maxiters: Option<usize>,
    epsilon: Option<f64>,
    printflag: bool,
) -> SolverResult<Vars>
{
    let eps: f64 = epsilon.unwrap_or(EPSILON);
    let mut x: nd::Array1<f64> = vars_init.into();
    let mut dx: nd::Array1<f64>;
    let mut y: nd::Array1<f64>;
    let mut J: nd::Array2<f64>;
    for k in 0..maxiters.unwrap_or(MAXITERS) {
        if printflag {
            print_flush!("  {} {}          \r", x, k);
        }
        y = lhs(x.clone().into(), consts);
        J = jacobian(x.clone().into(), consts);
        if J.det()?.abs() < 1e-15 {
            return Err(SolverError::SingularJacobian);
        }
        dx = -J.inv()?.dot(&y);
        x += &dx;
        if dx.norm() < eps {
            if printflag {
                println!();
            }
            return Ok(x.into());
        }
    }
    if printflag { println!(); }
    return Err(SolverError::NoConverge);
}

/******************************************************************************/

/// Holds values for all the single-site variables being solved for. This is
/// identical to [`Vars`], except `hpi` is omitted because we don't have
/// single-site π-pulse data.
#[derive(Copy, Clone, Debug)]
pub struct VarsSingle {
    pub p: f64,
    pub hB: f64,
    // pub hD: f64,
    pub hOP: f64,
    pub PDB: f64,
    pub PBD: f64,
}

impl From<VarsSingle> for nd::Array1<f64> {
    fn from(vars: VarsSingle) -> Self {
        return array![
            vars.p,
            vars.hB,
            vars.hOP,
            vars.PDB,
            vars.PBD,
        ];
    }
}

/// *Panics* if `arr` has the wrong length.
impl From<nd::Array1<f64>> for VarsSingle {
    fn from(arr: nd::Array1<f64>) -> Self {
        if arr.len() != 5 {
            panic!(
                "VarsSingle::from(ndarray::Array1<f64>): array has incorrect \
                length"
            );
        }
        return Self {
            p: arr[0],
            hB: arr[1],
            hOP: arr[2],
            PDB: arr[3],
            PBD: arr[4],
        };
    }
}

pub fn lhs_p_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB: _,
        hOP,
        PDB: _,
        PBD: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD: _,
        mp,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        -mp
        + F1 * hOP * p
        + p * (1.0 - F0) * (1.0 - hOP)
        + (1.0 - F0) * (1.0 - p)
    ;
}

pub fn lhs_hB_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        -mhB
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB) + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    ;
}

pub fn lhs_hOP_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        -mhOP
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    ;
}

pub fn lhs_PDB_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        -mPDB
        + (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0 
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
    ;
}

pub fn lhs_PBD_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD,
        mhpi: _,
    } = consts;
    return
        -mPBD
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
    ;
}

pub fn dlhs_p__dp_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p: _,
        hB: _,
        hOP,
        PDB: _,
        PBD: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD: _,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        F0
        + F1 * hOP
        + (1.0 - F0) * (1.0 - hOP)
        - 1.0
    ;
}
pub fn dlhs_p__dhB_single(_vars: VarsSingle, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_p__dhOP_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB: _,
        hOP: _,
        PDB: _,
        PBD: _,
    } = vars;
    let Consts {
        F0,
        F1,
        hD: _,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        F1 * p
        - p * (1.0 - F0)
    ;
}

pub fn dlhs_p__dPDB_single(_vars: VarsSingle, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_p__dPBD_single(_vars: VarsSingle, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_hB__dp_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hB * hOP.powi(2)
            + F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            + F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP) - (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            -F0 * F1 * PBD * hB * hOP * (1.0 - hOP)
            - F0 * F1 * hB * hOP * (1.0 - PBD) * (1.0 - hOP)
            - F0 * F1 * hOP * (1.0 - hB)
            - F0 * PDB * hD * (1.0 - F0) * (1.0 - hOP).powi(2)
            - F0 * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - F0 * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0)
            - F1.powi(2) * PBD * hB * hOP.powi(2)
            - F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            - F1 * PBD * hB * hOP.powi(2) * (1.0 - F1)
            - F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP.powi(2) * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * hOP * (1.0 - F0) * (1.0 - hB)
            - PDB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            - hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            - hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hB__dhB_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hOP.powi(2) * p
            + F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hOP * p * (1.0 - F0)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F0 * F1 * PBD * hOP * p * (1.0 - hOP)
            - F0 * F1 * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p
            - F1.powi(2) * PBD * hOP.powi(2) * p
            - F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            - F1 * PBD * hOP.powi(2) * p * (1.0 - F1)
            - F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0)
        ) * (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hB__dhOP_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            F0 * F1 * PBD * hB * hOP * p
            - F0 * F1 * PBD * hB * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD)
            - F0 * F1 * hB * p * (1.0 - PBD) * (1.0 - hOP)
            - F0 * F1 * p * (1.0 - hB)
            - F0 * PDB * hD * p * (1.0 - F0) * (2.0 * hOP - 2.0)
            - F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            + F0 * p * (1.0 - F0) * (1.0 - hD)
            - 2.0 * F1.powi(2) * PBD * hB * hOP * p
            - 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0)
            - 2.0 * F1 * PBD * hB * hOP * p * (1.0 - F1)
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0)
            - F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            - 2.0 * F1 * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            - F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            - PDB * hD * p * (1.0 - F0).powi(2) * (2.0 * hOP - 2.0)
            - PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB)
            - hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            - hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
        + (
            2.0 * F1.powi(2) * PBD * hB * hOP * p
            + 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            - F1 * PBD * hB * hOP * p * (1.0 - F0)
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * p * (1.0 - F0)
            + F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            + F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (2.0 * hOP - 2.0)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            - p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PBD * hB * hOP * p * (1.0 - hOP)
            + F0 * F1 * hB * hOP * p * (1.0 - PBD) * (1.0 - hOP)
            + F0 * F1 * hOP * p * (1.0 - hB)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    ;
}

pub fn dlhs_hB__dPDB_single(_vars: VarsSingle, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_hB__dPBD_single(_vars: VarsSingle, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_hOP__dp_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hB * hOP.powi(2)
            + F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            + F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            - (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            -F0 * F1 * PDB * hD * hOP * (1.0 - hOP)
            - F0 * F1 * hD * hOP * (1.0 - PDB) * (1.0 - hOP)
            - F0 * PDB * hD * (1.0 - F0) * (1.0 - hOP).powi(2)
            - F0 * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - F0 * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0)
            - F1.powi(2) * PBD * hB * hOP.powi(2)
            - F1.powi(2) * hB * hOP.powi(2) * (1.0 - PBD)
            - F1 * PBD * hB * hOP.powi(2) * (1.0 - F1)
            - F1 * PBD * hB * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP.powi(2) * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * hOP * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * hOP * (1.0 - F0) * (1.0 - hB)
            - PBD * hB * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - PDB * hD * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            - hB * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            - hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            - (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hOP__dhB_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hOP.powi(2) * p
            + F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hOP * p * (1.0 - F0)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
        + (
            -F1.powi(2) * PBD * hOP.powi(2) * p
            - F1.powi(2) * hOP.powi(2) * p * (1.0 - PBD)
            - F1 * PBD * hOP.powi(2) * p * (1.0 - F1)
            - F1 * PBD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0)
            - PBD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hOP * p * (1.0 - F0) * (1.0 - F1)
        ) * (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_hOP__dhOP_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ) * (
            F0 * F1 * PDB * hD * hOP * p
            - F0 * F1 * PDB * hD * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB)
            - F0 * F1 * hD * p * (1.0 - PDB) * (1.0 - hOP)
            - F0 * PDB * hD * p * (1.0 - F0) * (2.0 * hOP - 2.0)
            - F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            + F0 * p * (1.0 - F0) * (1.0 - hD)
            - 2.0 * F1.powi(2) * PBD * hB * hOP * p
            - 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP * p * (1.0 - F0)
            - 2.0 * F1 * PBD * hB * hOP * p * (1.0 - F1)
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0)
            - F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            - 2.0 * F1 * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            - F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            - F1 * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1)
            - PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - PDB * hD * p * (1.0 - F0).powi(2) * (2.0 * hOP - 2.0)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD)
            - hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            + p * (1.0 - F0).powi(2) * (1.0 - hD)
            - p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        ).powi(2)
        + (
            2.0 * F1.powi(2) * PBD * hB * hOP * p
            + 2.0 * F1.powi(2) * hB * hOP * p * (1.0 - PBD)
            - F1 * PBD * hB * hOP * p * (1.0 - F0)
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * PDB * hD * hOP * p * (1.0 - F0)
            + F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP)
            - F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD)
            + F1 * hB * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB)
            + F1 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * p * (1.0 - F0) * (1.0 - hB)
            + PDB * hD * p * (1.0 - F0).powi(2) * (2.0 * hOP - 2.0)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (2.0 * hOP - 2.0)
            - p * (1.0 - F0).powi(2) * (1.0 - hD)
        ) / (
            F0 * F1 * PDB * hD * hOP * p * (1.0 - hOP)
            + F0 * F1 * hD * hOP * p * (1.0 - PDB) * (1.0 - hOP)
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - hOP).powi(2)
            + F0 * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + F0 * p * (1.0 - F0) * (1.0 - hD) * (1.0 - hOP)
            + F0 * (1.0 - F0) * (1.0 - p)
            + F1.powi(2) * PBD * hB * hOP.powi(2) * p
            + F1.powi(2) * hB * hOP.powi(2) * p * (1.0 - PBD)
            + F1 * PBD * hB * hOP.powi(2) * p * (1.0 - F1)
            + F1 * PBD * hB * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - hOP)
            + F1 * hB * hOP.powi(2) * p * (1.0 - F1) * (1.0 - PBD)
            + F1 * hB * hOP * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP)
            + F1 * hOP * p * (1.0 - F0) * (1.0 - hB)
            + PBD * hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP).powi(2)
            + hB * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP).powi(2)
            + hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB)
            + p * (1.0 - F0).powi(2) * (1.0 - hD) * (1.0 - hOP)
            + (1.0 - F0).powi(2) * (1.0 - p)
        )
    ;
}

pub fn dlhs_hOP__dPDB_single(_vars: VarsSingle, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_hOP__dPBD_single(_vars: VarsSingle, _consts: Consts) -> f64 { 0.0 }

pub fn dlhs_PDB__dp_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0.powi(2) * PDB * hD.powi(2) * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0.powi(2) * (1.0 - F0)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0 - F0 * (1.0 - F0).powi(2)
            + F0 * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0 + F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * (1.0 - PDB).powi(2) / 2.0
            - F0.powi(2) * PDB * hD.powi(2) * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD.powi(2) * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - F0.powi(2) * (1.0 - F0) * (1.0 - hD) / 2.0 + F0.powi(2) * (1.0 - F0)
            - F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            - F0 * F1.powi(2) * PDB * hB * hD * hOP * (1.0 - PBD) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * hOP * (1.0 - F1)
            - F0 * F1 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hB * hD * hOP * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hB * hD * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hB * hD * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hD * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - F0 * PBD * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * PBD * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F0 * PDB * hB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * PDB * hD * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            - F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0 + F0 * (1.0 - F0).powi(2)
            - F0 * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PBD * hB.powi(2) * hOP * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F1 * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * hB.powi(2) * hOP * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            - F1 * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            - F1 * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            - PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - PBD * hB * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - hB.powi(2) * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            - hB * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            - (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    ;
}

pub fn dlhs_PDB__dhB_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F0 * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            + F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * PDB * hD * hOP * p * (1.0 - PBD) / 2.0
            - F0 * F1 * PBD * PDB * hD * hOP * p * (1.0 - F1)
            - F0 * F1 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) / 2.0
            - F0 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            - F0 * PBD * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * PBD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            - F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PBD * hB * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD)
            - F1 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            - F1 * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * hB * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2)
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP)
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            - PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - PBD * hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP)
            - PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - PBD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            - hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP)
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            - p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    ;
}

pub fn dlhs_PDB__dhOP_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            -F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            - PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            -F0.powi(2) * F1 * PDB * hD.powi(2) * p * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            - F0 * F1.powi(2) * PDB * hB * hD * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F1)
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hB * hD * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB.powi(2) * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
        ) * (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    ;
}

pub fn dlhs_PDB__dPDB_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            -F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            - F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (2.0 * PDB - 2.0) / 2.0
            - F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (2.0 * PDB - 2.0) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            - F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            - F0.powi(2) * F1 * hD.powi(2) * hOP * p * (2.0 * PDB - 2.0) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            - F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) / 2.0
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            - F0 * F1 * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (2.0 * PDB - 2.0) / 2.0
            - F0 * F1 * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F0 * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    ;
}

pub fn dlhs_PDB__dPBD_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (2.0 * PBD - 2.0) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        )
        + (
            -F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) / 2.0
            - F0 * F1 * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - F0 * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (2.0 * PBD - 2.0) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            - F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (2.0 * PBD - 2.0) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (2.0 * PBD - 2.0) / 2.0
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) / 2.0
            - PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            - hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hOP) * (2.0 * PBD - 2.0) / 2.0
            - hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - hB) / 2.0
        ) * (
            F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PDB) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0.powi(2) * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - PDB) / 2.0
            + F0.powi(2) * F1 * hD.powi(2) * hOP * p * (1.0 - PDB).powi(2) / 2.0
            + F0.powi(2) * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0.powi(2) * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0.powi(2) * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0.powi(2) * (1.0 - F0) * (1.0 - p)
            + F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - PBD) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F1)
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP)
            + F0 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hD) / 2.0
            + F0 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * PDB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * hB.powi(2) * hOP * p * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hD) / 2.0
            + hB.powi(2) * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + hB * p * (1.0 - F0) * (1.0 - F1).powi(2) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ).powi(2)
    ;
}

pub fn dlhs_PBD__dp_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F0 * (1.0 - F0).powi(2)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * hOP * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hB * hD * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hB * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * F1 * (1.0 - F0) * (1.0 - hB) / 2.0
            - F0 * PDB * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - F0 * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2)
            - F1.powi(3) * PBD * hB.powi(2) * hOP * (1.0 - PBD) / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * (1.0 - PBD).powi(2) / 2.0
            - F1.powi(2) * PBD * PDB * hB * hD * hOP * (1.0 - F0)
            - F1.powi(2) * PBD * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1)
            - F1 * PBD * PDB * hB * hD * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F1 * PBD * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * PBD * hB * hD * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * PBD * hB * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F1 * PDB * hB * hD * hOP * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * PDB * hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            - F1 * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            - PDB * hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - PDB * hD.powi(2) * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - hD.powi(2) * hOP * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            - hD.powi(2) * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            - hD * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            - (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3)
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_PBD__dhB_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            - F0 * F1 * p * (1.0 - F0) / 2.0
            + F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            + F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            + F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP)
            + F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP)
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            - PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            -F0 * F1.powi(2) * PBD * PDB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * PBD * hD * hOP * p * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * PDB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * PBD * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * p * (1.0 - F0) / 2.0
            - F1.powi(3) * PBD * hB * hOP * p * (1.0 - PBD)
            - F1.powi(3) * hB * hOP * p * (1.0 - PBD).powi(2)
            - F1.powi(2) * PBD * PDB * hD * hOP * p * (1.0 - F0)
            - F1.powi(2) * PBD * hB * hOP * p * (1.0 - F1) * (1.0 - PBD)
            - F1.powi(2) * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2)
            - F1 * PBD * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            - F1 * PBD * PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F1 * PBD * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * PBD * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * PBD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F1 * PDB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * p * (1.0 - F0).powi(2) / 2.0
        ) * (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_PBD__dhOP_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * p * (1.0 - PDB) / 2.0
            - F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) / 2.0
            - F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            - PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            - PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) * (
            -F0 * F1.powi(2) * PBD * PDB * hB * hD * p / 2.0
            - F0 * F1.powi(2) * PBD * hB * hD * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            - F1.powi(3) * PBD * hB.powi(2) * p * (1.0 - PBD) / 2.0
            - F1.powi(3) * hB.powi(2) * p * (1.0 - PBD).powi(2) / 2.0
            - F1.powi(2) * PBD * PDB * hB * hD * p * (1.0 - F0)
            - F1.powi(2) * PBD * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1.powi(2) * PDB * hB * hD * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) / 2.0
            - F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) / 2.0
            - PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) / 2.0
            - hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_PBD__dPDB_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            -F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (2.0 * PDB - 2.0) / 2.0
            - F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            - F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (2.0 * PDB - 2.0) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            - F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) / 2.0
            - F1.powi(2) * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            - F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) / 2.0
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            - F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (2.0 * PDB - 2.0) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - hOP) / 2.0
            - hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (2.0 * PDB - 2.0) / 2.0
            - hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - hOP) * (2.0 * PDB - 2.0) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
        ) * (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn dlhs_PBD__dPBD_single(vars: VarsSingle, consts: Consts) -> f64 {
    let VarsSingle {
        p,
        hB,
        hOP,
        PDB,
        PBD,
    } = vars;
    let Consts {
        F0,
        F1,
        hD,
        mp: _,
        mhB: _,
        mhOP: _,
        mPDB: _,
        mPBD: _,
        mhpi: _,
    } = consts;
    return
        (
            F0 * F1.powi(2) * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            - F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (2.0 * PBD - 2.0) / 2.0
            - F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hOP) * (2.0 * PBD - 2.0) / 2.0
            - F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        )
        + (
            -F0 * F1.powi(2) * PDB * hB * hD * hOP * p / 2.0
            - F0 * F1.powi(2) * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            - F0 * F1 * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            - F0 * F1 * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F0 * F1 * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            - F1.powi(3) * hB.powi(2) * hOP * p * (2.0 * PBD - 2.0) / 2.0
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) / 2.0
            - F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            - F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (2.0 * PBD - 2.0) / 2.0
            - F1.powi(2) * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            - F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            - F1 * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            - F1 * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            - F1 * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            - F1 * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
        ) * (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) / 2.0
            + F1 * PBD * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * hB.powi(2) * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD).powi(2) * (1.0 - hOP) / 2.0
            + F1 * hB * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hB) / 2.0
            + PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hOP) / 2.0
            + PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PBD) * (1.0 - hOP) / 2.0
            + PDB * hD * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - hB) / 2.0
        ) / (
            F0 * F1.powi(2) * PBD * PDB * hB * hD * hOP * p / 2.0
            + F0 * F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - PDB) / 2.0
            + F0 * F1 * PBD * PDB * hB * hD * p * (1.0 - F0) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * hD * p * (1.0 - F0) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * F1 * PBD * hB * p * (1.0 - F0) * (1.0 - hD) / 2.0
            + F0 * F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F0 * F1 * hD.powi(2) * hOP * p * (1.0 - F0) * (1.0 - PDB).powi(2) / 2.0
            + F0 * F1 * p * (1.0 - F0) * (1.0 - hB) / 2.0
            + F0 * PDB * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F0 * hD.powi(2) * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + F0 * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + F0 * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F0 * (1.0 - F0).powi(2) * (1.0 - p)
            + F1.powi(3) * PBD * hB.powi(2) * hOP * p * (1.0 - PBD) / 2.0
            + F1.powi(3) * hB.powi(2) * hOP * p * (1.0 - PBD).powi(2) / 2.0
            + F1.powi(2) * PBD * PDB * hB * hD * hOP * p * (1.0 - F0)
            + F1.powi(2) * PBD * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1.powi(2) * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PDB) / 2.0
            + F1.powi(2) * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - PBD) / 2.0
            + F1.powi(2) * hB.powi(2) * hOP * p * (1.0 - F1) * (1.0 - PBD).powi(2) / 2.0
            + F1 * PBD * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1)
            + F1 * PBD * PDB * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + F1 * PBD * hB * hD * p * (1.0 - F0).powi(2) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + F1 * PBD * hB * p * (1.0 - F0).powi(2) * (1.0 - hD) / 2.0
            + F1 * PDB * hB * hD * hOP * p * (1.0 - F0) * (1.0 - F1) * (1.0 - PBD) / 2.0
            + F1 * PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB) / 2.0
            + F1 * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - PDB).powi(2) / 2.0
            + F1 * p * (1.0 - F0).powi(2) * (1.0 - hB) / 2.0
            + PDB * hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB) / 2.0
            + PDB * hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hOP) / 2.0
            + hD.powi(2) * hOP * p * (1.0 - F0).powi(2) * (1.0 - F1) * (1.0 - PDB).powi(2) / 2.0
            + hD.powi(2) * p * (1.0 - F0).powi(3) * (1.0 - PDB).powi(2) * (1.0 - hOP) / 2.0
            + hD * p * (1.0 - F0).powi(3) * (1.0 - PDB) * (1.0 - hD) / 2.0
            + p * (1.0 - F0).powi(3) * (1.0 - hD) / 2.0
            + (1.0 - F0).powi(3) * (1.0 - p)
        ).powi(2)
    ;
}

pub fn lhs_single(vars: VarsSingle, consts: Consts) -> nd::Array1<f64> {
    return array![
        lhs_p_single(vars, consts),
        lhs_hB_single(vars, consts),
        lhs_hOP_single(vars, consts),
        lhs_PDB_single(vars, consts),
        lhs_PBD_single(vars, consts),
    ];
}

pub fn jacobian_single(vars: VarsSingle, consts: Consts) -> nd::Array2<f64> {
    return array![
        [
            dlhs_p__dp_single(vars, consts),
            dlhs_p__dhB_single(vars, consts),
            dlhs_p__dhOP_single(vars, consts),
            dlhs_p__dPDB_single(vars, consts),
            dlhs_p__dPBD_single(vars, consts),
        ],
        [
            dlhs_hB__dp_single(vars, consts),
            dlhs_hB__dhB_single(vars, consts),
            dlhs_hB__dhOP_single(vars, consts),
            dlhs_hB__dPDB_single(vars, consts),
            dlhs_hB__dPBD_single(vars, consts),
        ],
        [
            dlhs_hOP__dp_single(vars, consts),
            dlhs_hOP__dhB_single(vars, consts),
            dlhs_hOP__dhOP_single(vars, consts),
            dlhs_hOP__dPDB_single(vars, consts),
            dlhs_hOP__dPBD_single(vars, consts),
        ],
        [
            dlhs_PDB__dp_single(vars, consts),
            dlhs_PDB__dhB_single(vars, consts),
            dlhs_PDB__dhOP_single(vars, consts),
            dlhs_PDB__dPDB_single(vars, consts),
            dlhs_PDB__dPBD_single(vars, consts),
        ],
        [
            dlhs_PBD__dp_single(vars, consts),
            dlhs_PBD__dhB_single(vars, consts),
            dlhs_PBD__dhOP_single(vars, consts),
            dlhs_PBD__dPDB_single(vars, consts),
            dlhs_PBD__dPBD_single(vars, consts),
        ],
    ];
}

pub fn single_corr(
    vars_init: VarsSingle,
    consts: Consts,
    maxiters: Option<usize>,
    epsilon: Option<f64>,
    printflag: bool,
) -> SolverResult<VarsSingle>
{
    let eps: f64 = epsilon.unwrap_or(EPSILON);
    let mut x: nd::Array1<f64> = vars_init.into();
    let mut dx: nd::Array1<f64>;
    let mut y: nd::Array1<f64>;
    let mut J: nd::Array2<f64>;
    for k in 0..maxiters.unwrap_or(MAXITERS) {
        if printflag {
            print_flush!("  {} {}          \r", x, k);
        }
        y = lhs_single(x.clone().into(), consts);
        J = jacobian_single(x.clone().into(), consts);
        if J.det()?.abs() < 1e-15 {
            return Err(SolverError::SingularJacobian);
        }
        dx = -J.inv()?.dot(&y);
        x += &dx;
        // x.iter_mut()
        //     .for_each(|xk| *xk = xk.max(0.0));
        if dx.norm() < eps {
            if printflag {
                println!();
            }
            return Ok(x.into());
        }
    }
    if printflag { println!(); }
    return Err(SolverError::NoConverge);
}

