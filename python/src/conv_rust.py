import re
import sys

lhs_pat = re.compile(r"([a-zA-Z0-9]+) : (.*)")
diff_pat = re.compile(r"\('([a-zA-Z0-9]+)', '([a-zA-Z0-9]+)'\) : (.*)")
expr_subs = [
    (re.compile(r"\*\*([0-9]+)"), r".powi(\1)"),
    (re.compile(r"(\*|\/)"), r" \1 "),
    (re.compile(r"(\+|-|\*|\/) ([1-9]+)"), r"\1 \2.0"),
    (re.compile(r"([^a-zA-Z])([1-9]+) (\+|-|\*|\/)"), r"\1\2.0 \3"),
    (re.compile(r"([^0]|(\/ [0-9]+\.0)) (\+|-) ([^0-9])"), r"\1\n\3 \4"),
]

vars_type = "Vars"
vars_symbols = [
    "p",
    "hB",
    "hOP",
    "PDB",
    "PBD",
    "hsurv",
    "hcpi",
]
bind_vars = f"    let {vars_type} {{ {', '.join(s for s in vars_symbols)} }} = vars;"

consts_type = "Consts"
consts_symbols = [
    "F0",
    "F1",
    "hD",
    "mp",
    "mhB",
    "mhOP",
    "mPDB",
    "mPBD",
    "mhsurv",
    "mhcpi",
]
bind_consts = f"    let {consts_type} {{ {', '.join(s for s in consts_symbols)} }} = consts;"

def indent_block(s: str, indent: str="    ", n: int=1) -> str:
    return "\n".join(n * indent + line for line in s.split('\n'))

def process_line(line: str) -> str:
    if m := lhs_pat.match(line):
        func_head = f"fn lhs_{m.group(1)}(vars: {vars_type}, consts: {consts_type}) -> f64 {{"
        expr = m.group(2)
        for (pat, sub) in expr_subs:
            expr = pat.sub(sub, expr)
        expr = indent_block(expr)
        return f"{func_head}\n{bind_vars}\n{bind_consts}\n{expr}\n}}\n\n"
    elif m := diff_pat.match(line):
        func_head = f"fn dlhs_{m.group(1)}__d{m.group(2)}(vars: {vars_type}, consts: {consts_type}) -> f64 {{"
        expr = m.group(3)
        for (pat, sub) in expr_subs:
            expr = pat.sub(sub, expr)
        expr = indent_block(expr)
        return f"{func_head}\n{bind_vars}\n{bind_consts}\n{expr}\n}}\n\n"
    else:
        return "// " + line

usage = "usage: conv_rust.py <source> <target>"

try:
    source = sys.argv[1]
except IndexError:
    print("missing source file")
    print(usage)
    sys.exit(1)

if source == "help":
    print(usage)
    sys.exit(0)

try:
    target = sys.argv[2]
except IndexError:
    print("missing target file")
    print(usage)
    sys.exit(1)

with open(source, 'r') as infile:
    lines = infile.readlines()

new_lines = [process_line(line) for line in lines]

with open(target, 'w') as outfile:
    outfile.writelines(new_lines)

