import toml
import sys
import numpy as np
from lib.analysis import value_str

results_path = "output/results.toml"
with open(results_path, 'r') as infile:
    results_data = toml.load(infile)

for key, data in results_data.items():
    F0_data = data["F0"]
    F1_data = data["F1"]
    PBD_data = data["PBD"]
    PDB_data = data["PDB"]
    hB_data = data["hB"]
    hD_raw_data = data["hD_raw"]
    hOP_data = data["hOP"]
    p_data = data["p"]

    score = (
        F0_data[0]
        * F1_data[0]
        * (1 - PBD_data[0])
        * (1 - PDB_data[0])
        * hB_data[0]
        * hD_raw_data[0]
        * hOP_data[0]
        # * p_data[0]
    )
    score_err = np.sqrt(
        (
            F0_data[1]
            * F1_data[0]
            * (1 - PBD_data[0])
            * (1 - PDB_data[0])
            * hB_data[0]
            * hD_raw_data[0]
            * hOP_data[0]
            # * p_data[0]
        )**2
        + (
            F0_data[0]
            * F1_data[1]
            * (1 - PBD_data[0])
            * (1 - PDB_data[0])
            * hB_data[0]
            * hD_raw_data[0]
            * hOP_data[0]
            # * p_data[0]
        )**2
        + (
            F0_data[0]
            * F1_data[0]
            * PBD_data[1]
            * (1 - PDB_data[0])
            * hB_data[0]
            * hD_raw_data[0]
            * hOP_data[0]
            # * p_data[0]
        )**2
        + (
            F0_data[0]
            * F1_data[0]
            * (1 - PBD_data[0])
            * PDB_data[1]
            * hB_data[0]
            * hD_raw_data[0]
            * hOP_data[0]
            # * p_data[0]
        )**2
        + (
            F0_data[0]
            * F1_data[0]
            * (1 - PBD_data[0])
            * (1 - PDB_data[0])
            * hB_data[1]
            * hD_raw_data[0]
            * hOP_data[0]
            # * p_data[0]
        )**2
        + (
            F0_data[0]
            * F1_data[0]
            * (1 - PBD_data[0])
            * (1 - PDB_data[0])
            * hB_data[0]
            * hD_raw_data[1]
            * hOP_data[0]
            # * p_data[0]
        )**2
        + (
            F0_data[0]
            * F1_data[0]
            * (1 - PBD_data[0])
            * (1 - PDB_data[0])
            * hB_data[0]
            * hD_raw_data[0]
            * hOP_data[1]
            # * p_data[0]
        )**2
        # + (
        #     F0_data[0]
        #     * F1_data[0]
        #     * (1 - PBD_data[0])
        #     * (1 - PDB_data[0])
        #     * hB_data[0]
        #     * hD_raw_data[0]
        #     * hOP_data[0]
        #     * p_data[1]
        # )**2
    )
    print(key)
    print(value_str(score, score_err))



