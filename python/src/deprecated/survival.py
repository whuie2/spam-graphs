from dataclasses import dataclass
import numpy as np
import sympy as sy

def value_str(
    x: float,
    err: float,
    dec: int=None,
    trunc: bool=True,
    latex: bool=False,
) -> str:
    _err = err if np.isfinite(err) else -1.0
    z = int(-np.floor(np.log10(_err))) if _err > 0.0 else 5
    z = min(dec, z) if dec is not None else max(z, 0)
    if trunc:
        if _err > 0.0:
            outstr = f"{{:.{z:.0f}f}}({{:.0f}})".format(x, 10**z * _err)
        else:
            outstr = f"{{:.{z:.0f}f}}(~)".format(x)
    else:
        if _err > 0.0:
            outstr = f"{{:.{z:.0f}f}} +/- {{:.{z:.0f}f}}".format(x, err)
        else:
            outstr = f"{{:.{z:.0f}f}} +/- [~]".format(x)
    if latex:
        outstr = "$" + outstr.replace("+/-", "\\pm") + "$"
    return outstr

class Node:
    label: str

    def __init__(self, label: str):
        self.label = label

    def __hash__(self):
        return hash(self.label)

    def __str__(self):
        return self.label

    def __repr__(self):
        return self.label

class N:
    start0 = Node("Start in |0>")
    start1 = Node("Start in |1>")

    read0_BT = Node("Bright0 (true)")
    read0_BF = Node("Bright0 (false)")
    read0_DF = Node("Dark0 (false)")
    read0_DT = Node("Dark0 (true)")

    surv0 = Node("Survive |0>")
    surv1 = Node("Survive |1>")
    nosurv = Node("Don't survive")

    pump0 = Node("Pump to |0>")
    pump1 = Node("Pump to |1>")

    read1_BT = Node("Bright1 (true)")
    read1_BF = Node("Bright1 (false)")
    read1_DF = Node("Dark1 (false)")
    read1_DT = Node("Dark1 (true)")

(F0, F1, hB, hD, hOP, PBD, PDB) = sy.symbols("F0 F1 hB hD hOP PB>D PD>B")
(F0_err, F1_err, hB_err, hD_err, hOP_err, PBD_err, PDB_err) \
    = sy.symbols("F0_err F1_err hB_err hD_err hOP_err PB>D_err PD>B_err")
(mhB, mhD, mhOP, mPBD, mPDB) = sy.symbols("mhB mhD mhOP mPB>D mPD>B")
(mhB_err, mhD_err, mhOP_err, mPBD_err, mPDB_err) \
    = sy.symbols("mhB_err mhD_err mhOP_err mPB>D_err mPD>B_err")
graph = {
    N.start0: {
        N.read0_BT: F1,
        N.read0_DF: 1 - F1,
    },
    N.start1: {
        N.read0_BF: 1 - F0,
        N.read0_DT: F0,
    },
    N.read0_BT: {
        N.surv0: hB * (1 - PBD),
        N.surv1: hB * PBD,
        N.nosurv: 1 - hB,
    },
    N.read0_BF: {
        N.surv0: hD * PDB,
        N.surv1: hD * (1 - PDB),
        N.nosurv: 1 - hD,
    },
    N.read0_DF: {
        N.surv0: hB * (1 - PBD),
        N.surv1: hB * PBD,
        N.nosurv: 1 - hB,
    },
    N.read0_DT: {
        N.surv0: hD * PDB,
        N.surv1: hD * (1 - PDB),
        N.nosurv: 1 - hD,
    },
    N.surv0: {
        N.read1_BT: F1,
        N.read1_DF: 1 - F1,
    },
    N.surv1: {
        N.read1_BF: 1 - F0,
        N.read1_DT: F0,
    },
    N.nosurv: {
        N.read1_BF: 1 - F0,
        N.read1_DT: F0,
    },
    N.read1_BT: { },
    N.read1_BF: { },
    N.read1_DF: { },
    N.read1_DT: { },
}

def dfs_all_paths(
    graph: dict[Node, dict[Node, sy.Symbol]],
    start_nodes: list[Node],
) -> list[(list[Node], sy.Product)]:
    """
    Assumes `graph` is acyclic.
    """

    def do_dfs(
        graph: dict[Node, dict[Node, sy.Symbol]],
        start: Node,
        term: sy.Symbol | sy.Product,
    ) -> list[(list[Node], sy.Product)]:
        if len(graph[start]) == 0:
            return [([start], term)]
        else:
            paths = list()
            for (neighbor, prob) in graph[start].items():
                paths += [
                    ([start] + subpath, term * subprob)
                    for (subpath, subprob) in do_dfs(graph, neighbor, prob)
                ]
            return paths

    paths = list()
    for start_node in start_nodes:
        paths += do_dfs(graph, start_node, sy.S(1))
    return paths

paths = dfs_all_paths(graph, [N.start0, N.start1])

P_BB = sum(
    prob for (path, prob) in paths
    if (
        (N.read0_BT in path or N.read0_BF in path)
        and (N.read1_BT in path or N.read1_BF in path)
    )
)
P_BX = sum(
    prob for (path, prob) in paths
    if N.read0_BT in path or N.read0_BF in path
)

sols = sy.solve(P_BB / P_BX - mhB, hB)

expr = sols[-1]
expr_err = sy.sqrt(sum(
    (v_err * expr.diff(v))**2
    for v, v_err in [
        (mhB, mhB_err),
        (F0, F0_err),
        (F1, F1_err),
        (hD, hD_err),
        (hOP, hOP_err),
        (PDB, PDB_err),
        (PBD, PBD_err),
    ]
))

vals = {
    mhB: 0.954,
    mhB_err: 0.009,
    F0: 0.995,
    F0_err: 0.002,
    F1: 0.986,
    F1_err: 0.007,
    hD: 0.994, #
    hD_err: 0.005, #
    hOP: 0.96,
    hOP_err: 0.03,
    PDB: 0.024,
    PDB_err: 0.010,
    PBD: 0.023,
    PBD_err: 0.006,
}

print(
    "hB =",
    value_str(
        float(expr.subs(vals).evalf()),
        float(expr_err.subs(vals).evalf())
    ),
)

