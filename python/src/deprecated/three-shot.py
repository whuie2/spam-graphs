import numpy as np
import sympy as sy
import sys

def value_str(
    x: float,
    err: float,
    dec: int=None,
    trunc: bool=True,
    latex: bool=False,
) -> str:
    _err = err if np.isfinite(err) else -1.0
    z = int(-np.floor(np.log10(_err))) if _err > 0.0 else 5
    z = min(dec, z) if dec is not None else max(z, 0)
    if trunc:
        if _err > 0.0:
            outstr = f"{{:.{z:.0f}f}}({{:.0f}})".format(x, 10**z * _err)
        else:
            outstr = f"{{:.{z:.0f}f}}(~)".format(x)
    else:
        if _err > 0.0:
            outstr = f"{{:.{z:.0f}f}} +/- {{:.{z:.0f}f}}".format(x, err)
        else:
            outstr = f"{{:.{z:.0f}f}} +/- [~]".format(x)
    if latex:
        outstr = "$" + outstr.replace("+/-", "\\pm") + "$"
    return outstr

class Node:
    label: str

    def __init__(self, label: str):
        self.label = label

    def __hash__(self):
        return hash(self.label)

    def __str__(self):
        return self.label

    def __repr__(self):
        return self.label

class N:
    # init
    start = Node("Start")
    start_0 = Node("Start |0>")
    start_1 = Node("Start |1>")
    empty = Node("Empty")

    # readout 0
    read0_B_0 = Node("Readout0: B (|0>)")
    read0_D_0 = Node("Readout0: D (|0>)")
    read0_B_1 = Node("Readout0: B (|1>)")
    read0_D_1 = Node("Readout0: D (|1>)")
    read0_B_l = Node("Readout0: B (lost)")
    read0_D_l = Node("Readout0: D (lost)")

    # post-readout 0 survival
    surv0_0 = Node("Readout0: survive (|0>)")
    surv0_1 = Node("Readout0: survive (|1>)")
    surv0_l = Node("Readout0: lost")

    # readout 1
    read1_B_0 = Node("Readout1: B (|0>)")
    read1_D_0 = Node("Readout1: D (|0>)")
    read1_B_1 = Node("Readout1: B (|1>)")
    read1_D_1 = Node("Readout1: D (|1>)")
    read1_B_l = Node("Readout1: B (lost)")
    read1_D_l = Node("Readout1: D (lost)")

    # post-readout 1 survival
    surv1_0 = Node("Readout1: survive (|0>)")
    surv1_1 = Node("Readout1: survive (|1>)")
    surv1_l = Node("Readout1: lost")
    surv1_x = Node("Readout1: survived")

    # pump 1
    pump1_0 = Node("Pump1: |0>")
    pump1_1 = Node("Pump1: |1>")

    # readout 2
    read2_B_0 = Node("Readout2: B (|0>)")
    read2_D_0 = Node("Readout2: D (|0>)")
    read2_B_1 = Node("Readout2: B (|1>)")
    read2_D_1 = Node("Readout2: D (|1>)")
    read2_B_l = Node("Readout2: B (lost)")
    read2_D_l = Node("Readout2: D (lost)")

(p, F0, F1, hB, hD, hOP, PBD, PDB) = sy.symbols("p F0 F1 hB hD hOP PB>D PD>B")
(p_err, F0_err, F1_err, hB_err, hD_err, hOP_err, PBD_err, PDB_err) \
    = sy.symbols("p_err F0_err F1_err hB_err hD_err hOP_err PB>D_err PD>B_err")
(mp, mhB, mhD, mhOP, mPBD, mPDB) = sy.symbols("mp mhB mhD mhOP mPB>D mPD>B")
(mp_err, mhB_err, mhD_err, mhOP_err, mPBD_err, mPDB_err) \
    = sy.symbols("mp_err mhB_err mhD_err mhOP_err mPB>D_err mPD>B_err")

graph = {
    # init
    N.start: {
        N.start_0: p / 2,
        N.start_0: p / 2,
        N.empty: 1 - p,
    },
    N.start_0: {
        N.read0_B_0: F1,
        N.read0_D_0: 1 - F1,
    },
    N.start_1: {
        N.read0_B_1: 1 - F0,
        N.read0_D_1: F0,
    },
    N.empty: {
        N.read0_B_l: 1 - F0,
        N.read0_D_l: F0,
    },

    # readout 0
    N.read0_B_0: {
        N.surv0_0: hB * (1 - PBD),
        N.surv0_1: hB * PBD,
        N.surv0_l: 1 - hB,
    },
    N.read0_D_0: {
        N.surv0_0: hB * (1 - PBD),
        N.surv0_1: hB * PBD,
        N.surv0_l: 1 - hB,
    },
    N.read0_B_1: {
        N.surv0_0: hD * PDB,
        N.surv0_1: hD * (1 - PDB),
        N.surv0_l: 1 - hD,
    },
    N.read0_D_1: {
        N.surv0_0: hD * PDB,
        N.surv0_1: hD * (1 - PDB),
        N.surv0_l: 1 - hD,
    },
    N.read0_B_l: {
        N.surv0_l: sy.S(1),
    },
    N.read0_D_l: {
        N.surv0_l: sy.S(1),
    },

    # post-readout 0 survival
    N.surv0_0: {
        N.read1_B_0: F1,
        N.read1_D_0: 1 - F1,
    },
    N.surv0_1: {
        N.read1_B_1: 1 - F0,
        N.read1_D_1: F0,
    },
    N.surv0_l: {
        N.read1_B_l: 1 - F0,
        N.read1_D_1: F0,
    },

    # readout 1
    N.read1_B_0: {
        N.surv1_0: hB * (1 - PBD),
        N.surv1_1: hB * PBD,
        N.surv1_l: 1 - hB,
    },
    N.read1_D_0: {
        N.surv1_0: hB * (1 - PBD),
        N.surv1_1: hB * PBD,
        N.surv1_l: 1 - hB,
    },
    N.read1_B_1: {
        N.surv1_0: hD * PDB,
        N.surv1_1: hD * (1 - PDB),
        N.surv1_l: 1 - hD,
    },
    N.read1_D_1: {
        N.surv1_0: hD * PDB,
        N.surv1_1: hD * (1 - PDB),
        N.surv1_l: 1 - hD,
    },
    N.read1_B_l: {
        N.surv1_l: sy.S(1),
    },
    N.read1_D_l: {
        N.surv1_l: sy.S(1),
    },

    # post-readout 1 survival
    N.surv1_0: {
        N.surv1_x: sy.S(1),
    },
    N.surv1_1: {
        N.surv1_x: sy.S(1),
    },
    N.surv1_x: {
        N.pump1_0: hOP,
        N.pump1_1: 1 - hOP,
    },
    N.surv1_l: {
        N.read2_B_l: 1 - F0,
        N.read2_D_l: F0,
    },

    # pump 1
    N.pump1_0: {
        N.read2_B_0: F1,
        N.read2_D_0: 1 - F1,
    },
    N.pump1_1: {
        N.read2_B_1: 1 - F0,
        N.read2_D_1: F0,
    },

    # readout 2
    N.read2_B_0: { },
    N.read2_D_0: { },
    N.read2_B_1: { },
    N.read2_D_1: { },
    N.read2_B_l: { },
    N.read2_D_l: { },
}

def dfs_all_paths(
    graph: dict[Node, dict[Node, sy.Symbol]],
    start_nodes: list[Node],
) -> list[(list[Node], sy.Product)]:
    """
    Assumes `graph` is acyclic.
    """

    def do_dfs(
        graph: dict[Node, dict[Node, sy.Symbol]],
        start: Node,
        term: sy.Symbol | sy.Product,
    ) -> list[(list[Node], sy.Product)]:
        if len(graph[start]) == 0:
            return [([start], term)]
        else:
            paths = list()
            for (neighbor, prob) in graph[start].items():
                paths += [
                    ([start] + subpath, term * subprob)
                    for (subpath, subprob) in do_dfs(graph, neighbor, prob)
                ]
            return paths

    paths = list()
    for start_node in start_nodes:
        paths += do_dfs(graph, start_node, sy.S(1))
    return paths

paths = dfs_all_paths(graph, [N.start])

P_BDB = sum(
    prob for (path, prob) in paths
    if (
        (N.read0_B_0 in path or N.read0_B_1 in path or N.read0_B_l in path)
        and (N.read1_B_0 in path or N.read1_B_1 in path or N.read1_B_l in path)
        and (N.read2_B_0 in path or N.read2_B_1 in path or N.read2_B_l in path)
    )
)

P_BXB = sum(
    prob for (path, prob) in paths
    if (
        (N.read0_B_0 in path or N.read0_B_1 in path or N.read0_B_l in path)
        and (N.read2_B_0 in path or N.read2_B_1 in path or N.read2_B_l in path)
    )
)

latex_replace = {
    "mp": r'{{}^*\mkern-2.5mu{p}',
    "mhB": r'{{}^*\mkern-2.5mu{\eta_\text{B}}}',
    "mhD": r'{{}^*\mkern-2.5mu{\eta_\text{D}}}',
    "mhOP": r'{{}^*\mkern-2.5mu{\eta_\text{OP}}}',
    "mPB>D": r'{{}^*\mkern-2.5mu{P_\text{B$\rightarrow$D}}}',
    "mPD>B": r'{{}^*\mkern-2.5mu{P_\text{D$\rightarrow$B}}}',
    "mPdepol": r'{{}^*\mkern-2.5mu{\bar{P}_\text{depol}}}',
    "F0": r'\mathcal{F}_0',
    "F1": r'\mathcal{F}_1',
    "F": r'\mathcal{F}',
    "hB": r'\eta_\text{B}',
    "hD": r'\eta_\text{D}',
    "hOP": r'\eta_\text{OP}',
    "PB>D": r'P_\text{B$\rightarrow$D}',
    "PD>B": r'P_\text{D$\rightarrow$B}',
    "Pdepol": r'\bar{P}_\text{depol}',
}
def replace_symbols(s: str) -> str:
    sp = s
    for source, target in latex_replace.items():
        sp = sp.replace(source, target)
    return sp

print(replace_symbols(sy.latex(sy.Eq(mPBD, (P_BDB / P_BXB).simplify()))))
sys.exit(0)

