import numpy as np
import sympy as sy
import sys

def value_str(
    x: float,
    err: float,
    dec: int=None,
    trunc: bool=True,
    latex: bool=False,
) -> str:
    _err = err if np.isfinite(err) else -1.0
    z = int(-np.floor(np.log10(_err))) if _err > 0.0 else 5
    z = min(dec, z) if dec is not None else max(z, 0)
    if trunc:
        if _err > 0.0:
            outstr = f"{{:.{z:.0f}f}}({{:.0f}})".format(x, 10**z * _err)
        else:
            outstr = f"{{:.{z:.0f}f}}(~)".format(x)
    else:
        if _err > 0.0:
            outstr = f"{{:.{z:.0f}f}} +/- {{:.{z:.0f}f}}".format(x, err)
        else:
            outstr = f"{{:.{z:.0f}f}} +/- [~]".format(x)
    if latex:
        outstr = "$" + outstr.replace("+/-", "\\pm") + "$"
    return outstr

class Node:
    label: str

    def __init__(self, label: str):
        self.label = label

    def __hash__(self):
        return hash(self.label)

    def __str__(self):
        return self.label

    def __repr__(self):
        return self.label

class N:
    # init
    start_0 = Node("Start |0>")
    start_1 = Node("Start |1>")

    # pump 0
    pump0_0 = Node("Pump0: |0>")
    pump0_1 = Node("Pump0: |1>")

    # readout 0
    read0_B_0 = Node("Readout0: B (|0>)")
    read0_D_0 = Node("Readout0: D (|0>)")
    read0_B_1 = Node("Readout0: B (|1>)")
    read0_D_1 = Node("Readout0: D (|1>)")

    # post-readout 0 survival
    surv0_0 = Node("Readout0: survive (|0>)")
    surv0_1 = Node("Readout0: survive (|1>)")
    surv0_l = Node("Readout0: lost")

    # readout 1
    read1_B_0 = Node("Readout1: B (|0>)")
    read1_D_0 = Node("Readout1: D (|0>)")
    read1_B_1 = Node("Readout1: B (|1>)")
    read1_D_1 = Node("Readout1: D (|1>)")
    read1_B_l = Node("Readout1: B (lost)")
    read1_D_l = Node("Readout1: D (lost)")

    # post-readout 1 survival
    surv1_0 = Node("Readout1: survive (|0>)")
    surv1_1 = Node("Readout1: survive (|1>)")
    surv1_l = Node("Readout1: lost")

    # readout 2
    read2_B_0 = Node("Readout2: B (|0>)")
    read2_D_0 = Node("Readout2: D (|0>)")
    read2_B_1 = Node("Readout2: B (|1>)")
    read2_D_1 = Node("Readout2: D (|1>)")
    read2_B_l = Node("Readout2: B (lost)")
    read2_D_l = Node("Readout2: D (lost)")

    # post-readout 2 survival
    surv2_0 = Node("Readout2: survive (|0>)")
    surv2_1 = Node("Readout2: survive (|1>)")
    surv2_l = Node("Readout2: lost")

    # pump 1
    pump1_0 = Node("Pump1: |0>")
    pump1_1 = Node("Pump1: |1>")

    # readout 2
    read3_B_0 = Node("Readout3: B (|0>)")
    read3_D_0 = Node("Readout3: D (|0>)")
    read3_B_1 = Node("Readout3: B (|1>)")
    read3_D_1 = Node("Readout3: D (|1>)")
    read3_B_l = Node("Readout3: B (lost)")
    read3_D_l = Node("Readout3: D (lost)")

(F0, F1, hB, hD, hOP, PBD, PDB) = sy.symbols("F0 F1 hB hD hOP PB>D PD>B")
(F0_err, F1_err, hB_err, hD_err, hOP_err, PBD_err, PDB_err) \
    = sy.symbols("F0_err F1_err hB_err hD_err hOP_err PB>D_err PD>B_err")
(mhB, mhD, mhOP, mPBD, mPDB) = sy.symbols("mhB mhD mhOP mPB>D mPD>B")
(mhB_err, mhD_err, mhOP_err, mPBD_err, mPDB_err) \
    = sy.symbols("mhB_err mhD_err mhOP_err mPB>D_err mPD>B_err")

graph = {
    # init
    N.start_0: {
        N.pump0_0: sy.S(1),
    },
    N.start_1: {
        N.pump0_0: hOP,
        N.pump0_1: 1 - hOP,
    },

    # pump 0
    N.pump0_0: {
        N.read0_B_0: F1,
        N.read0_D_0: 1 - F1,
    },
    N.pump0_1: {
        N.read0_B_1: 1 - F0,
        N.read0_D_1: F0,
    },

    # readout 0
    N.read0_B_0: {
        N.surv0_0: hB * (1 - PBD),
        N.surv0_1: hB * PBD,
        N.surv0_l: 1 - hB,
    },
    N.read0_D_0: {
        N.surv0_0: hB * (1 - PBD),
        N.surv0_1: hB * PBD,
        N.surv0_l: 1 - hB,
    },
    N.read0_B_1: {
        N.surv0_0: hD * PDB,
        N.surv0_1: hD * (1 - PDB),
        N.surv0_l: 1 - hD,
    },
    N.read0_D_1: {
        N.surv0_0: hD * PDB,
        N.surv0_1: hD * (1 - PDB),
        N.surv0_l: 1 - hD,
    },

    # post-readout 0 survival
    N.surv0_0: {
        N.read1_B_0: F1,
        N.read1_D_0: 1 - F1,
    },
    N.surv0_1: {
        N.read1_B_1: 1 - F0,
        N.read1_D_1: F0,
    },
    N.surv0_l: {
        N.read1_B_l: 1 - F0,
        N.read1_D_l: F0,
    },

    # readout 1
    N.read1_B_0: {
        N.surv1_0: hB * (1 - PBD),
        N.surv1_1: hB * PBD,
        N.surv1_l: 1 - hB,
    },
    N.read1_D_0: {
        N.surv1_0: hB * (1 - PBD),
        N.surv1_1: hB * PBD,
        N.surv1_l: 1 - hB,
    },
    N.read1_B_1: {
        N.surv1_0: hD * PDB,
        N.surv1_1: hD * (1 - PDB),
        N.surv1_l: 1 - hD,
    },
    N.read1_D_1: {
        N.surv1_0: hD * PDB,
        N.surv1_1: hD * (1 - PDB),
        N.surv1_l: 1 - hD,
    },
    N.read1_B_l: {
        N.surv1_l: sy.S(1),
    },
    N.read1_D_l: {
        N.surv1_l: sy.S(1),
    },
    
    # post-readout 1 survival
    N.surv1_0: {
        N.read2_B_0: F1,
        N.read2_D_0: 1 - F1,
    },
    N.surv1_1: {
        N.read2_B_1: 1 - F0,
        N.read2_D_1: F0,
    },
    N.surv1_l: {
        N.read2_B_l: 1 - F0,
        N.read2_D_l: F0,
    },

    # readout 2
    N.read2_B_0: {
        N.surv2_0: hB * (1 - PBD),
        N.surv2_1: hB * PBD,
        N.surv2_l: 1 - hB,
    },
    N.read2_D_0: {
        N.surv2_0: hB * (1 - PBD),
        N.surv2_1: hB * PBD,
        N.surv2_l: 1 - hB,
    },
    N.read2_B_1: {
        N.surv2_0: hD * PDB,
        N.surv2_1: hD * (1 - PDB),
        N.surv2_l: 1 - hD,
    },
    N.read2_D_1: {
        N.surv2_0: hD * PDB,
        N.surv2_1: hD * (1 - PDB),
        N.surv2_l: 1 - hD,
    },
    N.read2_B_l: {
        N.surv2_l: sy.S(1),
    },
    N.read2_D_l: {
        N.surv2_l: sy.S(1),
    },

    # post-readout 2 survival
    N.surv1_0: {
        N.pump1_0: sy.S(1)
    },
    N.surv1_1: {
        N.pump1_0: hOP,
        N.pump1_1: 1 - hOP,
    },
    N.surv1_l: {
        N.read3_B_l: 1 - F0,
        N.read3_D_l: F0,
    },

    # pump 1
    N.pump1_0: {
        N.read3_B_0: F1,
        N.read3_D_0: 1 - F1,
    },
    N.pump1_1: {
        N.read3_B_1: 1 - F0,
        N.read3_D_1: F0,
    },

    # readout 2
    N.read3_B_0: { },
    N.read3_D_0: { },
    N.read3_B_1: { },
    N.read3_D_1: { },
    N.read3_B_l: { },
    N.read3_D_l: { },
}

def dfs_all_paths(
    graph: dict[Node, dict[Node, sy.Symbol]],
    start_nodes: list[Node],
) -> list[(list[Node], sy.Product)]:
    """
    Assumes `graph` is acyclic.
    """

    def do_dfs(
        graph: dict[Node, dict[Node, sy.Symbol]],
        start: Node,
        term: sy.Symbol | sy.Product,
    ) -> list[(list[Node], sy.Product)]:
        if len(graph[start]) == 0:
            return [([start], term)]
        else:
            paths = list()
            for (neighbor, prob) in graph[start].items():
                paths += [
                    ([start] + subpath, term * subprob)
                    for (subpath, subprob) in do_dfs(graph, neighbor, prob)
                ]
            return paths

    paths = list()
    for start_node in start_nodes:
        paths += do_dfs(graph, start_node, sy.S(1))
    return paths

paths = dfs_all_paths(graph, [N.start_0, N.start_1])

P_XBBX = sum(
    prob for (path, prob) in paths
    if (
        (N.read1_B_0 in path or N.read1_B_1 in path or N.read1_B_l in path)
        and (N.read2_B_0 in path or N.read2_B_1 in path or N.read2_B_l in path)
    )
)

P_XBXX = sum(
    prob for (path, prob) in paths
    if (
        (N.read1_B_0 in path or N.read1_B_1 in path or N.read1_B_l in path)
    )
)

P_XBDB = sum(
    prob for (path, prob) in paths
    if (
        (N.read1_B_0 in path or N.read1_B_1 in path or N.read1_B_l in path)
        and (N.read2_D_0 in path or N.read2_D_1 in path or N.read2_D_l in path)
        and (N.read3_B_0 in path or N.read3_B_1 in path or N.read3_B_l in path)
    )
)

print(P_XBDB)
sys.exit(0)

sols = sy.solve(P_XBBX / (P_XBXX - P_XBDB) - mhB, hB)
expr = sols[0]
expr_err = sy.sqrt(sum(
    (v_err * expr.diff(v))**2
    for (v, v_err) in [
        (mhB, mhB_err),
        (F0, F0_err),
        (F1, F1_err),
        (hD, hD_err),
        (hOP, hOP_err),
        (PDB, PDB_err),
        (PBD, PBD_err),
    ]
))

vals = {
    mhB: 0.9937,
    mhB_err: 0.0007,
    F0: 0.995,
    F0_err: 0.002,
    F1: 0.986,
    F1_err: 0.007,
    hD: 0.994, #
    hD_err: 0.005, #
    hOP: 0.96,
    hOP_err: 0.03,
    PDB: 0.024,
    PDB_err: 0.010,
    PBD: 0.023,
    PBD_err: 0.006,
}

print(
    "hB =",
    value_str(
        float(expr.subs(vals).evalf()),
        float(expr_err.subs(vals).evalf())
    ),
)

