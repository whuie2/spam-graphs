from dataclasses import dataclass
import numpy as np
import sympy as sy

def value_str(
    x: float,
    err: float,
    dec: int=None,
    trunc: bool=True,
    latex: bool=False,
) -> str:
    _err = err if np.isfinite(err) else -1.0
    z = int(-np.floor(np.log10(_err))) if _err > 0.0 else 5
    z = min(dec, z) if dec is not None else max(z, 0)
    if trunc:
        if _err > 0.0:
            outstr = f"{{:.{z:.0f}f}}({{:.0f}})".format(x, 10**z * _err)
        else:
            outstr = f"{{:.{z:.0f}f}}(~)".format(x)
    else:
        if _err > 0.0:
            outstr = f"{{:.{z:.0f}f}} +/- {{:.{z:.0f}f}}".format(x, err)
        else:
            outstr = f"{{:.{z:.0f}f}} +/- [~]".format(x)
    if latex:
        outstr = "$" + outstr.replace("+/-", "\\pm") + "$"
    return outstr

class Node:
    label: str

    def __init__(self, label: str):
        self.label = label

    def __hash__(self):
        return hash(self.label)

    def __str__(self):
        return self.label

    def __repr__(self):
        return self.label

class Nodes:
    start0 = Node("Start in |0>")
    start1 = Node("Start in |1>")
    to0_pump0 = Node("Goes to |0> (pump 0)")
    to1_pump0 = Node("Goes to |1> (pump 0)")
    B_readout0 = Node("Bright in readout 0")
    B_readout0_fake = Node("Bright in readout 0 (fake)")
    D_readout0 = Node("Dark in readout 0")
    D_readout0_fake = Node("Dark in readout 0 (fake)")
    to0_pump1 = Node("Goes to |0> (pump 1)")
    to1_pump1 = Node("Goes to |1> (pump 1)")
    B_readout1 = Node("Bright in readout 1")
    B_readout1_fake = Node("Bright in readout 1 (fake)")
    D_readout1 = Node("Dark in readout 1")
    D_readout1_fake = Node("Dark in readout 1 (fake)")

eta, eta_err, n, F0, F0_err, F1, F1_err = sy.symbols("eta etaerr n F0 F0err F1 F1err")
graph = {
    Nodes.start0: {
        Nodes.to0_pump0: sy.S(1),
    },
    Nodes.start1: {
        Nodes.to0_pump0: n,
        Nodes.to1_pump0: 1 - n,
    },
    Nodes.to0_pump0: {
        Nodes.B_readout0: F1,
        Nodes.D_readout0_fake: 1 - F1,
    },
    Nodes.to1_pump0: {
        Nodes.B_readout0_fake: 1 - F0,
        Nodes.D_readout0: F0,
    },
    Nodes.B_readout0: {
        Nodes.to0_pump1: sy.S(1),
    },
    Nodes.B_readout0_fake: {
        Nodes.to0_pump1: n,
        Nodes.to1_pump1: 1 - n,
    },
    Nodes.D_readout0: {
        Nodes.to0_pump1: n,
        Nodes.to1_pump1: 1 - n,
    },
    Nodes.D_readout0_fake: {
        Nodes.to0_pump1: sy.S(1),
    },
    Nodes.to0_pump1: {
        Nodes.B_readout1: F1,
        Nodes.D_readout1_fake: 1 - F1,
    },
    Nodes.to1_pump1: {
        Nodes.B_readout1_fake: 1 - F0,
        Nodes.D_readout1: F0,
    },
    Nodes.B_readout1: { },
    Nodes.B_readout1_fake: { },
    Nodes.D_readout1: { },
    Nodes.D_readout1_fake: { },
}

def dfs_all_paths(
    graph: dict[Node, dict[Node, sy.Symbol]],
    start_nodes: list[Node],
) -> list[(list[Node], sy.Product)]:
    """
    Assumes `graph` is acyclic.
    """

    def do_dfs(
        graph: dict[Node, dict[Node, sy.Symbol]],
        start: Node,
        term: sy.Symbol | sy.Product,
    ) -> list[(list[Node], sy.Product)]:
        if len(graph[start]) == 0:
            return [([start], term)]
        else:
            paths = list()
            for (neighbor, prob) in graph[start].items():
                paths += [
                    ([start] + subpath, term * subprob)
                    for (subpath, subprob) in do_dfs(graph, neighbor, prob)
                ]
            return paths

    paths = list()
    for start_node in start_nodes:
        paths += do_dfs(graph, start_node, sy.S(1))
    return paths

paths = dfs_all_paths(graph, [Nodes.start0, Nodes.start1])

P_BB = sum(
    prob for (path, prob) in paths
    if (
        (Nodes.B_readout0 in path or Nodes.B_readout0_fake in path)
        and (Nodes.B_readout1 in path or Nodes.B_readout1_fake in path)
    )
)

P_XB = sum(
    prob for (path, prob) in paths
    if Nodes.B_readout1 in path or Nodes.B_readout1_fake in path
)

# print("P_BB =")
# sy.pprint(P_BB)
# print("P_XB =")
# sy.pprint(P_XB)

sols = sy.solve(P_BB / P_XB - eta, n)

expr = sols[1]
expr_err = sy.sqrt(sum(
    (v_err * expr.diff(v))**2
    for v, v_err in [
        (eta, eta_err),
        (F0, F0_err),
        (F1, F1_err),
    ]
))

vals = {
    eta: 0.967,
    eta_err: 0.009,
    F0: 0.995,
    F0_err: 0.002,
    F1: 0.986,
    F1_err: 0.007,
}

print(
    "eta =",
    value_str(
        float(expr.subs(vals).evalf()),
        float(expr_err.subs(vals).evalf())
    ),
)

