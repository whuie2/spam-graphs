from itertools import product
import sys
import toml
import numpy as np
from lib.analysis import value_str
from lib.sequences import all_corr
from lib.symbols import S


def qq(X):
    print(X)
    return X

bootstrap_path = "raw/bootstrap_results.toml"
depolarization_path = "raw/depolarization_results.toml"
dark_survival_path = "raw/dark_state_survival.toml"

with open(bootstrap_path, 'r') as infile:
    bootstrap_data = toml.load(infile)
with open(depolarization_path, 'r') as infile:
    depolarization_data = toml.load(infile)
with open(dark_survival_path, 'r') as infile:
    dark_surv_data = toml.load(infile)

keys = sorted(
    set(bootstrap_data.keys())
    .intersection(set(depolarization_data.keys()))
    .intersection(set(dark_surv_data.keys()))
)[:]
results = dict()
for key in keys:
    print(key, file=sys.stderr)

    F = bootstrap_data[key]["fidelity"]
    F0 = bootstrap_data[key]["F0"]
    F1 = bootstrap_data[key]["F1"]
    p_raw = bootstrap_data[key]["fill"]
    hB_raw = bootstrap_data[key]["survival_raw"]
    hD_raw = dark_surv_data[key]["survD_raw"]
    hOP_raw = bootstrap_data[key]["op_raw"]
    PDB_raw = depolarization_data[key]["P(D -> B)"]
    PBD_raw = depolarization_data[key]["P(B -> D)"]
    hpi_raw = [0.9693396226415094, 0.008372281115795138]

    print("find nominal corrected values", file=sys.stderr)
    (p, hB, hOP, PDB, PBD, hpi) = all_corr(
        p_init=p_raw[0],
        hB_init=hB_raw[0],
        # hD_init=hD_raw[0],
        hOP_init=hOP_raw[0],
        PDB_init=PDB_raw[0],
        PBD_init=PBD_raw[0],
        hpi_init=hpi_raw[0],
        consts={
            S.F0: F0[0],
            S.F1: F1[0],
            S.mp: p_raw[0],
            S.mhB: hB_raw[0],
            S.mhD: hD_raw[0],
            S.hD: hD_raw[0],
            S.mhOP: hOP_raw[0],
            S.mPDB: PDB_raw[0],
            S.mPBD: PBD_raw[0],
            S.mhpi: hpi_raw[0],
        },
        maxiters=10,
        eps=1e-6,
        printflag=False,
    )

    print("find correction bounds", file=sys.stderr)
    p_lim = list()
    hB_lim = list()
    hOP_lim = list()
    PDB_lim = list()
    PBD_lim = list()
    hpi_lim = list()
    prod_iter = enumerate(product(*[
        [v - v_err, v + v_err]
        for (v, v_err) in [
            F0,
            F1,
            p_raw,
            hB_raw,
            hD_raw,
            hOP_raw,
            PDB_raw,
            PBD_raw,
            hpi_raw,
        ]
    ]))
    for (k, (_F0, _F1, _p, _hB, _hD, _hOP, _PDB, _PBD, _hpi)) in prod_iter:
        print(f"  {k}          \r", end="", flush=True, file=sys.stderr)
        (_p_, _hB_, _hOP_, _PDB_, _PBD_, _hpi_) = all_corr(
            p_init=p,
            hB_init=hB,
            hOP_init=hOP,
            PDB_init=PDB,
            PBD_init=PBD,
            hpi_init=hpi,
            consts={
                S.F0: _F0,
                S.F1: _F1,
                S.mp: _p,
                S.mhB: _hB,
                S.mhD: _hD,
                S.hD: _hD,
                S.mhOP: _hOP,
                S.mPDB: _PDB,
                S.mPBD: _PBD,
                S.mhpi: _hpi,
            },
            maxiters=10,
            eps=1e-6,
            printflag=False,
        )
        p_lim.append(_p_)
        hB_lim.append(_hB_)
        hOP_lim.append(_hOP_)
        PDB_lim.append(_PDB_)
        PBD_lim.append(_PBD_)
        hpi_lim.append(_hpi_)
    print(file=sys.stderr)
    p_err = np.sqrt(sum(abs(x - p)**2 for x in p_lim) / len(p_lim))
    hB_err = np.sqrt(sum(abs(x - hB)**2 for x in hB_lim) / len(hB_lim))
    hOP_err = np.sqrt(sum(abs(x - hOP)**2 for x in hOP_lim) / len(hOP_lim))
    PDB_err = np.sqrt(sum(abs(x - PDB)**2 for x in PDB_lim) / len(PDB_lim))
    PBD_err = np.sqrt(sum(abs(x - PBD)**2 for x in PBD_lim) / len(PBD_lim))
    hpi_err = np.sqrt(sum(abs(x - hpi)**2 for x in hpi_lim) / len(hpi_lim))

    p = (float(p), float(p_err))
    hB = (float(hB), float(hB_err))
    hOP = (float(hOP), float(hOP_err))
    PDB = (float(PDB), float(PDB_err))
    PBD = (float(PBD), float(PBD_err))
    hpi = (float(hpi), float(hpi_err))

    results[key] = {
        "F0": F0,
        "F1": F1,
        "F": F,
        "p_raw": p_raw,
        "p": p,
        "hB_raw": hB_raw,
        "hB": hB,
        "hD_raw": hD_raw,
        "hOP_raw": hOP_raw,
        "hOP": hOP,
        "PDB_raw": PDB_raw,
        "PDB": PDB,
        "PBD_raw": PBD_raw,
        "PBD": PBD,
        "hpi_raw": hpi_raw,
        "hpi": hpi,
    }

    print(key)
    print(f"fidelity                         = {value_str(*F)}")
    print(f"F0                               = {value_str(*F0)}")
    print(f"F1                               = {value_str(*F1)}")
    print(f"p (raw)                          = {value_str(*p_raw)}")
    print(f"p (corrected)                    = {value_str(*p)}")
    # print(f"depol (raw)                      = {value_str(*depol_raw)}")
    # print(f"depol (corrected)                = {value_str(*depol)}")
    print(f"P(D -> B) (raw)                  = {value_str(*PDB_raw)}")
    print(f"P(D -> B) (corrected)            = {value_str(*PDB)}")
    print(f"P(B -> D) (raw)                  = {value_str(*PBD_raw)}")
    print(f"P(B -> D) (corrected)            = {value_str(*PBD)}")
    print(f"dark survival (raw)              = {value_str(*hD_raw)}")
    print(f"OP efficiency (raw)              = {value_str(*hOP_raw)}")
    print(f"OP efficiency (corrected)        = {value_str(*hOP)}")
    print(f"bright survival (raw)            = {value_str(*hB_raw)}")
    print(f"bright survival (corrected)      = {value_str(*hB)}")
    print(f"pi-pulse probability (raw)       = {value_str(*hpi_raw)}")
    print(f"pi-pulse probability (corrected) = {value_str(*hpi)}")
    print()

with open("output/results.toml", 'w') as outfile:
    toml.dump(results, outfile)

