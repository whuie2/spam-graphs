import toml
import sys
import numpy as np
import sympy as sy
from lib.analysis import value_str
import lib.sequences as seq
from lib.sequences import PiPulse
from lib.symbols import S

results_path = "output/results.toml"
key = "flattened 5-array -3/2"
with open(results_path, 'r') as infile:
    results_data = toml.load(infile)[key]

F0_data = results_data["F0"]
F1_data = results_data["F1"]
PBD_data = results_data["PBD"]
PDB_data = results_data["PDB"]
hB_data = results_data["hB"]
hD_raw_data = results_data["hD_raw"]
hOP_data = results_data["hOP"]
hpi_data = results_data["hpi"]
p_data = results_data["p"]

vals = {
    S.F0: F0_data[0],
    S.F1: F1_data[0],
    S.PBD: PBD_data[0],
    S.PDB: PDB_data[0],
    S.hB: hB_data[0],
    S.hD: hD_raw_data[0],
    S.hOP: hOP_data[0],
    S.hpi: hpi_data[0],
    S.p: p_data[0],
}
vals_err = {
    S.F0_err: F0_data[1],
    S.F1_err: F1_data[0],
    S.PBD_err: PBD_data[1],
    S.PDB_err: PDB_data[1],
    S.hB_err: hB_data[1],
    S.hD_err: hD_raw_data[1],
    S.hOP_err: hOP_data[1],
    S.hpi_err: hpi_data[1],
    S.p_err: p_data[1],
}

def ev(x) -> float:
    return float(x.evalf(subs=vals))

def ev_err(x) -> float:
    expr = sy.sqrt(sum(
        (v_err * x.diff(v))**2
        for (v, v_err) in zip(vals.keys(), vals_err.keys())
    ))
    return float(expr.evalf(subs=vals | vals_err))

rho_0_post = (
    sum(
        prob for (path, prob) in seq._paths_pipulse
        if (
            PiPulse.read0_B_0 in path
            or PiPulse.read0_B_1 in path
            or PiPulse.read0_B_e in path
        ) and (
            PiPulse.pipulse_0 in path
        # ) and (
        #     PiPulse.read1_B_0 in path
        #     or PiPulse.read1_B_1 in path
        #     or PiPulse.read1_B_l in path
        # ) and (
        #     PiPulse.read2_B_0 in path
        #     or PiPulse.read2_B_1 in path
        #     or PiPulse.read2_B_l in path
        )
    )
    / sum(
        prob for (path, prob) in seq._paths_pipulse
        if (
            PiPulse.read0_B_0 in path
            or PiPulse.read0_B_1 in path
            or PiPulse.read0_B_e in path
        # ) and (
        #     PiPulse.read2_B_0 in path
        #     or PiPulse.read2_B_1 in path
        #     or PiPulse.read2_B_l in path
        )
    )
).simplify()

rho_0_pre = (
    sum( # a
        prob for (path, prob) in seq._paths_pipulse
        if (
            PiPulse.read0_B_0 in path
            or PiPulse.read0_B_1 in path
            or PiPulse.read0_B_e in path
        ) and (
            PiPulse.surv0_0 in path
        # ) and (
        #     PiPulse.read2_B_0 in path
        #     or PiPulse.read2_B_1 in path
        #     or PiPulse.read2_B_l in path
        )
    )
    / sum(
        prob for (path, prob) in seq._paths_pipulse
        if (
            PiPulse.read0_B_0 in path
            or PiPulse.read0_B_1 in path
            or PiPulse.read0_B_e in path
        # ) and (
        #     PiPulse.read2_B_0 in path
        #     or PiPulse.read2_B_1 in path
        #     or PiPulse.read2_B_l in path
        )
    )
).simplify()

rho_1_pre = (
    sum( # b
        prob for (path, prob) in seq._paths_pipulse
        if (
            PiPulse.read0_B_0 in path
            or PiPulse.read0_B_1 in path
            or PiPulse.read0_B_e in path
        ) and (
            PiPulse.surv0_1 in path
        # ) and (
        #     PiPulse.read2_B_0 in path
        #     or PiPulse.read2_B_1 in path
        #     or PiPulse.read2_B_l in path
        )
    )
    / sum(
        prob for (path, prob) in seq._paths_pipulse
        if (
            PiPulse.read0_B_0 in path
            or PiPulse.read0_B_1 in path
            or PiPulse.read0_B_e in path
        # ) and (
        #     PiPulse.read2_B_0 in path
        #     or PiPulse.read2_B_1 in path
        #     or PiPulse.read2_B_l in path
        )
    )
).simplify()

rho_e_pre = (
    sum( # b
        prob for (path, prob) in seq._paths_pipulse
        if (
            PiPulse.read0_B_0 in path
            or PiPulse.read0_B_1 in path
            or PiPulse.read0_B_e in path
        ) and (
            PiPulse.surv0_l in path
        # ) and (
        #     PiPulse.read2_B_0 in path
        #     or PiPulse.read2_B_1 in path
        #     or PiPulse.read2_B_l in path
        )
    )
    / sum(
        prob for (path, prob) in seq._paths_pipulse
        if (
            PiPulse.read0_B_0 in path
            or PiPulse.read0_B_1 in path
            or PiPulse.read0_B_e in path
        # ) and (
        #     PiPulse.read2_B_0 in path
        #     or PiPulse.read2_B_1 in path
        #     or PiPulse.read2_B_l in path
        )
    )
).simplify()

F_pi = sy.sqrt((1 - (2 * rho_0_post - (rho_0_pre + rho_1_pre)) / (rho_0_pre - rho_1_pre)) / 2)

print("rho_0 =", ev(rho_0_post))
print("a =", ev(rho_0_pre))
print("b =", ev(rho_1_pre))
print("c =", ev(rho_e_pre))
print("a + b + c =", ev(rho_0_pre) + ev(rho_1_pre) + ev(rho_e_pre))
print("F_pi =", value_str(ev(F_pi), ev_err(F_pi)))

